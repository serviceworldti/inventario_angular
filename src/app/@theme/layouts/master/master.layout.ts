import {Component} from '@angular/core';
import {NbMenuItem, NbThemeService} from '@nebular/theme';
import {map, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {MenuService} from '../../../@core/utils';

@Component({
  selector: 'ngx-principal-layout',
  styleUrls: ['./master.layout.scss'],
  template: `
    <mat-sidenav-container class="h-100 bg-transparent" (window:resize)="onSidenavClose()">
      <mat-sidenav-content class="{{theme}}">
        <nb-layout windowMode>
          <nb-layout-header fixed>
            <ngx-header [layout]="layout"></ngx-header>
          </nb-layout-header>

          <nb-sidebar class="menu-sidebar" tag="menu-sidebar" *ngIf="navbarOpen && layout !== 'panel'">
            <nb-menu [items]="NbNavbar"></nb-menu>
          </nb-sidebar>

          <nb-sidebar class="menu-sidebar" tag="menu-sidebar" *ngIf="layout === 'panel'" responsive>
            <nb-menu [items]="NbSidebar"></nb-menu>
          </nb-sidebar>

          <nb-layout-column>
            <div class="progress-bar">
              <nb-progress-bar *ngIf="progressBar.value > 0"
                               [value]="progressBar.value"
                               [status]="progressBar.status"
                               [size]="'tiny'">
              </nb-progress-bar>
            </div>
            <nb-alert status="{{alert.type}}" *ngIf="alert.open">{{alert.message}}</nb-alert>
            <ng-content select="router-outlet"></ng-content>
          </nb-layout-column>

          <nb-layout-footer fixed>
            <ngx-footer></ngx-footer>
          </nb-layout-footer>
        </nb-layout>
      </mat-sidenav-content>
    </mat-sidenav-container>
  `,
})
export class PrincipalLayoutComponent {

  layout: string = 'principal';
  NbNavbar: NbMenuItem[] = this.oMenuService.dataNbMenuBarV;
  NbSidebar: NbMenuItem[] = this.oMenuService.dataNbMenuSidebar;
  theme: string = 'default';
  progressBar = {status: 'control', value: 0};
  alert: any = this.oMenuService.http().alert().dataAlert;
  navbarOpen: boolean = this.oMenuService.isNavbarOpen;
  private destroy$: Subject<void> = new Subject<void>();
  private lastUri: string = '/';

  constructor(
    private themeService: NbThemeService,
    private oMenuService: MenuService,
  ) {
    this.theme = this.themeService.currentTheme;
    this.oMenuService.http().storage().object().translate()
      .setDefaultLang(this.oMenuService.http().storage().getEnviroment().lang);

    this.oMenuService.http().alert().object().loading().progress.subscribe(progress => {
      this.progressBar = progress;
    });

    this.oMenuService.http().alert().alert.subscribe((data) => {
      this.alert = data;
    });

    this.oMenuService.change.subscribe(x => {
      this.NbNavbar = this.oMenuService.dataNbMenuBarV;
      this.NbSidebar = this.oMenuService.dataNbMenuSidebar;
    });

    this.oMenuService.navbarOpen.subscribe(x => {
      this.navbarOpen = x;
    });

    this.oMenuService.http().storage().object().router().events.subscribe(x => {
      if (this.oMenuService.http().storage().object().router().url !== this.lastUri) {
        this.lastUri = this.oMenuService.http().storage().object().router().url;
        this.layout = (this.lastUri.startsWith('/panel')) ? 'panel' : 'principal';
      }
    });

    this.themeService.onThemeChange()
      .pipe(
        map(({name}) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => this.theme = themeName);
  }

  /**
   * cierra el sidenav
   */
  public onSidenavClose = () => {
    this.navbarOpen = false;
    this.oMenuService.isNavbarOpen = false;
  }

}
