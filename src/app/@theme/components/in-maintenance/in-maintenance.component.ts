import { NbMenuService } from '@nebular/theme';
import { Component } from '@angular/core';

@Component({
  selector: 'ngx-not-found',
  styleUrls: ['./in-maintenance.component.scss'],
  template: `
    <div class="row">
      <div class="col-md-12">
        <nb-card>
          <nb-card-body>
            <div class="flex-centered col-xl-4 col-lg-6 col-md-8 col-sm-12">
              <h2 class="title">{{'error.in-maintenance' | translate}}</h2>
              <small class="sub-title">{{'error.dont-maintenance' | translate}}</small>
              <button nbButton fullWidth (click)="goToHome()" type="button" class="home-button">
                {{'error.go-home' | translate}}
              </button>
            </div>
          </nb-card-body>
        </nb-card>
      </div>
    </div>
  `,
})
export class InMaintenanceComponent {

  constructor(private menuService: NbMenuService) {
  }

  goToHome() {
    this.menuService.navigateHome();
  }
}
