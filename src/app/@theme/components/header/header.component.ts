import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {LoginService, MenuService} from '../../../@core/utils';
import {Subject} from 'rxjs';
import {LoginComponent, RegisterComponent} from '../../modules';
import {WebSocketSubject} from 'rxjs/webSocket';
import {SystemConfiguration} from '../../../@core/entities';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  template: `
    <div class="header-container">
      <div class="logo-container">
        <a (click)="toggleSidebar()" href="#" class="d-block d-md-none sidebar-toggle" *ngIf="layout === 'panel'">
          <nb-icon icon="menu-2-outline"></nb-icon>
        </a>
        <a (click)="onSidenavToogle()" href="#" class="d-block d-md-none sidebar-toggle" *ngIf="layout !== 'panel'">
          <nb-icon icon="menu-2-outline"></nb-icon>
        </a>
        <a class="w-75" href="#" (click)="navigateHome()">
          <img class="w-50" alt="logo" src="{{ logo }}"/>
        </a>
      </div>
    </div>

    <div class="header-container">
      <ngx-menubar class="d-none d-md-flex"></ngx-menubar>
      <nb-actions size="small">
        <nb-action class="user-action">
          <button mat-button (click)="login()" *ngIf="!logged">
            <nb-icon icon="user-check" pack="solid"></nb-icon> &nbsp;
            <span class="d-none d-sm-inline">{{'app.login' | translate}}</span>
          </button> &nbsp;
          <button mat-button (click)="register()" *ngIf="!logged">
            <nb-icon icon="user-plus" pack="solid"></nb-icon> &nbsp;
            <span class="d-none d-sm-inline">{{'app.register' | translate}}</span>
          </button> &nbsp;
          <button mat-button (click)="logout()" *ngIf="logged">
            <nb-icon icon="sign-out-alt" pack="solid"></nb-icon> &nbsp;
            <span class="d-none d-sm-inline">{{'app.logout' | translate}}</span>
          </button>
          <nb-user [onlyPicture]="userPictureOnly"
                   [name]="user?.name"
                   [picture]="user?.picture"
                   [routerLink]="'/panel'"
                   *ngIf="logged">
          </nb-user>
        </nb-action>
        <nb-action class="control-item" icon="bell-outline" badgeText="{{notifications}}" badgePosition="top right"
                   badgeStatus="danger" *ngIf="notifications > 0"></nb-action>
      </nb-actions>
    </div>
  `,
})
export class HeaderComponent implements OnInit, OnDestroy {

  logo: string = '../../../../assets/icons/icon-512x512.png';
  userPictureOnly: boolean = false;
  logged: boolean = false;
  notifications: number = 0;
  user: { name: string, picture: string };
  @Input() public layout: string = 'principal';
  private notification: WebSocketSubject<any>;
  private destroy$: Subject<void> = new Subject<void>();

  constructor(
    private oMenuService: MenuService,
    private oLoginService: LoginService,
  ) {
    this.logged = this.oLoginService.getLogin();
    this.user = this.oLoginService.getUserPhoto();
    this.getNotification();

    this.oLoginService.login.subscribe(data => {
      this.logged = this.oLoginService.getLogin();
      this.user = this.oLoginService.getUserPhoto();
      this.oMenuService.setMenu();
      this.notification.complete();
      this.getNotification();
    });

    this.oMenuService.http().storage().object().NbIconLibraries().registerFontPack('any', {
      iconClassPrefix: '',
    });
    this.oMenuService.http().storage().object().NbIconLibraries().registerFontPack('solid', {
      packClass: 'fas',
      iconClassPrefix: 'fa',
    });
    this.oMenuService.http().storage().object().NbIconLibraries().registerFontPack('regular', {
      packClass: 'far',
      iconClassPrefix: 'fa',
    });
    this.oMenuService.http().storage().object().NbIconLibraries().registerFontPack('light', {
      packClass: 'fal',
      iconClassPrefix: 'fa',
    });
    this.oMenuService.http().storage().object().NbIconLibraries().registerFontPack('duotone', {
      packClass: 'fad',
      iconClassPrefix: 'fa',
    });
    this.oMenuService.http().storage().object().NbIconLibraries().registerFontPack('brands', {
      packClass: 'fab',
      iconClassPrefix: 'fa',
    });
  }

  /**
   * al arrancar completamente el componente
   */
  ngOnInit() {
    this.oMenuService.setMenu();
    this.oMenuService.http().get<SystemConfiguration>(null, {
      path: 'rest/system_configuration',
      entity: SystemConfiguration,
    }, {}).then((config) => {
      this.oMenuService.http().storage().object().NbTheme().changeTheme(config.systemTheme.name);
      this.oMenuService.http().storage().object().translate().setDefaultLang(config.worldLanguage.iso);
    });
  }

  /**
   * inicia la sesion
   */
  public login = () => {
    this.oMenuService.http().storage().object().dialog().open(LoginComponent);
  }

  /**
   * inicia la sesion
   */
  public register = () => {
    this.oMenuService.http().storage().object().dialog().open(RegisterComponent);
  }

  /**
   * inicia la sesion
   */
  public logout = () => {
    this.oMenuService.router().navigate(['auth/logout'], {replaceUrl: true});
  }

  /**
   * al terminar de renderizar
   */
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  /**
   * lleva a la pagina principal
   */
  navigateHome() {
    this.oMenuService.http().storage().object().NbMenu().navigateHome();

    return false;
  }

  /**
   * cierra/abre el sidenav
   */
  public onSidenavToogle = () => {
    this.oMenuService.toogleNavbar();

    return false;
  }

  /**
   * minimiza/abre el sidebar
   */
  toggleSidebar(): boolean {
    this.oMenuService.http().storage().object().NbSidebar().toggle(true, 'menu-sidebar');
    this.oMenuService.http().storage().object().layout().changeLayoutSize();

    return false;
  }

  /**
   * obtiene las notificaciones a traves de un socket
   */
  private getNotification() {
    this.notification = this.oMenuService.http().getNotifications((data) => {
      this.notifications = data.length;
    });
  }

}
