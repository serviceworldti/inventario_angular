import { NbMenuService } from '@nebular/theme';
import { Component } from '@angular/core';

@Component({
  selector: 'ngx-not-logged',
  styleUrls: ['./not-keep.component.scss'],
  template: `
    <div class="row">
      <div class="col-md-12">
        <nb-card>
          <nb-card-body>
            <div class="flex-centered col-xl-4 col-lg-6 col-md-8 col-sm-12">
              <h2 class="title">{{'error.not-keep' | translate}}</h2>
              <small class="sub-title">{{'error.dont-keep' | translate}}</small>
              <button nbButton fullWidth (click)="goToHome()" type="button" class="home-button">
                {{'error.go-home' | translate}}
              </button>
            </div>
          </nb-card-body>
        </nb-card>
      </div>
    </div>
  `,
})
export class NotKeepComponent {

  constructor(private menuService: NbMenuService) {
  }

  goToHome() {
    this.menuService.navigateHome();
  }
}
