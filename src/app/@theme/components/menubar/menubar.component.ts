import {Component} from '@angular/core';
import {MenuService} from '../../../@core/utils';
import {NbMenuItem} from '@nebular/theme';

@Component({
  selector: 'ngx-menubar',
  styleUrls: ['./menubar.component.scss'],
  template: `
    <nb-menu class="horizontal" [items]="items"></nb-menu>
  `,
})
export class MenubarComponent {

  items: NbMenuItem[] = [];

  constructor(
    private oMenuService: MenuService,
  ) {
    this.items = this.oMenuService.dataNbMenuBarH;

    this.oMenuService.change.subscribe(() => {
      this.items = this.oMenuService.dataNbMenuBarH;
    });

    this.oMenuService.http().storage().object().translate().onTranslationChange.subscribe(() => {
      this.oMenuService.constructMenuBar();
      this.items = this.oMenuService.dataNbMenuBarH;
    });
  }

}
