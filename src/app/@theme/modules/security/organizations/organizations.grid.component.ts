import {Component, OnDestroy} from '@angular/core';
import {GridService} from '../../../../@core/utils';
import {SystemOrganization} from '../../../../@core/entities';
import {OrganizationsComponent} from './organizations.component';

@Component({
  selector: 'ngx-organizations-grid-grid',
  styleUrls: ['./organizations.component.scss'],
  template: `
    <nb-card>
      <nb-card-header>
        {{'app.organization' | translate}}
      </nb-card-header>

      <nb-card-body>
        <p class="pointer">
          <a *ngIf="oGridService.canReport" (click)="oGridService.onReport($event, 'pdf')">
            <i class="fa fa-file-pdf fa-3x"></i>
          </a> &nbsp;
          <a *ngIf="oGridService.canReport" (click)="oGridService.onReport($event, 'xlsx')">
            <i class="fas fa-file-spreadsheet fa-3x"></i>
          </a> &nbsp;
        </p>
        <ng2-smart-table [settings]="oGridService.settings"
                         [source]="oGridService.source"
                         (create)="oGridService.onCreate($event)"
                         (edit)="oGridService.onEdit($event)"
                         (delete)="oGridService.onDelete($event)">
        </ng2-smart-table>
      </nb-card-body>
    </nb-card>`,
})
export class OrganizationsGridComponent implements OnDestroy {

  private alive = true;

  constructor(
    public oGridService: GridService,
  ) {
    this.oGridService.settings.columns = {
      name: {
        title: 'app.name',
        type: 'string',
      },
      email: {
        title: 'app.email',
        type: 'string',
      },
      systemGroup: {
        title: 'app.group',
        type: 'string',
        attribute: 'systemGroup.name',
        valuePrepareFunction: (cell, row) => cell.name,
      },
      status: {
        title: 'app.status',
        type: 'checkbox',
        translate: true,
      },
    };

    this.oGridService.setInit('rest/system_organization', {
      entity: SystemOrganization, update: OrganizationsComponent, view: OrganizationsComponent,
    });

    this.oGridService.list();
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
