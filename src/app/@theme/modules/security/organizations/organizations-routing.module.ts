import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {OrganizationsGridComponent} from './organizations.grid.component';

const routes: Routes = [{
  path: '',
  component: OrganizationsGridComponent,
  children: [],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrganizationsRoutingModule {
}
