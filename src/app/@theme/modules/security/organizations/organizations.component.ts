import {Component, Input, OnDestroy, ViewChild} from '@angular/core';
import {FormGroup, FormGroupDirective} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {FormService, GridService} from '../../../../@core/utils';
import {SystemGroup, SystemOrganization} from '../../../../@core/entities';
import {GridUtil} from '../../../../@core/entities/util/grid.util';

@Component({
  selector: 'ngx-organizations',
  styleUrls: ['./organizations.component.scss'],
  template: `
    <form name="form" (ngSubmit)="f.form.valid && onSubmit()" #f="ngForm" novalidate>
      <nb-card>
        <nb-card-header>{{title | translate}} {{'app.organization' | translate}}</nb-card-header>
        <nb-card-body>

          <div class="form-group row">
            <mat-form-field class="col-md-6">
              <input matInput id="name" name="name" #name="ngModel" [(ngModel)]="model.name"
                     placeholder="{{'app.name' | translate}}" required minlength="4" type="text"/>
              <mat-error *ngIf="name?.errors">{{ oFormService.getErrorMessage(name) }}</mat-error>
            </mat-form-field>

            <mat-form-field class="col-md-6">
              <input matInput id="email" name="email" #email="ngModel" [(ngModel)]="model.email"
                     placeholder="{{'app.email' | translate}}" required minlength="4" type="text"/>
              <mat-error *ngIf="email?.errors">{{ oFormService.getErrorMessage(email) }}</mat-error>
            </mat-form-field>
          </div>

          <mat-form-field class="col-12">
            <input matInput id="code" name="code" #code="ngModel" [(ngModel)]="model.code"
                   placeholder="{{'app.code' | translate}}" required minlength="4" type="text"/>
            <mat-error *ngIf="code?.errors">{{ oFormService.getErrorMessage(code) }}</mat-error>
          </mat-form-field>

          <mat-form-field class="col-12">
            <mat-label>{{'app.groups' | translate}}</mat-label>
            <mat-select placeholder="{{'app.groups' | translate}}" name="systemGroup"
                        #systemGroup="ngModel" [compareWith]="oFormService.compareFn"
                        [(ngModel)]="model.systemGroup">
              <mat-option *ngFor="let o of groups" [value]="o"> {{o.name}} </mat-option>
            </mat-select>
            <mat-error *ngIf="systemGroup?.errors">
              {{ oFormService.getErrorMessage(systemGroup) }}
            </mat-error>
          </mat-form-field>

          <div class="form-group row">
            <div class="form-check">
              <mat-slide-toggle id="status" name="status" [(ngModel)]="model.status" #status="ngModel"
                                ng-true-value="'t'"
                                ng-false-value="'f'"> {{'app.status' | translate}} </mat-slide-toggle>
            </div>
          </div>

        </nb-card-body>
        <nb-card-footer>
          <button type="button" nbButton status="danger" (click)="cancel()">{{'app.cancel' | translate}}</button>
          <button nbButton hero status="success" class="float-right"
                  [disabled]="!f.form.valid">{{'app.save' | translate}}</button>
        </nb-card-footer>
      </nb-card>
    </form>
  `,
})
export class OrganizationsComponent implements OnDestroy {

  public form: FormGroup;
  public groups: SystemGroup[] = [];
  @ViewChild(FormGroupDirective, {static: false}) public formDirective: FormGroupDirective;
  @Input() public title: string;
  @Input() public model: SystemOrganization = new SystemOrganization();
  private alive = true;

  constructor(
    protected ref: NbDialogRef<OrganizationsComponent>,
    public oFormService: FormService,
    private oGridService: GridService,
  ) {
    this.oGridService.http().list(null, {
      path: 'rest/system_group',
      entity: GridUtil,
    }).then((result: GridUtil) => {
      this.groups = result.deepCopyData(SystemGroup);
    });
  }

  /**
   * realiza la peticion del login
   */
  public onSubmit(): void {
    const t = this;

    this.oGridService.saveOrUpdate(this.model).then(() => {
      this.cancel();
    });
  }

  /**
   * cierra la modal del login
   */
  public cancel(): void {
    this.ref.close();
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
