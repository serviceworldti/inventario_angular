import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {PermissionsGridComponent} from './permissions.grid.component';

const routes: Routes = [{
  path: '',
  component: PermissionsGridComponent,
  children: [],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PermissionsRoutingModule {
}
