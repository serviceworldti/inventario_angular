import {Component, OnDestroy} from '@angular/core';
import {GridService} from '../../../../@core/utils';
import {SystemPermission} from '../../../../@core/entities';
import {PermissionsComponent} from './permissions.component';

@Component({
  selector: 'ngx-permissions-grid',
  styleUrls: ['./permissions.component.scss'],
  template: `
    <nb-card>
      <nb-card-header>
        {{'app.permission' | translate}}
      </nb-card-header>

      <nb-card-body>
        <p class="pointer">
          <a *ngIf="oGridService.canReport" (click)="oGridService.onReport($event, 'pdf')">
            <i class="fa fa-file-pdf fa-3x"></i>
          </a> &nbsp;
          <a *ngIf="oGridService.canReport" (click)="oGridService.onReport($event, 'xlsx')">
            <i class="fas fa-file-spreadsheet fa-3x"></i>
          </a> &nbsp;
        </p>
        <ng2-smart-table [settings]="oGridService.settings"
                         [source]="oGridService.source"
                         (create)="oGridService.onCreate($event)"
                         (edit)="oGridService.onEdit($event)"
                         (delete)="oGridService.onDelete($event)">
        </ng2-smart-table>
      </nb-card-body>
    </nb-card>`,
})
export class PermissionsGridComponent implements OnDestroy {

  private alive = true;

  constructor(
    public oGridService: GridService,
  ) {
    this.oGridService.settings.columns = {
      name: {
        title: 'app.name',
        type: 'string',
      },
      uri: {
        title: 'app.uri',
        type: 'string',
      },
      icon: {
        title: 'app.icon',
        type: 'string',
      },
      keepLogin: {
        title: 'app.keep-login',
        type: 'boolean',
        translate: true,
      },
      showInNavbar: {
        title: 'app.show-in-navbar',
        type: 'boolean',
        translate: true,
      },
      showInSidebar: {
        title: 'app.show-in-sidebar',
        type: 'boolean',
        translate: true,
      },
      logged: {
        title: 'app.logged',
        type: 'boolean',
        translate: true,
      },
      status: {
        title: 'app.status',
        type: 'checkbox',
        translate: true,
      },
    };

    this.oGridService.setInit('rest/system_permission', {
      entity: SystemPermission, update: PermissionsComponent, view: PermissionsComponent,
    });

    this.oGridService.list();
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
