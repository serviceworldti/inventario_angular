import {Component, Input, OnDestroy, ViewChild} from '@angular/core';
import {FormGroup, FormGroupDirective} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {FormService, GridService} from '../../../../@core/utils';
import {SystemPermission} from '../../../../@core/entities';
import {GridUtil} from '../../../../@core/entities/util/grid.util';

@Component({
  selector: 'ngx-permissions',
  styleUrls: ['./permissions.component.scss'],
  template: `
    <form name="form" (ngSubmit)="f.form.valid && onSubmit()" #f="ngForm" novalidate>
      <nb-card>
        <nb-card-header>{{title | translate}} {{'app.permission' | translate}}</nb-card-header>
        <nb-card-body>

          <div class="form-group row">
            <mat-form-field class="col-6">
              <input matInput id="name" name="name" #name="ngModel" [(ngModel)]="model.name"
                     placeholder="{{'app.name' | translate}}" required minlength="4" type="text"/>
              <mat-error *ngIf="name?.errors">{{ oFormService.getErrorMessage(name) }}</mat-error>
            </mat-form-field>

            <mat-form-field class="col-6">
              <input matInput id="label" name="label" #label="ngModel" [(ngModel)]="model.label"
                     placeholder="{{'app.label' | translate}}" required minlength="4" type="text"/>
              <mat-error *ngIf="label?.errors">{{ oFormService.getErrorMessage(label) }}</mat-error>
            </mat-form-field>
          </div>

          <div class="form-group row">
            <mat-form-field class="col-md-6">
              <input matInput id="uri" name="uri" #uri="ngModel" [(ngModel)]="model.uri"
                     placeholder="{{'app.uri' | translate}}" required minlength="4" type="text"/>
              <mat-error *ngIf="uri?.errors">{{ oFormService.getErrorMessage(uri) }}</mat-error>
            </mat-form-field>

            <mat-form-field class="col-md-6">
              <input matInput id="icon" name="icon" #icon="ngModel" [(ngModel)]="model.icon"
                     placeholder="{{'app.icon' | translate}}" required minlength="4" type="text"/>
              <mat-error *ngIf="icon?.errors">{{ oFormService.getErrorMessage(icon) }}</mat-error>
            </mat-form-field>
          </div>

          <mat-form-field class="col-12">
            <mat-label>{{'pages.permissions' | translate}}</mat-label>
            <mat-select placeholder="{{'pages.permissions' | translate}}" name="systemPermission"
                        #systemPermission="ngModel" [compareWith]="oFormService.compareFn"
                        [(ngModel)]="model.systemPermission" multiple="multiple">
              <mat-option *ngFor="let o of permissions" [value]="o"> {{o.name}} </mat-option>
            </mat-select>
            <mat-error *ngIf="systemPermission?.errors">
              {{ oFormService.getErrorMessage(systemPermission) }}
            </mat-error>
          </mat-form-field>

          <div class="form-group row">
            <div class="form-group row col-md-4">
              <div class="form-check">
                <mat-slide-toggle id="status" name="status" [(ngModel)]="model.status" #status="ngModel"
                                  ng-true-value="'t'"
                                  ng-false-value="'f'"> {{'app.status' | translate}} </mat-slide-toggle>
              </div>
            </div>

            <div class="form-group row col-md-4">
              <div class="form-check">
                <mat-slide-toggle id="showInNavbar" name="showInNavbar" [(ngModel)]="model.showInNavbar"
                                  #showInNavbar="ngModel"
                                  ng-true-value="'t'"
                                  ng-false-value="'f'"> {{'app.show-in-navbar' | translate}} </mat-slide-toggle>
              </div>
            </div>

            <div class="form-group row col-md-4">
              <div class="form-check">
                <mat-slide-toggle id="showInSidebar" name="showInSidebar" [(ngModel)]="model.showInSidebar"
                                  #showInSidebar="ngModel"
                                  ng-true-value="'t'"
                                  ng-false-value="'f'"> {{'app.show-in-sidebar' | translate}} </mat-slide-toggle>
              </div>
            </div>
          </div>

          <div class="form-group row">
            <div class="form-group row col-md-4">
              <div class="form-check">
                <mat-slide-toggle id="logged" name="logged" [(ngModel)]="model.logged" #logged="ngModel"
                                  ng-true-value="'t'"
                                  ng-false-value="'f'"> {{'app.logged' | translate}} </mat-slide-toggle>
              </div>
            </div>

            <div class="form-group row col-md-4">
              <div class="form-check">
                <mat-slide-toggle id="keepLogin" name="keepLogin" [(ngModel)]="model.keepLogin" #keepLogin="ngModel"
                                  ng-true-value="'t'"
                                  ng-false-value="'f'"> {{'app.keep-login' | translate}} </mat-slide-toggle>
              </div>
            </div>
          </div>

        </nb-card-body>
        <nb-card-footer>
          <button type="button" nbButton status="danger" (click)="cancel()">{{'app.cancel' | translate}}</button>
          <button nbButton hero status="success" class="float-right"
                  [disabled]="!f.form.valid">{{'app.save' | translate}}</button>
        </nb-card-footer>
      </nb-card>
    </form>
  `,
})
export class PermissionsComponent implements OnDestroy {

  public form: FormGroup;
  public permissions: SystemPermission[] = [];
  @ViewChild(FormGroupDirective, {static: false}) public formDirective: FormGroupDirective;
  @Input() public title: string;
  @Input() public model: SystemPermission = new SystemPermission();
  private alive = true;

  constructor(
    protected ref: NbDialogRef<PermissionsComponent>,
    public oFormService: FormService,
    private oGridService: GridService,
  ) {
    this.oGridService.http().list(null, {
      path: 'rest/system_permission',
      entity: GridUtil,
    }).then((result: GridUtil) => {
      this.permissions = result.deepCopyData(SystemPermission);
    });
  }

  /**
   * realiza la peticion del login
   */
  public onSubmit(): void {
    const t = this;

    this.oGridService.saveOrUpdate(this.model).then(() => {
      this.cancel();
    });
  }

  /**
   * cierra la modal del login
   */
  public cancel(): void {
    this.ref.close();
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
