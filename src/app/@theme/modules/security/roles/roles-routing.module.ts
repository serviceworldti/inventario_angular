import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {RolesGridComponent} from './roles.grid.component';

const routes: Routes = [{
  path: '',
  component: RolesGridComponent,
  children: [],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RolesRoutingModule {
}
