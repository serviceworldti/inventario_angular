import {Component, Input, OnDestroy, ViewChild} from '@angular/core';
import {FormGroup, FormGroupDirective} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {FormService, GridService} from '../../../../@core/utils';
import {SystemAssignment, SystemPermission, SystemRole} from '../../../../@core/entities';
import {GridUtil} from '../../../../@core/entities/util/grid.util';

@Component({
  selector: 'ngx-roles',
  styleUrls: ['./roles.component.scss'],
  template: `
    <form name="form" (ngSubmit)="f.form.valid && onSubmit()" #f="ngForm" novalidate>
      <nb-card>
        <nb-card-header>{{title | translate}} {{'app.role' | translate}}</nb-card-header>
        <nb-card-body>

          <mat-form-field class="col-12">
            <input matInput id="name" name="name" #name="ngModel" [(ngModel)]="model.name"
                   placeholder="{{'app.name' | translate}}" required minlength="4" type="text"/>
            <mat-error *ngIf="name?.errors">{{ oFormService.getErrorMessage(name) }}</mat-error>
          </mat-form-field>

          <mat-form-field class="col-12">
            <mat-label>{{'app.assignments' | translate}}</mat-label>
            <mat-select placeholder="{{'app.assignments' | translate}}" name="systemAssignments"
                        #systemAssignments="ngModel" [compareWith]="oFormService.compareFn"
                        [(ngModel)]="model.systemAssignments" required multiple>
              <mat-option *ngFor="let o of assignments" [value]="o"> {{o.name}} </mat-option>
            </mat-select>
            <mat-error *ngIf="systemAssignments?.errors">
              {{ oFormService.getErrorMessage(systemAssignments) }}
            </mat-error>
          </mat-form-field>

          <mat-form-field class="col-12">
            <mat-label>{{'app.permissions' | translate}}</mat-label>
            <mat-select placeholder="{{'app.permissions' | translate}}" name="systemPermissions"
                        #systemPermissions="ngModel" [compareWith]="oFormService.compareFn"
                        [(ngModel)]="model.systemPermissions" required multiple>
              <mat-option *ngFor="let o of permissions" [value]="o"> {{o.name}} </mat-option>
            </mat-select>
            <mat-error *ngIf="systemPermissions?.errors">
              {{ oFormService.getErrorMessage(systemPermissions) }}
            </mat-error>
          </mat-form-field>

          <div class="form-group row">
            <div class="form-check">
              <mat-slide-toggle id="status" name="status" [(ngModel)]="model.status" #status="ngModel"
                                ng-true-value="'t'"
                                ng-false-value="'f'"> {{'app.status' | translate}} </mat-slide-toggle>
            </div>
          </div>

        </nb-card-body>
        <nb-card-footer>
          <button type="button" nbButton status="danger" (click)="cancel()">{{'app.cancel' | translate}}</button>
          <button nbButton hero status="success" class="float-right"
                  [disabled]="!f.form.valid">{{'app.save' | translate}}</button>
        </nb-card-footer>
      </nb-card>
    </form>
  `,
})
export class RolesComponent implements OnDestroy {

  public form: FormGroup;
  public permissions: SystemPermission[] = [];
  public assignments: SystemAssignment[] = [];
  @ViewChild(FormGroupDirective, {static: false}) public formDirective: FormGroupDirective;
  @Input() public title: string;
  @Input() public model: SystemRole = new SystemRole();
  private alive = true;

  constructor(
    protected ref: NbDialogRef<RolesComponent>,
    public oFormService: FormService,
    private oGridService: GridService,
  ) {
    this.oGridService.http().list(null, {
      path: 'rest/system_permission',
      entity: GridUtil,
    }).then((result: GridUtil) => {
      this.permissions = result.deepCopyData(SystemPermission);
    });

    this.oGridService.http().list(null, {
      path: 'rest/system_assignment',
      entity: GridUtil,
    }).then((result: GridUtil) => {
      this.assignments = result.deepCopyData(SystemAssignment);
    });
  }

  /**
   * realiza la peticion del login
   */
  public onSubmit(): void {
    const t = this;

    this.oGridService.saveOrUpdate(this.model).then(() => {
      this.cancel();
    });
  }

  /**
   * cierra la modal del login
   */
  public cancel(): void {
    this.ref.close();
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
