import {Component, Input, OnDestroy, ViewChild} from '@angular/core';
import {FormGroup, FormGroupDirective} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {FormService, GridService} from '../../../../@core/utils';
import {
  SystemConfiguration,
  SystemOrganization,
  SystemReportConfiguration,
  SystemTheme,
  WorldLanguage,
} from '../../../../@core/entities';
import {GridUtil} from '../../../../@core/entities/util/grid.util';

@Component({
  selector: 'ngx-configurations',
  styleUrls: ['./configurations.component.scss'],
  template: `
    <form name="form" (ngSubmit)="f.form.valid && onSubmit()" #f="ngForm" novalidate>
      <nb-card>
        <nb-card-header>{{title | translate}} {{'app.configuration' | translate}}</nb-card-header>
        <nb-card-body>

          <mat-form-field class="col-12">
            <mat-label>{{'app.organizations' | translate}}</mat-label>
            <mat-select placeholder="{{'app.organizations' | translate}}" name="systemOrganization"
                        #systemOrganization="ngModel" [compareWith]="oFormService.compareFn"
                        [(ngModel)]="model.systemOrganization" required>
              <mat-option *ngFor="let o of organizations" [value]="o"> {{o.name}} </mat-option>
            </mat-select>
          </mat-form-field>

          <mat-form-field class="col-12">
            <mat-label>{{'app.themes' | translate}}</mat-label>
            <mat-select placeholder="{{'app.themes' | translate}}" name="systemTheme"
                        #systemTheme="ngModel" [compareWith]="oFormService.compareFn"
                        [(ngModel)]="model.systemTheme" required>
              <mat-option *ngFor="let o of themes" [value]="o"> {{o.name}} </mat-option>
            </mat-select>
          </mat-form-field>

          <mat-form-field class="col-12">
            <mat-label>{{'app.languages' | translate}}</mat-label>
            <mat-select placeholder="{{'app.languages' | translate}}" name="worldLanguage"
                        #worldLanguage="ngModel" [compareWith]="oFormService.compareFn"
                        [(ngModel)]="model.worldLanguage" required>
              <mat-option *ngFor="let o of languages" [value]="o"> {{o.name}} </mat-option>
            </mat-select>
            <mat-error *ngIf="worldLanguage?.errors">
              {{ oFormService.getErrorMessage(worldLanguage) }}
            </mat-error>
          </mat-form-field>

          <mat-form-field class="col-12">
            <mat-label>{{'app.reports' | translate}}</mat-label>
            <mat-select placeholder="{{'app.reports' | translate}}" name="systemReportConfigurations"
                        #systemReportConfigurations="ngModel" [compareWith]="oFormService.compareFn"
                        [(ngModel)]="model.systemReportConfigurations" multiple>
              <mat-option *ngFor="let o of reports" [value]="o"> {{o.name}} </mat-option>
            </mat-select>
            <mat-error *ngIf="systemReportConfigurations?.errors">
              {{ oFormService.getErrorMessage(systemReportConfigurations) }}
            </mat-error>
          </mat-form-field>

        </nb-card-body>
        <nb-card-footer>
          <button type="button" nbButton status="danger" (click)="cancel()">{{'app.cancel' | translate}}</button>
          <button nbButton hero status="success" class="float-right"
                  [disabled]="!f.form.valid">{{'app.save' | translate}}</button>
        </nb-card-footer>
      </nb-card>
    </form>
  `,
})
export class ConfigurationsComponent implements OnDestroy {

  public form: FormGroup;
  public organizations: SystemOrganization[] = [];
  public themes: SystemTheme[] = [];
  public languages: WorldLanguage[] = [];
  public reports: SystemReportConfiguration[] = [];
  @ViewChild(FormGroupDirective, {static: false}) public formDirective: FormGroupDirective;
  @Input() public title: string;
  @Input() public model: SystemConfiguration = new SystemConfiguration();
  private alive = true;

  constructor(
    protected ref: NbDialogRef<ConfigurationsComponent>,
    public oFormService: FormService,
    private oGridService: GridService,
  ) {
    this.oGridService.http().list(null, {
      path: 'rest/system_organization',
      entity: GridUtil,
    }).then((result: GridUtil) => {
      this.organizations = result.deepCopyData(SystemOrganization);
    });

    this.oGridService.http().list(null, {
      path: 'rest/system_theme',
      entity: GridUtil,
    }).then((result: GridUtil) => {
      this.themes = result.deepCopyData(SystemTheme);
    });

    this.oGridService.http().list(null, {
      path: 'rest/world_language',
      entity: GridUtil,
    }).then((result: GridUtil) => {
      this.languages = result.deepCopyData(WorldLanguage);
    });

    this.oGridService.http().list(null, {
      path: 'rest/system_report_configuration',
      entity: GridUtil,
    }).then((result: GridUtil) => {
      this.reports = result.deepCopyData(SystemReportConfiguration);
    });
  }

  /**
   * realiza la peticion del login
   */
  public onSubmit(): void {
    const t = this;

    this.oGridService.saveOrUpdate(this.model).then(() => {
      this.cancel();
    });
  }

  /**
   * cierra la modal del login
   */
  public cancel(): void {
    this.ref.close();
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
