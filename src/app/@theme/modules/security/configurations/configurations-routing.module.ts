import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ConfigurationsGridComponent} from './configurations.grid.component';

const routes: Routes = [{
  path: '',
  component: ConfigurationsGridComponent,
  children: [],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfigurationsRoutingModule {
}
