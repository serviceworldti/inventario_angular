import {Component, OnDestroy} from '@angular/core';
import {GridService} from '../../../../@core/utils';
import {SystemConfiguration} from '../../../../@core/entities';
import {ConfigurationsComponent} from './configurations.component';

@Component({
  selector: 'ngx-configurations-grid',
  styleUrls: ['./configurations.component.scss'],
  template: `
    <nb-card>
      <nb-card-header>
        {{'app.configuration' | translate}}
      </nb-card-header>

      <nb-card-body>
        <p class="pointer">
          <a *ngIf="oGridService.canReport" (click)="oGridService.onReport($event, 'pdf')">
            <i class="fa fa-file-pdf fa-3x"></i>
          </a> &nbsp;
          <a *ngIf="oGridService.canReport" (click)="oGridService.onReport($event, 'xlsx')">
            <i class="fas fa-file-spreadsheet fa-3x"></i>
          </a> &nbsp;
        </p>
        <ng2-smart-table [settings]="oGridService.settings"
                         [source]="oGridService.source"
                         (create)="oGridService.onCreate($event)"
                         (edit)="oGridService.onEdit($event)"
                         (delete)="oGridService.onDelete($event)">
        </ng2-smart-table>
      </nb-card-body>
    </nb-card>`,
})
export class ConfigurationsGridComponent implements OnDestroy {

  private alive = true;

  constructor(
    public oGridService: GridService,
  ) {
    this.oGridService.settings.columns = {
      systemOrganization: {
        title: 'app.organization',
        type: 'list-multiple',
        attribute: 'systemOrganization.name',
        valuePrepareFunction: (cell, row) => cell.name,
      },
      systemTheme: {
        title: 'app.theme',
        type: 'list-multiple',
        attribute: 'systemTheme.name',
        valuePrepareFunction: (cell, row) => cell.name,
      },
      worldLanguage: {
        title: 'app.language',
        type: 'list-multiple',
        attribute: 'worldLanguage.name',
        valuePrepareFunction: (cell, row) => cell.name,
      },
    };

    this.oGridService.setInit('rest/system_configuration', {
      entity: SystemConfiguration, update: ConfigurationsComponent, view: ConfigurationsComponent,
    });

    this.oGridService.list();
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
