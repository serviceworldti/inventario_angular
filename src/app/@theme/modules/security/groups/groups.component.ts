import {Component, Input, OnDestroy, ViewChild} from '@angular/core';
import {FormGroup, FormGroupDirective} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {FormService, GridService} from '../../../../@core/utils';
import {SystemGroup, SystemRole} from '../../../../@core/entities';
import {GridUtil} from '../../../../@core/entities/util/grid.util';

@Component({
  selector: 'ngx-groups',
  styleUrls: ['./groups.component.scss'],
  template: `
    <form name="form" (ngSubmit)="f.form.valid && onSubmit()" #f="ngForm" novalidate>
      <nb-card>
        <nb-card-header>{{title | translate}} {{'app.group' | translate}}</nb-card-header>
        <nb-card-body>

          <mat-form-field class="col-12">
            <input matInput id="name" name="name" #name="ngModel" [(ngModel)]="model.name"
                   placeholder="{{'app.name' | translate}}" required minlength="4" type="text"/>
            <mat-error *ngIf="name?.errors">{{ oFormService.getErrorMessage(name) }}</mat-error>
          </mat-form-field>

          <mat-form-field class="col-12">
            <mat-label>{{'app.roles' | translate}}</mat-label>
            <mat-select placeholder="{{'app.roles' | translate}}" name="systemRole"
                        #systemRole="ngModel" [compareWith]="oFormService.compareFn" multiple="multiple"
                        [(ngModel)]="model.systemRoles">
              <mat-option *ngFor="let o of roles" [value]="o"> {{o.name}} </mat-option>
            </mat-select>
            <mat-error *ngIf="systemRole?.errors">
              {{ oFormService.getErrorMessage(systemRole) }}
            </mat-error>
          </mat-form-field>

          <div class="form-group row">
            <div class="form-check">
              <mat-slide-toggle id="status" name="status" [(ngModel)]="model.status" #status="ngModel"
                                ng-true-value="'t'"
                                ng-false-value="'f'"> {{'app.status' | translate}} </mat-slide-toggle>
            </div>
          </div>

        </nb-card-body>
        <nb-card-footer>
          <button type="button" nbButton status="danger" (click)="cancel()">{{'app.cancel' | translate}}</button>
          <button nbButton hero status="success" class="float-right"
                  [disabled]="!f.form.valid">{{'app.save' | translate}}</button>
        </nb-card-footer>
      </nb-card>
    </form>
  `,
})
export class GroupsComponent implements OnDestroy {

  public form: FormGroup;
  public roles: SystemRole[] = [];
  @ViewChild(FormGroupDirective, {static: false}) public formDirective: FormGroupDirective;
  @Input() public title: string;
  @Input() public model: SystemGroup = new SystemGroup();
  private alive = true;

  constructor(
    protected ref: NbDialogRef<GroupsComponent>,
    public oFormService: FormService,
    private oGridService: GridService,
  ) {
    this.oGridService.http().list(null, {
      path: 'rest/system_role',
      entity: GridUtil,
    }).then((result: GridUtil) => {
      this.roles = result.deepCopyData(SystemRole);
    });
  }

  /**
   * realiza la peticion del login
   */
  public onSubmit(): void {
    const t = this;

    this.oGridService.saveOrUpdate(this.model).then(() => {
      this.cancel();
    });
  }

  /**
   * cierra la modal del login
   */
  public cancel(): void {
    this.ref.close();
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
