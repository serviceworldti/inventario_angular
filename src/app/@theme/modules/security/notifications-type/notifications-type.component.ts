import {Component, Input, OnDestroy, ViewChild} from '@angular/core';
import {FormGroup, FormGroupDirective} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {FormService, GridService} from '../../../../@core/utils';
import {SystemNotificationType} from '../../../../@core/entities';

@Component({
  selector: 'ngx-notifications-type',
  styleUrls: ['./notifications-type.component.scss'],
  template: `
    <form name="form" (ngSubmit)="f.form.valid && onSubmit()" #f="ngForm" novalidate>
      <nb-card>
        <nb-card-header>{{title | translate}} {{'app.notification-type' | translate}}</nb-card-header>
        <nb-card-body>

          <mat-form-field class="col-md-6">
            <input matInput id="name" name="name" #name="ngModel" [(ngModel)]="model.name"
                   placeholder="{{'app.name' | translate}}" required minlength="4" type="text"/>
            <mat-error *ngIf="name?.errors">{{ oFormService.getErrorMessage(name) }}</mat-error>
          </mat-form-field>

          <div class="form-group row">
            <div class="form-check">
              <mat-slide-toggle id="status" name="status" [(ngModel)]="model.status" #status="ngModel"
                                ng-true-value="'t'"
                                ng-false-value="'f'"> {{'app.status' | translate}} </mat-slide-toggle>
            </div>
          </div>

        </nb-card-body>
        <nb-card-footer>
          <button type="button" nbButton status="danger" (click)="cancel()">{{'app.cancel' | translate}}</button>
          <button nbButton hero status="success" class="float-right"
                  [disabled]="!f.form.valid">{{'app.save' | translate}}</button>
        </nb-card-footer>
      </nb-card>
    </form>
  `,
})
export class NotificationsTypeComponent implements OnDestroy {

  public form: FormGroup;
  @ViewChild(FormGroupDirective, {static: false}) public formDirective: FormGroupDirective;
  @Input() public title: string;
  @Input() public model: SystemNotificationType = new SystemNotificationType();
  private alive = true;

  constructor(
    protected ref: NbDialogRef<NotificationsTypeComponent>,
    public oFormService: FormService,
    private oGridService: GridService,
  ) {
  }

  /**
   * realiza la peticion del login
   */
  public onSubmit(): void {
    const t = this;

    this.oGridService.saveOrUpdate(this.model).then(() => {
      this.cancel();
    });
  }

  /**
   * cierra la modal del login
   */
  public cancel(): void {
    this.ref.close();
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
