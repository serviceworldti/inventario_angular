import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {NotificationsTypeGridComponent} from './notifications-type.grid.component';

const routes: Routes = [{
  path: '',
  component: NotificationsTypeGridComponent,
  children: [],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotificationsTypeRoutingModule {
}
