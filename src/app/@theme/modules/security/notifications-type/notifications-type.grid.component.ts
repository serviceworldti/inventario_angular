import {Component, OnDestroy} from '@angular/core';
import {GridService} from '../../../../@core/utils';
import {SystemNotificationType} from '../../../../@core/entities';
import {NotificationsTypeComponent} from './notifications-type.component';

@Component({
  selector: 'ngx-notifications-type-grid',
  styleUrls: ['./notifications-type.component.scss'],
  template: `
    <nb-card>
      <nb-card-header>
        {{'app.notification-type' | translate}}
      </nb-card-header>

      <nb-card-body>
        <p class="pointer">
          <a *ngIf="oGridService.canReport" (click)="oGridService.onReport($event, 'pdf')">
            <i class="fa fa-file-pdf fa-3x"></i>
          </a> &nbsp;
          <a *ngIf="oGridService.canReport" (click)="oGridService.onReport($event, 'xlsx')">
            <i class="fas fa-file-spreadsheet fa-3x"></i>
          </a> &nbsp;
        </p>
        <ng2-smart-table [settings]="oGridService.settings"
                         [source]="oGridService.source"
                         (create)="oGridService.onCreate($event)"
                         (edit)="oGridService.onEdit($event)"
                         (delete)="oGridService.onDelete($event)">
        </ng2-smart-table>
      </nb-card-body>
    </nb-card>`,
})
export class NotificationsTypeGridComponent implements OnDestroy {

  private alive = true;

  constructor(
    public oGridService: GridService,
  ) {
    this.oGridService.settings.columns = {
      name: {
        title: 'app.name',
        type: 'string',
      },
      status: {
        title: 'app.status',
        type: 'checkbox',
        translate: true,
      },
    };

    this.oGridService.setInit('rest/system_notification_type', {
      entity: SystemNotificationType, update: NotificationsTypeComponent, view: NotificationsTypeComponent,
    });

    this.oGridService.list();
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
