import {Component} from '@angular/core';

@Component({
  selector: 'ngx-panel',
  styleUrls: ['security.component.scss'],
  template: `
    <router-outlet></router-outlet>
  `,
})
export class SecurityComponent {
}
