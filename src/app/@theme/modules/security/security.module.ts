import {NgModule} from '@angular/core';
import {NbMenuModule} from '@nebular/theme';

import {ThemeModule} from '../../theme.module';
import {SecurityComponent} from './security.component';
import {PermissionsModule} from './permissions/permissions.module';
import {SecurityRoutingModule} from './security-routing.module';

@NgModule({
  imports: [
    SecurityRoutingModule,
    ThemeModule,
    NbMenuModule,
    PermissionsModule,
  ],
  declarations: [
    SecurityComponent,
  ],
})
export class SecurityModule {
}
