import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

import {AccessPanelRootService} from '../../../@core/mock';
import {NotFoundComponent} from '../../components';
import {SecurityComponent} from './security.component';

const routes: Routes = [{
  path: '',
  component: SecurityComponent,
  canActivate: [AccessPanelRootService],
  children: [
    {
      path: 'assignment',
      loadChildren: () => import('./assignments/assignments.module').then(m => m.AssignmentsModule),
    },
    {
      path: 'audit',
      loadChildren: () => import('./audits/audits.module').then(m => m.AuditsModule),
    },
    {
      path: 'configuration',
      loadChildren: () => import('./configurations/configurations.module').then(m => m.ConfigurationsModule),
    },
    {
      path: 'group',
      loadChildren: () => import('./groups/groups.module').then(m => m.GroupsModule),
    },
    {
      path: 'notification-type',
      loadChildren: () => import('./notifications-type/notifications-type.module').then(m => m.NotificationsTypeModule),
    },
    {
      path: 'organization',
      loadChildren: () => import('./organizations/organizations.module').then(m => m.OrganizationsModule),
    },
    {
      path: 'permission',
      loadChildren: () => import('./permissions/permissions.module').then(m => m.PermissionsModule),
    },
    {
      path: 'role',
      loadChildren: () => import('./roles/roles.module').then(m => m.RolesModule),
    },
    {
      path: 'report-configuration',
      loadChildren: () => import('./reports-configuration/reports-configuration.module')
        .then(m => m.ReportsConfigurationModule),
    },
    {
      path: 'theme',
      loadChildren: () => import('./themes/themes.module').then(m => m.ThemesModule),
    },
    {
      path: 'user',
      loadChildren: () => import('./users/users.module').then(m => m.UsersModule),
    },
    {
      path: 'user-configuration',
      loadChildren: () => import('./users-configuration/users-configuration.module')
        .then(m => m.UsersConfigurationModule),
    },
    {
      path: 'user-status',
      loadChildren: () => import('./users-status/users-status.module').then(m => m.UsersStatusModule),
    },
    {
      path: '',
      redirectTo: 'user',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SecurityRoutingModule {
}
