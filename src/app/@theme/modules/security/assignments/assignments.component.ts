import {Component, Input, OnDestroy, ViewChild} from '@angular/core';
import {FormGroup, FormGroupDirective} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {FormService, GridService} from '../../../../@core/utils';
import {SystemAssignment, SystemPermission} from '../../../../@core/entities';
import {GridUtil} from '../../../../@core/entities/util/grid.util';

@Component({
  selector: 'ngx-assignments',
  styleUrls: ['./assignments.component.scss'],
  template: `
    <form name="form" (ngSubmit)="f.form.valid && onSubmit()" #f="ngForm" novalidate>
      <nb-card>
        <nb-card-header>{{title | translate}} {{'app.assignment' | translate}}</nb-card-header>
        <nb-card-body>

          <div class="form-group row">
            <mat-form-field class="col-md-6">
              <input matInput id="name" name="name" #name="ngModel" [(ngModel)]="model.name"
                     placeholder="{{'app.name' | translate}}" required minlength="4" type="text"/>
              <mat-error *ngIf="name?.errors">{{ oFormService.getErrorMessage(name) }}</mat-error>
            </mat-form-field>

            <mat-form-field class="col-md-6">
              <input matInput id="method" name="method" #method="ngModel" [(ngModel)]="model.method"
                     placeholder="{{'app.method' | translate}}" required minlength="4" type="text"/>
              <mat-error *ngIf="method?.errors">{{ oFormService.getErrorMessage(method) }}</mat-error>
            </mat-form-field>
          </div>

          <mat-form-field class="col-12">
            <mat-label>{{'app.permissions' | translate}}</mat-label>
            <mat-select placeholder="{{'app.permissions' | translate}}" name="systemPermission"
                        #systemPermission="ngModel" [compareWith]="oFormService.compareFn"
                        [(ngModel)]="model.systemPermission">
              <mat-option *ngFor="let o of permissions" [value]="o"> {{o.name}} </mat-option>
            </mat-select>
            <mat-error *ngIf="systemPermission?.errors">
              {{ oFormService.getErrorMessage(systemPermission) }}
            </mat-error>
          </mat-form-field>

          <div class="form-group row">
            <div class="form-check">
              <mat-slide-toggle id="status" name="status" [(ngModel)]="model.status" #status="ngModel"
                                ng-true-value="'t'"
                                ng-false-value="'f'"> {{'app.status' | translate}} </mat-slide-toggle>
            </div>
          </div>

          <div class="form-group row">
            <div class="form-check">
              <mat-slide-toggle id="showInMenu" name="showInMenu" [(ngModel)]="model.showInMenu" #showInMenu="ngModel"
                                ng-true-value="'t'"
                                ng-false-value="'f'"> {{'app.show-in-menu' | translate}} </mat-slide-toggle>
            </div>
          </div>

        </nb-card-body>
        <nb-card-footer>
          <button type="button" nbButton status="danger" (click)="cancel()">{{'app.cancel' | translate}}</button>
          <button nbButton hero status="success" class="float-right"
                  [disabled]="!f.form.valid">{{'app.save' | translate}}</button>
        </nb-card-footer>
      </nb-card>
    </form>
  `,
})
export class AssignmentsComponent implements OnDestroy {

  public form: FormGroup;
  public permissions: SystemPermission[] = [];
  @ViewChild(FormGroupDirective, {static: false}) public formDirective: FormGroupDirective;
  @Input() public title: string;
  @Input() public model: SystemAssignment = new SystemAssignment();
  private alive = true;

  constructor(
    protected ref: NbDialogRef<AssignmentsComponent>,
    public oFormService: FormService,
    private oGridService: GridService,
  ) {
    this.oGridService.http().list(null, {
      path: 'rest/system_permission',
      entity: GridUtil,
    }).then((result: GridUtil) => {
      this.permissions = result.deepCopyData(SystemPermission);
    });
  }

  /**
   * realiza la peticion del login
   */
  public onSubmit(): void {
    this.oGridService.saveOrUpdate(this.model).then(() => {
      this.cancel();
    });
  }

  /**
   * cierra la modal del login
   */
  public cancel(): void {
    this.ref.close();
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
