import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AssignmentsGridComponent} from './assignments.grid.component';

const routes: Routes = [{
  path: '',
  component: AssignmentsGridComponent,
  children: [],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AssignmentsRoutingModule {
}
