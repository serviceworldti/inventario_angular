import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {UsersStatusGridComponent} from './users-status.grid.component';

const routes: Routes = [{
  path: '',
  component: UsersStatusGridComponent,
  children: [],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersStatusRoutingModule {
}
