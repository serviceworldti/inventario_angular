import {Component, OnDestroy} from '@angular/core';
import {GridService} from '../../../../@core/utils';
import {SystemUser} from '../../../../@core/entities';
import {UsersComponent} from './users.component';

@Component({
  selector: 'ngx-users-grid',
  styleUrls: ['./users.component.scss'],
  template: `
    <nb-card>
      <nb-card-header>
        {{'app.user' | translate}}
      </nb-card-header>

      <nb-card-body>
        <p class="pointer">
          <a *ngIf="oGridService.canReport" (click)="oGridService.onReport($event, 'pdf')">
            <i class="fa fa-file-pdf fa-3x"></i>
          </a> &nbsp;
          <a *ngIf="oGridService.canReport" (click)="oGridService.onReport($event, 'xlsx')">
            <i class="fas fa-file-spreadsheet fa-3x"></i>
          </a> &nbsp;
        </p>
        <ng2-smart-table [settings]="oGridService.settings"
                         [source]="oGridService.source"
                         (create)="oGridService.onCreate($event)"
                         (edit)="oGridService.onEdit($event)"
                         (delete)="oGridService.onDelete($event)">
        </ng2-smart-table>
      </nb-card-body>
    </nb-card>`,
})
export class UsersGridComponent implements OnDestroy {

  private alive = true;

  constructor(
    public oGridService: GridService,
  ) {
    this.oGridService.settings.columns = {
      email: {
        title: 'app.email',
        type: 'string',
      },
      username: {
        title: 'app.username',
        type: 'string',
      },
      systemUserStatus: {
        title: 'app.status',
        type: 'boolean',
        valuePrepareFunction: (cell, row) => cell.name,
      },
    };

    this.oGridService.setInit('rest/system_user', {
      entity: SystemUser, update: UsersComponent, view: UsersComponent,
    });

    this.oGridService.list();
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
