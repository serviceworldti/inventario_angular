import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {UsersGridComponent} from './users.grid.component';

const routes: Routes = [{
  path: '',
  component: UsersGridComponent,
  children: [],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule {
}
