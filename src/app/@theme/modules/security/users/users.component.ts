import {Component, Input, OnDestroy, ViewChild} from '@angular/core';
import {FormGroup, FormGroupDirective} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {FormService, GridService} from '../../../../@core/utils';
import {SystemOrganization, SystemRole, SystemUser, SystemUserStatus} from '../../../../@core/entities';
import {GridUtil} from '../../../../@core/entities/util/grid.util';

@Component({
  selector: 'ngx-users',
  styleUrls: ['./users.component.scss'],
  template: `
    <form name="form" (ngSubmit)="f.form.valid && onSubmit()" #f="ngForm" novalidate>
      <nb-card>
        <nb-card-header>{{title | translate}} {{'app.user' | translate}}</nb-card-header>
        <nb-card-body>

          <div class="form-group row">
            <mat-form-field class="col-md-6">
              <input matInput id="username" name="username" #username="ngModel" [(ngModel)]="model.username"
                     placeholder="{{'app.username' | translate}}" required minlength="4" type="text"/>
              <mat-error *ngIf="username?.errors">{{ oFormService.getErrorMessage(username) }}</mat-error>
            </mat-form-field>

            <mat-form-field class="col-md-6">
              <input matInput id="email" name="email" #email="ngModel" [(ngModel)]="model.email"
                     placeholder="{{'app.email' | translate}}" required minlength="4" type="text"/>
              <mat-error *ngIf="email?.errors">{{ oFormService.getErrorMessage(email) }}</mat-error>
            </mat-form-field>
          </div>

          <div class="form-group row">
            <mat-form-field class="col-md-6">
              <input matInput id="password" name="password" #password="ngModel" [(ngModel)]="model.password"
                     placeholder="{{'app.password' | translate}}" required minlength="4" type="password"/>
              <mat-error *ngIf="password?.errors">{{ oFormService.getErrorMessage(password) }}</mat-error>
            </mat-form-field>

            <mat-form-field class="col-md-6">
              <input matInput id="repassword" name="repassword" #repassword="ngModel" [(ngModel)]="model.repassword"
                     placeholder="{{'app.repassword' | translate}}" required minlength="4" type="password"/>
              <mat-error *ngIf="repassword?.errors">{{ oFormService.getErrorMessage(repassword) }}</mat-error>
            </mat-form-field>
          </div>

          <mat-form-field class="col-12">
            <mat-label>{{'app.roles' | translate}}</mat-label>
            <mat-select placeholder="{{'app.roles' | translate}}" name="systemRole"
                        #systemRole="ngModel" [compareWith]="oFormService.compareFn"
                        [(ngModel)]="model.systemRoles" multiple required>
              <mat-option *ngFor="let o of roles" [value]="o"> {{o.name}} </mat-option>
            </mat-select>
            <mat-error *ngIf="systemRole?.errors">
              {{ oFormService.getErrorMessage(systemRole) }}
            </mat-error>
          </mat-form-field>

          <mat-form-field class="col-12">
            <mat-label>{{'app.organizations' | translate}}</mat-label>
            <mat-select placeholder="{{'app.organizations' | translate}}" name="systemOrganization"
                        #systemOrganization="ngModel" [compareWith]="oFormService.compareFn"
                        [(ngModel)]="model.systemOrganization" required>
              <mat-option *ngFor="let o of organizations" [value]="o"> {{o.name}} </mat-option>
            </mat-select>
            <mat-error *ngIf="systemOrganization?.errors">
              {{ oFormService.getErrorMessage(systemOrganization) }}
            </mat-error>
          </mat-form-field>

          <mat-form-field class="col-12">
            <mat-label>{{'app.statues' | translate}}</mat-label>
            <mat-select placeholder="{{'app.statues' | translate}}" name="systemUserStatus"
                        #systemUserStatus="ngModel" [compareWith]="oFormService.compareFn"
                        [(ngModel)]="model.systemUserStatus" required>
              <mat-option *ngFor="let o of statues" [value]="o"> {{o.name}} </mat-option>
            </mat-select>
            <mat-error *ngIf="systemUserStatus?.errors">
              {{ oFormService.getErrorMessage(systemUserStatus) }}
            </mat-error>
          </mat-form-field>

        </nb-card-body>
        <nb-card-footer>
          <button type="button" nbButton status="danger" (click)="cancel()">{{'app.cancel' | translate}}</button>
          <button nbButton hero status="success" class="float-right"
                  [disabled]="!f.form.valid">{{'app.save' | translate}}</button>
        </nb-card-footer>
      </nb-card>
    </form>
  `,
})
export class UsersComponent implements OnDestroy {

  public form: FormGroup;
  public statues: SystemUserStatus[] = [];
  public organizations: SystemOrganization[] = [];
  public roles: SystemRole[] = [];
  @ViewChild(FormGroupDirective, {static: false}) public formDirective: FormGroupDirective;
  @Input() public title: string;
  @Input() public model: SystemUser = new SystemUser();
  private alive = true;

  constructor(
    protected ref: NbDialogRef<UsersComponent>,
    public oFormService: FormService,
    private oGridService: GridService,
  ) {
    this.oGridService.http().list(null, {
      path: 'rest/system_user_status',
      entity: GridUtil,
    }).then((result: GridUtil) => {
      this.statues = result.deepCopyData(SystemUserStatus);
    });

    this.oGridService.http().list(null, {
      path: 'rest/system_organization',
      entity: GridUtil,
    }).then((result: GridUtil) => {
      this.organizations = result.deepCopyData(SystemOrganization);
    });

    this.oGridService.http().list(null, {
      path: 'rest/system_role',
      entity: GridUtil,
    }).then((result: GridUtil) => {
      this.roles = result.deepCopyData(SystemRole);
    });
  }

  /**
   * realiza la peticion del login
   */
  public onSubmit(): void {
    const t = this;

    this.oGridService.saveOrUpdate(this.model).then(() => {
      this.cancel();
    });
  }

  /**
   * cierra la modal del login
   */
  public cancel(): void {
    this.ref.close();
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
