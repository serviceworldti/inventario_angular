import {Component, OnDestroy} from '@angular/core';
import {GridService} from '../../../../@core/utils';
import {SystemAudit} from '../../../../@core/entities';
import {AuditsComponent} from './audits.component';

@Component({
  selector: 'ngx-audits-grid',
  styleUrls: ['./audits.component.scss'],
  template: `
    <nb-card>
      <nb-card-header>
        {{'app.audit' | translate}}
      </nb-card-header>

      <nb-card-body>
        <p class="pointer">
          <a *ngIf="oGridService.canReport" (click)="oGridService.onReport($event, 'pdf')">
            <i class="fa fa-file-pdf fa-3x"></i>
          </a> &nbsp;
          <a *ngIf="oGridService.canReport" (click)="oGridService.onReport($event, 'xlsx')">
            <i class="fas fa-file-spreadsheet fa-3x"></i>
          </a> &nbsp;
        </p>
        <ng2-smart-table [settings]="oGridService.settings"
                         [source]="oGridService.source"
                         (create)="oGridService.onCreate($event)"
                         (edit)="oGridService.onEdit($event)"
                         (delete)="oGridService.onDelete($event)">
        </ng2-smart-table>
      </nb-card-body>
    </nb-card>`,
})
export class AuditsGridComponent implements OnDestroy {

  private alive = true;

  constructor(
    public oGridService: GridService,
  ) {
    this.oGridService.settings.columns = {
      dateCreated: {
        title: 'app.dateCreated',
        type: 'date-range',
      },
      oldChange: {
        title: 'app.oldChange',
        type: 'string',
      },
      tableOperation: {
        title: 'app.tableOperation',
        type: 'list-multiple',
      },
      typeOperation: {
        title: 'app.typeOperation',
        type: 'list-multiple',
      },
      systemUser: {
        title: 'app.user',
        type: 'string',
        attribute: 'systemUser.username',
        valuePrepareFunction: (cell, row) => cell.username,
      },
    };
    this.oGridService.settings.actions.add = false;
    this.oGridService.settings.actions.edit = false;
    this.oGridService.settings.actions.delete = false;

    this.oGridService.setInit('rest/system_audit', {
      entity: SystemAudit, update: AuditsComponent, view: AuditsComponent,
    });

    this.oGridService.list().then(() => {
      this.oGridService.settings.actions.add = false;
      this.oGridService.settings.actions.edit = false;
      this.oGridService.settings.actions.delete = false;
    });
  }

  ngOnDestroy() {
    this.alive = false;
    this.oGridService.settings.actions.add = true;
    this.oGridService.settings.actions.edit = true;
    this.oGridService.settings.actions.delete = true;
  }
}
