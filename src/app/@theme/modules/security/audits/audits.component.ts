import {Component, Input, OnDestroy, ViewChild} from '@angular/core';
import {FormGroup, FormGroupDirective} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {FormService, GridService} from '../../../../@core/utils';
import {SystemAudit, SystemUser} from '../../../../@core/entities';
import {GridUtil} from '../../../../@core/entities/util/grid.util';

@Component({
  selector: 'ngx-audits',
  styleUrls: ['./audits.component.scss'],
  template: `
    <form name="form" (ngSubmit)="f.form.valid && onSubmit()" #f="ngForm" novalidate>
      <nb-card>
        <nb-card-header>{{title | translate}} {{'app.audit' | translate}}</nb-card-header>
        <nb-card-body>

          <mat-form-field class="col-md-12">
            <input matInput id="oldChange" name="oldChange" #oldChange="ngModel" [(ngModel)]="model.oldChange"
                   placeholder="{{'app.oldChange' | translate}}" required minlength="4" type="text"/>
            <mat-error *ngIf="oldChange?.errors">{{ oFormService.getErrorMessage(oldChange) }}</mat-error>
          </mat-form-field>

          <div class="form-group row">
            <mat-form-field class="col-md-6">
              <input matInput id="tableOperation" name="tableOperation" #tableOperation="ngModel"
                     [(ngModel)]="model.tableOperation"
                     placeholder="{{'app.tableOperation' | translate}}" required minlength="4" type="text"/>
              <mat-error *ngIf="tableOperation?.errors">{{ oFormService.getErrorMessage(tableOperation) }}</mat-error>
            </mat-form-field>

            <mat-form-field class="col-md-6">
              <input matInput id="typeOperation" name="typeOperation" #typeOperation="ngModel"
                     [(ngModel)]="model.typeOperation"
                     placeholder="{{'app.typeOperation' | translate}}" required minlength="4" type="text"/>
              <mat-error *ngIf="typeOperation?.errors">{{ oFormService.getErrorMessage(typeOperation) }}</mat-error>
            </mat-form-field>
          </div>

          <mat-form-field class="col-12">
            <mat-label>{{'app.users' | translate}}</mat-label>
            <mat-select placeholder="{{'app.users' | translate}}" name="systemUser"
                        #systemUser="ngModel" [compareWith]="oFormService.compareFn"
                        [(ngModel)]="model.systemUser">
              <mat-option *ngFor="let o of users" [value]="o"> {{o.username}} </mat-option>
            </mat-select>
            <mat-error *ngIf="systemUser?.errors">
              {{ oFormService.getErrorMessage(systemUser) }}
            </mat-error>
          </mat-form-field>

        </nb-card-body>
        <nb-card-footer>
          <button type="button" nbButton status="danger" (click)="cancel()">{{'app.cancel' | translate}}</button>
          <button nbButton hero status="success" class="float-right"
                  [disabled]="!f.form.valid">{{'app.save' | translate}}</button>
        </nb-card-footer>
      </nb-card>
    </form>
  `,
})
export class AuditsComponent implements OnDestroy {

  public form: FormGroup;
  public users: SystemUser[] = [];
  @ViewChild(FormGroupDirective, {static: false}) public formDirective: FormGroupDirective;
  @Input() public title: string;
  @Input() public model: SystemAudit = new SystemAudit();
  private alive = true;

  constructor(
    protected ref: NbDialogRef<AuditsComponent>,
    public oFormService: FormService,
    private oGridService: GridService,
  ) {
    this.oGridService.http().list(null, {
      path: 'rest/system_user',
      entity: GridUtil,
    }).then((result: GridUtil) => {
      this.users = result.deepCopyData(SystemUser);
    });
  }

  /**
   * realiza la peticion del login
   */
  public onSubmit(): void {
    this.oGridService.saveOrUpdate(this.model).then(() => {
      this.cancel();
    });
  }

  /**
   * cierra la modal del login
   */
  public cancel(): void {
    this.ref.close();
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
