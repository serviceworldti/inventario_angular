import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbIconModule,
  NbListModule,
  NbRadioModule,
  NbSelectModule,
  NbTabsetModule,
  NbUserModule,
} from '@nebular/theme';
import {TranslateModule} from '@ngx-translate/core';
import {NgxEchartsModule} from 'ngx-echarts';
import {Ng2SmartTableModule} from 'ng2-smart-table';

import {ThemeModule} from '../../../theme.module';
import {AuditsRoutingModule} from './audits-routing.module';
import {AuditsGridComponent} from './audits.grid.component';
import {AuditsComponent} from './audits.component';

@NgModule({
  imports: [
    AuditsRoutingModule,
    FormsModule,
    ThemeModule,
    // Nebular
    NbCardModule,
    NbUserModule,
    NbButtonModule,
    NbTabsetModule,
    NbActionsModule,
    NbRadioModule,
    NbSelectModule,
    NbListModule,
    NbIconModule,
    NbButtonModule,
    NgxEchartsModule,
    Ng2SmartTableModule,
    // Angular
    MatInputModule,
    MatSelectModule,
    MatSlideToggleModule,
    // Others
    TranslateModule,
  ],
  declarations: [
    AuditsGridComponent,
    AuditsComponent,
  ],
  entryComponents: [
    AuditsComponent,
  ],
})
export class AuditsModule {
}
