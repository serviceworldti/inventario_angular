import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AuditsGridComponent} from './audits.grid.component';

const routes: Routes = [{
  path: '',
  component: AuditsGridComponent,
  children: [],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuditsRoutingModule {
}
