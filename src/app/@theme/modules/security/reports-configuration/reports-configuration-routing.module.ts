import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ReportsConfigurationGridComponent} from './reports-configuration.grid.component';

const routes: Routes = [{
  path: '',
  component: ReportsConfigurationGridComponent,
  children: [],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportsConfigurationRoutingModule {
}
