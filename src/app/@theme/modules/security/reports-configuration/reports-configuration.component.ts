import {Component, Input, OnDestroy, ViewChild} from '@angular/core';
import {FormGroup, FormGroupDirective} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {FormService, GridService} from '../../../../@core/utils';
import {SystemReportConfiguration, SystemUser} from '../../../../@core/entities';
import {GridUtil} from '../../../../@core/entities/util/grid.util';

@Component({
  selector: 'ngx-reports-configuration',
  styleUrls: ['./reports-configuration.component.scss'],
  template: `
    <form name="form" (ngSubmit)="f.form.valid && onSubmit()" #f="ngForm" novalidate>
      <nb-card>
        <nb-card-header>{{title | translate}} {{'app.report-configuration' | translate}}</nb-card-header>
        <nb-card-body class="form-group row">

          <mat-form-field class="col-md-12">
            <input matInput id="name" name="name" #name="ngModel" [(ngModel)]="model.name"
                   placeholder="{{'app.name' | translate}}" required minlength="4" type="text"/>
            <mat-error *ngIf="name?.errors">{{ oFormService.getErrorMessage(name) }}</mat-error>
          </mat-form-field>

          <mat-form-field class="col-md-12">
            <textarea matInput id="template" name="template" #template="ngModel" [(ngModel)]="model.template"
                      placeholder="{{'app.template-url' | translate}}" required minlength="8" rows="5"></textarea>
            <mat-error *ngIf="template?.errors">{{ oFormService.getErrorMessage(template) }}</mat-error>
          </mat-form-field>

          <div class="form-check">
            <mat-slide-toggle id="status" name="status" [(ngModel)]="model.status" #status="ngModel"
                              ng-true-value="'t'"
                              ng-false-value="'f'"> {{'app.status' | translate}} </mat-slide-toggle>
          </div>

        </nb-card-body>
        <nb-card-footer>
          <button type="button" nbButton status="danger" (click)="cancel()">{{'app.cancel' | translate}}</button>
          <button nbButton hero status="success" class="float-right"
                  [disabled]="!f.form.valid">{{'app.save' | translate}}</button>
        </nb-card-footer>
      </nb-card>
    </form>
  `,
})
export class ReportsConfigurationComponent implements OnDestroy {

  public form: FormGroup;
  public users: SystemUser[] = [];
  @ViewChild(FormGroupDirective, {static: false}) public formDirective: FormGroupDirective;
  @Input() public title: string;
  @Input() public model: SystemReportConfiguration = new SystemReportConfiguration();
  private alive = true;

  constructor(
    protected ref: NbDialogRef<ReportsConfigurationComponent>,
    public oFormService: FormService,
    private oGridService: GridService,
  ) {
    this.oGridService.http().list(null, {
      path: 'rest/system_user',
      entity: GridUtil,
    }).then((result: GridUtil) => {
      this.users = result.deepCopyData(SystemUser);
    });
  }

  /**
   * realiza la peticion del login
   */
  public onSubmit(): void {
    const t = this;

    this.oGridService.saveOrUpdate(this.model).then(() => {
      this.cancel();
    });
  }

  /**
   * cierra la modal del login
   */
  public cancel(): void {
    this.ref.close();
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
