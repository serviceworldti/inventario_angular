import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ThemesGridComponent} from './themes.grid.component';

const routes: Routes = [{
  path: '',
  component: ThemesGridComponent,
  children: [],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ThemesRoutingModule {
}
