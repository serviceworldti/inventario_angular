import {Component, OnDestroy} from '@angular/core';
import {GridService} from '../../../../@core/utils';
import {SystemTheme} from '../../../../@core/entities';
import {ThemesComponent} from './themes.component';

@Component({
  selector: 'ngx-themes-grid',
  styleUrls: ['./themes.component.scss'],
  template: `
    <nb-card>
      <nb-card-header>
        {{'app.theme' | translate}}
      </nb-card-header>

      <nb-card-body>
        <p class="pointer">
          <a *ngIf="oGridService.canReport" (click)="oGridService.onReport($event, 'pdf')">
            <i class="fa fa-file-pdf fa-3x"></i>
          </a> &nbsp;
          <a *ngIf="oGridService.canReport" (click)="oGridService.onReport($event, 'xlsx')">
            <i class="fas fa-file-spreadsheet fa-3x"></i>
          </a> &nbsp;
        </p>
        <ng2-smart-table [settings]="oGridService.settings"
                         [source]="oGridService.source"
                         (create)="oGridService.onCreate($event)"
                         (edit)="oGridService.onEdit($event)"
                         (delete)="oGridService.onDelete($event)">
        </ng2-smart-table>
      </nb-card-body>
    </nb-card>`,
})
export class ThemesGridComponent implements OnDestroy {

  private alive = true;

  constructor(
    public oGridService: GridService,
  ) {
    this.oGridService.settings.columns = {
      name: {
        title: 'app.name',
        type: 'string',
      },
      status: {
        title: 'app.status',
        type: 'checkbox',
        translate: true,
      },
    };

    this.oGridService.setInit('rest/system_theme', {
      entity: SystemTheme, update: ThemesComponent, view: ThemesComponent,
    });

    this.oGridService.list();
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
