import {Component, OnDestroy} from '@angular/core';
import {GridService} from '../../../../@core/utils';
import {SystemAssignment} from '../../../../@core/entities';
import {UsersConfigurationComponent} from './users-configuration.component';

@Component({
  selector: 'ngx-users-configuration-grid',
  styleUrls: ['./users-configuration.component.scss'],
  template: `
    <nb-card>
      <nb-card-header>
        {{'app.user-configuration' | translate}}
      </nb-card-header>

      <nb-card-body>
        <p class="pointer">
          <a *ngIf="oGridService.canReport" (click)="oGridService.onReport($event, 'pdf')">
            <i class="fa fa-file-pdf fa-3x"></i>
          </a> &nbsp;
          <a *ngIf="oGridService.canReport" (click)="oGridService.onReport($event, 'xlsx')">
            <i class="fas fa-file-spreadsheet fa-3x"></i>
          </a> &nbsp;
        </p>
        <ng2-smart-table [settings]="oGridService.settings"
                         [source]="oGridService.source"
                         (create)="oGridService.onCreate($event)"
                         (edit)="oGridService.onEdit($event)"
                         (delete)="oGridService.onDelete($event)">
        </ng2-smart-table>
      </nb-card-body>
    </nb-card>`,
})
export class UsersConfigurationGridComponent implements OnDestroy {

  private alive = true;

  constructor(
    public oGridService: GridService,
  ) {
    this.oGridService.settings.columns = {
      name: {
        title: 'app.name',
        type: 'string',
      },
      method: {
        title: 'app.method',
        type: 'list',
      },
      showInMenu: {
        title: 'app.show-in-menu',
        type: 'boolean',
        translate: true,
      },
      status: {
        title: 'app.status',
        type: 'checkbox',
        translate: true,
      },
    };

    this.oGridService.setInit('rest/system_user_configuration', {
      entity: SystemAssignment, update: UsersConfigurationComponent, view: UsersConfigurationComponent,
    });

    this.oGridService.list();
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
