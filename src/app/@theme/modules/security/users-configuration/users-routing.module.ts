import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {UsersConfigurationGridComponent} from './users-configuration.grid.component';

const routes: Routes = [{
  path: '',
  component: UsersConfigurationGridComponent,
  children: [],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule {
}
