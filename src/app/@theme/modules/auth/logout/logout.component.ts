import {Component, OnDestroy} from '@angular/core';
import {LoginService, MenuService} from '../../../../@core/utils';

@Component({
  selector: 'ngx-logout',
  styleUrls: ['./logout.component.scss'],
  template: ``,
})
export class LogoutComponent implements OnDestroy {

  private alive = true;

  constructor(
    private oLoginService: LoginService,
    private oMenuService: MenuService,
  ) {
    this.oLoginService.setLogout();
    this.oMenuService.nbMenu().navigateHome();
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
