import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbIconModule,
  NbListModule,
  NbRadioModule,
  NbSelectModule,
  NbTabsetModule,
  NbUserModule,
} from '@nebular/theme';
import {TranslateModule} from '@ngx-translate/core';
import {NgxEchartsModule} from 'ngx-echarts';

import {ThemeModule} from '../../../theme.module';
import {LogoutRoutingModule} from './logout-routing.module';
import {LogoutComponent} from './logout.component';

@NgModule({
  imports: [
    LogoutRoutingModule,
    FormsModule,
    ThemeModule,
    // Angular Mat
    MatFormFieldModule,
    MatInputModule,
    // Nebular
    NbActionsModule,
    NbButtonModule,
    NbCardModule,
    NbIconModule,
    NbListModule,
    NbRadioModule,
    NbSelectModule,
    NbTabsetModule,
    NbUserModule,
    NgxEchartsModule,
    // translate
    TranslateModule,
  ],
  declarations: [
    LogoutComponent,
  ],
})
export class LogoutModule {
}
