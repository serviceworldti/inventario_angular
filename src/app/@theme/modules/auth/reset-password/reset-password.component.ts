import {Component, OnDestroy} from '@angular/core';

@Component({
  selector: 'ngx-reset-password',
  styleUrls: ['./reset-password.component.scss'],
  template: ``,
})
export class ResetPasswordComponent implements OnDestroy {

  private alive = true;

  ngOnDestroy() {
    this.alive = false;
  }
}
