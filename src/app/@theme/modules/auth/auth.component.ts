import {Component} from '@angular/core';

@Component({
  selector: 'ngx-panel',
  styleUrls: ['auth.component.scss'],
  template: `
    <router-outlet></router-outlet>
  `,
})
export class AuthComponent {
}
