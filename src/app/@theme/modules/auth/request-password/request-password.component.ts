import {Component, OnDestroy, ViewChild} from '@angular/core';
import {NbDialogRef} from '@nebular/theme';
import {FormService, LoginService} from '../../../../@core/utils';
import {FormGroup, FormGroupDirective} from '@angular/forms';
import {SystemUser} from '../../../../@core/entities';

@Component({
  selector: 'ngx-request-password',
  styleUrls: ['./request-password.component.scss'],
  template: `
    <form name="form" (ngSubmit)="f.form.valid && onSubmit()" #f="ngForm" novalidate>
      <nb-card>
        <nb-card-header>
          <img alt="login" class="w-100" src="/assets/images/login.jpg">
        </nb-card-header>
        <nb-card-body>

          <div class="form-group row">
            <mat-form-field class="col-12">
              <input matInput id="username" name="username" #username="ngModel" [(ngModel)]="model.username"
                     placeholder="{{'app.username_email' | translate}}" required minlength="4" type="text"
                     [disabled]="next"/>
              <mat-error *ngIf="username?.errors">{{ oFormService.getErrorMessage(username) }}</mat-error>
            </mat-form-field>
          </div>

          <div *ngIf="next">

            <div class="form-group row">
              <mat-form-field class="col-12">
                <input matInput id="tokenRecoveryPassword" name="tokenRecoveryPassword" #tokenRecoveryPassword="ngModel"
                       [(ngModel)]="model.tokenRecoveryPassword"
                       placeholder="{{'app.tokenRecoveryPassword' | translate}}" required minlength="70" type="text"/>
                <mat-error
                  *ngIf="tokenRecoveryPassword?.errors">{{ oFormService.getErrorMessage(tokenRecoveryPassword) }}</mat-error>
              </mat-form-field>
            </div>

            <div class="form-group row">
              <mat-form-field class="col-12">
                <input matInput id="password" name="password" #password="ngModel" [(ngModel)]="model.password"
                       placeholder="{{'app.password' | translate}}" required minlength="4" type="password"/>
                <mat-error *ngIf="password?.errors">{{ oFormService.getErrorMessage(password) }}</mat-error>
              </mat-form-field>
            </div>

            <div class="form-group row">
              <mat-form-field class="col-12">
                <input matInput id="repassword" name="repassword" #repassword="ngModel" [(ngModel)]="model.repassword"
                       placeholder="{{'app.repassword' | translate}}" required minlength="4" type="password"/>
                <mat-error *ngIf="repassword?.errors">{{ oFormService.getErrorMessage(repassword) }}</mat-error>
                <mat-error>{{ errorRepassword }}</mat-error>
              </mat-form-field>
            </div>

          </div>

        </nb-card-body>
        <nb-card-footer>
          <button type="button" nbButton status="danger" (click)="cancel()">{{'app.dismiss' | translate}}</button>
          <button nbButton hero status="success" class="float-right"
                  [disabled]="!f.form.valid">{{'app.enter' | translate}}</button>
        </nb-card-footer>
      </nb-card>
    </form>
  `,
})
export class RequestPasswordComponent implements OnDestroy {

  public errorRepassword: string = '';
  public form: FormGroup;
  @ViewChild(FormGroupDirective, {static: false}) public formDirective: FormGroupDirective;
  public model: SystemUser = new SystemUser();
  public next: boolean = false;
  private alive = true;

  constructor(
    protected ref: NbDialogRef<RequestPasswordComponent>,
    private oLoginService: LoginService,
    public oFormService: FormService,
  ) {
  }

  /**
   * realiza la peticion del login
   */
  public onSubmit(): void {
    const t = this;
    this.model.email = this.model.username;

    if (!this.next) {
      this.oLoginService.setRecovery(this.model).then(() => t.next = true);
    } else {
      if (this.oFormService.comparePassword(this.model.password, this.model.repassword)) {
        this.oLoginService.setChangePassword(this.model).then(() => t.cancel());
      } else {
        this.errorRepassword = this.oFormService.translate().instant('error.password-match');
      }
    }
  }

  /**
   * cierra la modal
   */
  cancel(): void {
    this.ref.close();
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
