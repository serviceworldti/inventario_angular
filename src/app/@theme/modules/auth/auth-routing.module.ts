import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

import {NotFoundComponent} from '../../components';
import {AuthComponent} from './auth.component';

const routes: Routes = [{
  path: '',
  component: AuthComponent,
  children: [
    {
      path: 'login',
      loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
    },
    {
      path: 'logout',
      loadChildren: () => import('./logout/logout.module').then(m => m.LogoutModule),
    },
    {
      path: 'register',
      loadChildren: () => import('./register/register.module').then(m => m.RegisterModule),
    },
    {
      path: 'request-password',
      loadChildren: () => import('./request-password/request-password.module').then(m => m.RequestPasswordModule),
    },
    {
      path: 'reset-password',
      loadChildren: () => import('./reset-password/reset-password.module').then(m => m.ResetPasswordModule),
    },
    {
      path: '',
      redirectTo: 'login',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {
}
