import {Component, OnDestroy, ViewChild} from '@angular/core';
import {FormGroup, FormGroupDirective} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {FormService, LoginService} from '../../../../@core/utils';
import {SystemOrganization, SystemUser} from '../../../../@core/entities';
import {GridUtil} from '../../../../@core/entities/util/grid.util';
import {RequestPasswordComponent} from '../request-password/request-password.component';

@Component({
  selector: 'ngx-register',
  styleUrls: ['./register.component.scss'],
  template: `
    <form name="form" (ngSubmit)="f.form.valid && onSubmit()" #f="ngForm" novalidate>
      <nb-card>
        <nb-card-header>
          <img alt="login" class="w-100" src="/assets/images/login.jpg">
        </nb-card-header>
        <nb-card-body>

          <div class="form-group row">
            <mat-form-field class="col-12">
              <input matInput id="username" name="username" #username="ngModel" [(ngModel)]="model.username"
                     placeholder="{{'app.username' | translate}}" required minlength="4" type="text"/>
              <mat-error *ngIf="username?.errors">{{ oFormService.getErrorMessage(username) }}</mat-error>
            </mat-form-field>
          </div>

          <div class="form-group row">
            <mat-form-field class="col-12">
              <input matInput id="password" name="password" #password="ngModel" [(ngModel)]="model.password"
                     placeholder="{{'app.password' | translate}}" required minlength="4" type="password"/>
              <mat-error *ngIf="password?.errors">{{ oFormService.getErrorMessage(password) }}</mat-error>
            </mat-form-field>
          </div>

          <div class="form-group row">
            <mat-form-field class="col-12">
              <input matInput id="email" name="email" #email="ngModel" [(ngModel)]="model.email"
                     placeholder="{{'app.email' | translate}}" required email minlength="10" type="text"/>
              <mat-error *ngIf="email?.errors">{{ oFormService.getErrorMessage(email) }}</mat-error>
            </mat-form-field>
          </div>

          <mat-form-field class="col-12" *ngIf="show">
            <mat-label>{{'pages.organizations' | translate}}</mat-label>
            <mat-select placeholder="{{'pages.organizations' | translate}}" name="organization" #organization="ngModel"
                        [(ngModel)]="model.systemOrganization" required>
              <mat-option *ngFor="let o of organizations" [value]="o"> {{o.name}} </mat-option>
            </mat-select>
          </mat-form-field>

          <div>
            <button type="button" nbButton status="default" (click)="recovery()">{{'app.recovery' | translate}}</button>
          </div>

        </nb-card-body>
        <nb-card-footer>
          <button type="button" nbButton status="danger" (click)="cancel()">{{'app.dismiss' | translate}}</button>
          <button nbButton hero status="success" class="float-right"
                  [disabled]="!f.form.valid">{{'app.registro' | translate}}</button>
        </nb-card-footer>
      </nb-card>
    </form>
  `,
})
export class RegisterComponent implements OnDestroy {

  public form: FormGroup;
  @ViewChild(FormGroupDirective, {static: false}) public formDirective: FormGroupDirective;
  public model: SystemUser = new SystemUser();
  public organizations: SystemOrganization[] = [];
  public show: boolean = true;
  private alive = true;

  constructor(
    protected ref: NbDialogRef<RegisterComponent>,
    private oLoginService: LoginService,
    public oFormService: FormService,
  ) {
    this.oLoginService.http().list(null, {
      path: 'rest/system_organization',
      entity: GridUtil,
    }).then((result: GridUtil) => {
      this.organizations = result.deepCopyData(SystemOrganization);
    });
  }

  /**
   * realiza la peticion del registro
   */
  public onSubmit(): void {
    this.oLoginService.setRegister(this.model).then(() => this.ref.close());
  }

  /**
   * inicia la sesion
   */
  public recovery = () => {
    this.oLoginService.http().storage().object().dialog().open(RequestPasswordComponent);
    this.cancel();
  }

  /**
   * cierra la modal
   */
  cancel(): void {
    this.ref.close();
  }

  /**
   * destroy
   */

  ngOnDestroy() {
    this.alive = false;
  }
}
