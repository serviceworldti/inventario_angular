import {Component, OnDestroy, ViewChild} from '@angular/core';
import {FormGroup, FormGroupDirective} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {FormService, LoginService} from '../../../../@core/utils';
import {SystemUser} from '../../../../@core/entities';
import {RegisterComponent} from '../register/register.component';
import {RequestPasswordComponent} from '../request-password/request-password.component';

@Component({
  selector: 'ngx-login',
  styleUrls: ['./login.component.scss'],
  template: `
    <form name="form" (ngSubmit)="f.form.valid && onSubmit()" #f="ngForm" novalidate>
      <nb-card>
        <nb-card-header>
          <img alt="login" class="w-100" src="/assets/images/login.jpg">
        </nb-card-header>
        <nb-card-body>

          <div class="form-group row">
            <mat-form-field class="col-12">
              <input matInput id="username" name="username" #username="ngModel" [(ngModel)]="model.username"
                     placeholder="{{'app.username_email' | translate}}" required minlength="4" type="text"/>
              <mat-error *ngIf="username?.errors">{{ oFormService.getErrorMessage(username) }}</mat-error>
            </mat-form-field>
          </div>

          <div class="form-group row">
            <mat-form-field class="col-12">
              <input matInput id="password" name="password" #password="ngModel" [(ngModel)]="model.password"
                     placeholder="{{'app.password' | translate}}" required minlength="4" type="password"/>
              <mat-error *ngIf="password?.errors">{{ oFormService.getErrorMessage(password) }}</mat-error>
            </mat-form-field>
          </div>

          <div>
            <button type="button" nbButton status="default" (click)="recovery()">{{'app.recovery' | translate}}</button>
            <button type="button" nbButton hero status="primary" class="float-right"
                    (click)="register()">{{'app.register' | translate}}</button>
          </div>
        </nb-card-body>
        <nb-card-footer>
          <button type="button" nbButton status="danger" (click)="cancel()">{{'app.dismiss' | translate}}</button>
          <button nbButton hero status="success" class="float-right"
                  [disabled]="!f.form.valid">{{'app.enter' | translate}}</button>
        </nb-card-footer>
      </nb-card>
    </form>
  `,
})
export class LoginComponent implements OnDestroy {

  public form: FormGroup;
  @ViewChild(FormGroupDirective, {static: false}) public formDirective: FormGroupDirective;
  public model: SystemUser = new SystemUser();
  private alive = true;

  constructor(
    protected ref: NbDialogRef<LoginComponent>,
    private oLoginService: LoginService,
    public oFormService: FormService,
  ) {
  }

  /**
   * realiza la peticion del login
   */
  public onSubmit(): void {
    this.model.email = this.model.username;

    this.oLoginService.setLogin(this.model).then(() => this.ref.close());
  }

  /**
   * inicia la sesion
   */
  public register = () => {
    this.oLoginService.http().storage().object().dialog().open(RegisterComponent);
    this.cancel();
  }

  /**
   * inicia la sesion
   */
  public recovery = () => {
    this.oLoginService.http().storage().object().dialog().open(RequestPasswordComponent);
    this.cancel();
  }

  /**
   * cierra la modal
   */
  cancel(): void {
    this.ref.close();
  }

  /**
   * destroy
   */
  ngOnDestroy() {
    this.alive = false;
  }
}
