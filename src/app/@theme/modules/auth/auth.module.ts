import {NgModule} from '@angular/core';
import {NbMenuModule} from '@nebular/theme';

import {ThemeModule} from '../../theme.module';
import {AuthComponent} from './auth.component';
import {AuthRoutingModule} from './auth-routing.module';
import {LoginModule} from './login/login.module';
import {LogoutModule} from './logout/logout.module';
import {RegisterModule} from './register/register.module';
import {RequestPasswordModule} from './request-password/request-password.module';
import {ResetPasswordModule} from './reset-password/reset-password.module';

@NgModule({
  imports: [
    AuthRoutingModule,
    ThemeModule,
    NbMenuModule,
    LoginModule,
    LogoutModule,
    RegisterModule,
    RequestPasswordModule,
    ResetPasswordModule,
  ],
  declarations: [
    AuthComponent,
  ],
  entryComponents: [],
  providers: [],
})
export class AuthModule {
}
