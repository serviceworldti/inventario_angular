import {Component} from '@angular/core';

@Component({
  selector: 'ngx-account',
  styleUrls: ['account.component.scss'],
  template: `
    <router-outlet></router-outlet>
  `,
})
export class AccountComponent {
}
