import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

import {AccessPanelRootService} from '../../../@core/mock';
import {NotFoundComponent} from '../../components';
import {AccountComponent} from './account.component';

const routes: Routes = [{
  path: '',
  component: AccountComponent,
  canActivate: [AccessPanelRootService],
  children: [
    {
      path: 'organization',
      loadChildren: () => import('./organization/organization.module').then(m => m.OrganizationModule),
    },
    {
      path: 'profile',
      loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule),
    },
    {
      path: '',
      redirectTo: 'login',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountRoutingModule {
}
