import {NgModule} from '@angular/core';
import {NbMenuModule} from '@nebular/theme';

import {ThemeModule} from '../../theme.module';
import {AccountComponent} from './account.component';
import {OrganizationModule} from './organization/organization.module';
import {AccountRoutingModule} from './account-routing.module';

@NgModule({
  imports: [
    AccountRoutingModule,
    ThemeModule,
    NbMenuModule,
    OrganizationModule,
  ],
  declarations: [
    AccountComponent,
  ],
})
export class AccountModule {
}
