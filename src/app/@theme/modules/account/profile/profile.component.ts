import {Component, OnDestroy} from '@angular/core';
import {HttpService} from '../../../../@core/utils';
import {GridUtil} from '../../../../@core/entities/util/grid.util';
import {PersonSystem, SystemUser} from '../../../../@core/entities';

@Component({
  selector: 'ngx-profile',
  styleUrls: ['./profile.component.scss'],
  template: `
    <nb-card>
      <nb-card-body>
        <nb-stepper orientation="vertical">

          <nb-step label="{{'app.user-data' | translate}}">

            <ngx-profile-user [model]="model"></ngx-profile-user>

          </nb-step>

          <nb-step label="{{'app.person-data' | translate}}">

            <ngx-profile-person [model]="person"></ngx-profile-person>

          </nb-step>

        </nb-stepper>
      </nb-card-body>
    </nb-card>
  `,
})
export class ProfileComponent implements OnDestroy {

  public model: SystemUser = new SystemUser();
  public person: PersonSystem = new PersonSystem();
  private alive = true;

  constructor(
    private oHttpService: HttpService,
  ) {
    this.oHttpService.list(null, {
      path: 'rest/profile',
      entity: GridUtil,
    }).then((result: GridUtil) => {
      this.model = result.deepCopyData(SystemUser)[0];
      if (this.oHttpService.storage().object().isNotNull(this.model.person)) {
        this.person = this.model.person;
      } else {
        this.model.person = this.person;
        this.oHttpService.put(this.model, {
          path: 'rest/profile',
        });
      }
    });
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
