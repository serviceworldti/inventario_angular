import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormGroup, FormGroupDirective} from '@angular/forms';
import {FilePreviewModel} from 'ngx-awesome-uploader';
import {SystemUser} from '../../../../@core/entities';
import {FileService, FormService, HttpService} from '../../../../@core/utils';

@Component({
  selector: 'ngx-profile-user',
  styleUrls: ['./profile.component.scss'],
  template: `
    <div class="row">
      <div class="col-12">
        <form name="form" (ngSubmit)="f.form.valid && onSubmit()" #f="ngForm" novalidate>
          <nb-card>
            <nb-card-header>{{'app.profile' | translate}}</nb-card-header>
            <nb-card-body class="form-group row">

              <div class="form-group col-12">
                <div class="uploader-wrapper w-100">
                  <ngx-file-picker #photo class="w-100 simpleButton"
                                   [fileMaxCount]="1"
                                   [uploadType]="uploadType"
                                   [accept]="accept"
                                   [showeDragDropZone]="true"
                                   [adapter]="oFileService.adapter"
                                   (uploadSuccess)="oFileService.onAddFile($event, myFiles)"
                                   (removeSuccess)="oFileService.onRemoveFile($event, myFiles)"
                                   (validationError)="oFileService.onValidationError($event)">
                    <div class="dropzoneTemplate">
                      <button type="button" nbButton status="info" class="w-100"> {{'app.photo' | translate}} </button>
                    </div>
                  </ngx-file-picker>
                </div>
              </div>

              <div class="form-group col-12 col-md-6">
                <mat-form-field class="w-100">
                  <input matInput id="username" name="username" #username="ngModel" [(ngModel)]="model.username"
                         placeholder="{{'app.username' | translate}}" required minlength="5" type="text"/>
                  <mat-error *ngIf="username?.errors">{{ oFormService.getErrorMessage(username) }}</mat-error>
                </mat-form-field>
              </div>

              <div class="form-group col-12 col-md-6">
                <mat-form-field class="w-100">
                  <input matInput name="email" #email="ngModel" [(ngModel)]="model.email"
                         placeholder="{{'app.email' | translate}}" required email minlength="10" type="text"/>
                  <mat-error *ngIf="email?.errors">{{ oFormService.getErrorMessage(email) }}</mat-error>
                </mat-form-field>
              </div>

              <div class="form-group col-12 col-md-6">
                <mat-form-field class="w-100">
                  <input matInput name="password" #password="ngModel" [(ngModel)]="model.password"
                         placeholder="{{'app.password' | translate}}" required minlength="4" type="password"/>
                  <mat-error *ngIf="password?.errors">{{ oFormService.getErrorMessage(password) }}</mat-error>
                </mat-form-field>
              </div>

              <div class="form-group col-12 col-md-6">
                <mat-form-field class="w-100">
                  <input matInput name="repassword" #repassword="ngModel" [(ngModel)]="model.repassword"
                         placeholder="{{'app.repassword' | translate}}" minlength="4" type="password"/>
                  <mat-error *ngIf="repassword?.errors">{{ oFormService.getErrorMessage(repassword) }}</mat-error>
                </mat-form-field>
              </div>

            </nb-card-body>
            <nb-card-footer>
              <button nbButton hero status="success" class="float-right"
                      [disabled]="!f.form.valid">{{'app.save' | translate}}</button>
            </nb-card-footer>
          </nb-card>
        </form>
      </div>
    </div>`,
})
export class ProfileUserComponent implements OnInit, OnDestroy {

  public form: FormGroup;
  @Input() public model: SystemUser = new SystemUser();
  @ViewChild(FormGroupDirective, {static: false}) public formDirective: FormGroupDirective;
  public myFiles: FilePreviewModel[] = [];
  public uploadType: string = 'single';
  public accept: string = 'image/*';
  public adapter;
  private alive = true;

  constructor(
    public oFormService: FormService,
    public oFileService: FileService,
    private oHttpService: HttpService,
  ) {
  }

  ngOnInit() {
  }

  /**
   * realiza la peticion del login
   */
  public onSubmit(): void {
    const t = this;

    if (this.myFiles.length > 0) {
      this.myFiles.forEach((v: FilePreviewModel, i: number) => {
        t.oFileService.getFileString(v).then((f) => {
          t.model.photo = f;

          t.oHttpService.put(this.model, {
            path: 'rest/profile',
          });
        });
      });
    } else {
      t.oHttpService.put(this.model, {
        path: 'rest/profile',
      });
    }
  }

  /**
   * cierra la modal del login
   */
  public cancel(): void {
    this.oHttpService.storage().object().NbMenu().navigateHome();
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
