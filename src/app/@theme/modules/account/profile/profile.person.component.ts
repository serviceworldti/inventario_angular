import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormGroup, FormGroupDirective} from '@angular/forms';
import {FormService, HttpService} from '../../../../@core/utils';
import {PersonSystem} from '../../../../@core/entities';

@Component({
  selector: 'ngx-profile-person',
  styleUrls: ['./profile.component.scss'],
  template: `
    <div class="row">
      <div class="col-12">
        <form name="form" (ngSubmit)="f.form.valid && onSubmit()" #f="ngForm" novalidate>
          <nb-card>
            <nb-card-header>{{'app.my_data' | translate}}</nb-card-header>
            <nb-card-body class="row">

              <div class="form-group col-12 col-md-6">
                <mat-form-field class="w-100">
                  <input matInput id="firstName" name="firstName" #firstName="ngModel"
                         [(ngModel)]="model.firstName"
                         placeholder="{{'pages.firstName' | translate}}" required minlength="2" type="text"/>
                  <mat-error *ngIf="firstName?.errors">{{ oFormService.getErrorMessage(firstName) }}</mat-error>
                </mat-form-field>
              </div>

              <div class="form-group col-12 col-md-6">
                <mat-form-field class="w-100">
                  <input matInput id="lastName" name="lastName" #lastName="ngModel" [(ngModel)]="model.lastName"
                         placeholder="{{'pages.lastName' | translate}}" required minlength="2" type="text"/>
                  <mat-error *ngIf="lastName?.errors">{{ oFormService.getErrorMessage(lastName) }}</mat-error>
                </mat-form-field>
              </div>

              <div class="form-group col-12 col-md-6">
                <mat-form-field class="w-100">
                  <input matInput id="birthdate" name="birthdate" #birthdate="ngModel"
                         [(ngModel)]="model.birthdate" [min]="minDate" [max]="maxDate"
                         [matDatepicker]="birthdateDP" placeholder="{{'pages.birthdate' | translate}}" readonly>
                  <mat-datepicker-toggle matSuffix [for]="birthdateDP"></mat-datepicker-toggle>
                  <mat-datepicker #birthdateDP disabled="false"></mat-datepicker>
                </mat-form-field>
              </div>

            </nb-card-body>
            <nb-card-footer>
              <button nbButton hero status="success" class="float-right"
                      [disabled]="!f.form.valid">{{'app.save' | translate}}</button>
            </nb-card-footer>
          </nb-card>
        </form>
      </div>
    </div>
  `,
})
export class ProfilePersonComponent implements OnInit, OnDestroy {

  public form: FormGroup;
  @Input() public model: PersonSystem = new PersonSystem();
  @ViewChild(FormGroupDirective, {static: false}) public formDirective: FormGroupDirective;
  minDate: Date;
  maxDate: Date;
  private alive = true;

  constructor(
    public oFormService: FormService,
    private oHttpService: HttpService,
  ) {
    this.maxDate = new Date();
    this.minDate = new Date(this.maxDate.getFullYear() - 50, 0, 1);
  }

  ngOnInit() {
  }

  /**
   * realiza la peticion del login
   */
  public onSubmit(): void {
    this.oHttpService.put(this.model, {
      path: 'rest/person',
    });
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
