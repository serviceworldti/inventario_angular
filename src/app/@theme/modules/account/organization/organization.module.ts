import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbIconModule,
  NbListModule,
  NbRadioModule,
  NbSelectModule,
  NbStepperModule,
  NbTabsetModule,
  NbUserModule,
} from '@nebular/theme';
import {TranslateModule} from '@ngx-translate/core';
import {NgxEchartsModule} from 'ngx-echarts';
import {FilePickerModule} from 'ngx-awesome-uploader';

import {ThemeModule} from '../../../theme.module';
import {OrganizationRoutingModule} from './organization-routing.module';
import {OrganizationComponent} from './organization.component';
import {OrganizationDataComponent} from './organization.data.component';
import {OrganizationConfigurationComponent} from './organization.configuration.component';

@NgModule({
  imports: [
    OrganizationRoutingModule,
    FormsModule,
    ThemeModule,
    // Nebular
    NbCardModule,
    NbUserModule,
    NbButtonModule,
    NbTabsetModule,
    NbActionsModule,
    NbRadioModule,
    NbSelectModule,
    NbListModule,
    NbIconModule,
    NbButtonModule,
    NbStepperModule,
    NgxEchartsModule,
    // Angular
    MatInputModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatSelectModule,
    MatSlideToggleModule,
    // Others
    TranslateModule,
    NgxEchartsModule,
    FilePickerModule,
  ],
  declarations: [
    OrganizationComponent,
    OrganizationDataComponent,
    OrganizationConfigurationComponent,
  ],
})
export class OrganizationModule {
}
