import {Component, OnDestroy} from '@angular/core';
import {SystemConfiguration, SystemOrganization, SystemUser} from '../../../../@core/entities';
import {FormService, HttpService} from '../../../../@core/utils';
import {GridUtil} from '../../../../@core/entities/util/grid.util';

@Component({
  selector: 'ngx-config',
  styleUrls: ['./organization.component.scss'],
  template: `
    <nb-card>
      <nb-card-body>
        <nb-stepper orientation="vertical">

          <nb-step label="{{'app.organization-data' | translate}}">

            <ngx-organization-data [model]="organization"></ngx-organization-data>

          </nb-step>

          <nb-step label="{{'app.organization-configuration' | translate}}">

            <ngx-organization-configuration [model]="configuration"></ngx-organization-configuration>

          </nb-step>

        </nb-stepper>
      </nb-card-body>
    </nb-card>
  `,
})
export class OrganizationComponent implements OnDestroy {

  public user: SystemUser = new SystemUser();
  public organization: SystemOrganization = new SystemOrganization();
  public configuration: SystemConfiguration = new SystemConfiguration();
  private alive = true;

  constructor(
    public oFormService: FormService,
    private oHttpService: HttpService,
  ) {
    this.oHttpService.list(null, {
      path: 'rest/profile',
      entity: GridUtil,
    }).then((o: GridUtil) => {
      this.user = o.deepCopyData(SystemUser)[0];
      this.organization = this.user.systemOrganization;

      this.oHttpService.get(null, {
        path: 'rest/system_configuration/' + this.user.systemOrganization.id,
        entity: SystemConfiguration,
      }).then((c: SystemConfiguration) => {
        this.configuration = c;
      });
    });
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
