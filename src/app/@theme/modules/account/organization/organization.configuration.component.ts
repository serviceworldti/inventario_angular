import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormGroup, FormGroupDirective} from '@angular/forms';
import {SystemConfiguration, SystemReportConfiguration, SystemTheme, WorldLanguage} from '../../../../@core/entities';
import {FormService, HttpService} from '../../../../@core/utils';
import {GridUtil} from '../../../../@core/entities/util/grid.util';

@Component({
  selector: 'ngx-organization-configuration',
  styleUrls: ['./organization.component.scss'],
  template: `
    <div class="row">
      <div class="col-12">
        <form name="form" (ngSubmit)="f.form.valid && onSubmit()" #f="ngForm" novalidate>
          <nb-card>
            <nb-card-header>{{'app.organization-configuration' | translate}}</nb-card-header>
            <nb-card-body class="form-group row">

              <mat-form-field class="col-12">
                <mat-label>{{'app.themes' | translate}}</mat-label>
                <mat-select placeholder="{{'app.themes' | translate}}" name="systemTheme"
                            #systemTheme="ngModel" [compareWith]="oFormService.compareFn"
                            [(ngModel)]="model.systemTheme" required>
                  <mat-option *ngFor="let o of themes" [value]="o"> {{o.name}} </mat-option>
                </mat-select>
              </mat-form-field>

              <mat-form-field class="col-12">
                <mat-label>{{'app.languages' | translate}}</mat-label>
                <mat-select placeholder="{{'app.languages' | translate}}" name="worldLanguage"
                            #worldLanguage="ngModel" [compareWith]="oFormService.compareFn"
                            [(ngModel)]="model.worldLanguage" required>
                  <mat-option *ngFor="let o of languages" [value]="o"> {{o.name}} </mat-option>
                </mat-select>
                <mat-error *ngIf="worldLanguage?.errors">
                  {{ oFormService.getErrorMessage(worldLanguage) }}
                </mat-error>
              </mat-form-field>

              <mat-form-field class="col-12">
                <mat-label>{{'app.reports' | translate}}</mat-label>
                <mat-select placeholder="{{'app.reports' | translate}}" name="systemReportConfigurations"
                            #systemReportConfigurations="ngModel" [compareWith]="oFormService.compareFn"
                            [(ngModel)]="model.systemReportConfigurations" multiple>
                  <mat-option *ngFor="let o of reports" [value]="o"> {{o.name}} </mat-option>
                </mat-select>
                <mat-error *ngIf="systemReportConfigurations?.errors">
                  {{ oFormService.getErrorMessage(systemReportConfigurations) }}
                </mat-error>
              </mat-form-field>

            </nb-card-body>
            <nb-card-footer>
              <button nbButton hero status="success" class="float-right"
                      [disabled]="!f.form.valid">{{'app.save' | translate}}</button>
            </nb-card-footer>
          </nb-card>
        </form>
      </div>
    </div>`,
})
export class OrganizationConfigurationComponent implements OnInit, OnDestroy {

  public form: FormGroup;
  @Input() public model: SystemConfiguration = new SystemConfiguration();
  public themes: SystemTheme[] = [];
  public languages: WorldLanguage[] = [];
  public reports: SystemReportConfiguration[] = [];
  @ViewChild(FormGroupDirective, {static: false}) public formDirective: FormGroupDirective;
  private alive = true;

  constructor(
    public oFormService: FormService,
    private oHttpService: HttpService,
  ) {
  }

  ngOnInit() {
    this.oHttpService.list(null, {
      path: 'rest/system_theme',
      entity: GridUtil,
    }).then((result: GridUtil) => {
      this.themes = result.deepCopyData(SystemTheme);
    });

    this.oHttpService.list(null, {
      path: 'rest/world_language',
      entity: GridUtil,
    }).then((result: GridUtil) => {
      this.languages = result.deepCopyData(WorldLanguage);
    });

    this.oHttpService.list(null, {
      path: 'rest/system_report_configuration',
      entity: GridUtil,
    }).then((result: GridUtil) => {
      this.reports = result.deepCopyData(SystemReportConfiguration);
    });
  }

  /**
   * realiza la peticion del login
   */
  public onSubmit(): void {
    this.oHttpService.get(this.model, {
      path: 'rest/system_configuration/' + this.model.systemOrganization.id,
    });
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
