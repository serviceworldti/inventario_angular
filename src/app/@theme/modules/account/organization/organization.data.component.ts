import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormGroup, FormGroupDirective} from '@angular/forms';
import {SystemOrganization} from '../../../../@core/entities';
import {FormService, HttpService} from '../../../../@core/utils';

@Component({
  selector: 'ngx-organization-data',
  styleUrls: ['./organization.component.scss'],
  template: `
    <div class="row">
      <div class="col-12">
        <form name="form" (ngSubmit)="f.form.valid && onSubmit()" #f="ngForm" novalidate>
          <nb-card>
            <nb-card-header>{{'app.organization-data' | translate}}</nb-card-header>
            <nb-card-body class="form-group row">

              <div class="form-group col-12 col-md-6">
                <mat-form-field class="w-100">
                  <input matInput id="name" name="name" #name="ngModel" [(ngModel)]="model.name"
                         placeholder="{{'app.name' | translate}}" required minlength="5" type="text"/>
                  <mat-error *ngIf="name?.errors">{{ oFormService.getErrorMessage(name) }}</mat-error>
                </mat-form-field>
              </div>

              <div class="form-group col-12 col-md-6">
                <mat-form-field class="w-100">
                  <input matInput name="email" #email="ngModel" [(ngModel)]="model.email"
                         placeholder="{{'app.email' | translate}}" required email minlength="10" type="text"/>
                  <mat-error *ngIf="email?.errors">{{ oFormService.getErrorMessage(email) }}</mat-error>
                </mat-form-field>
              </div>

              <div class="form-group col-12">
                <mat-form-field class="w-100">
                  <input matInput name="code" #code="ngModel" [(ngModel)]="model.code"
                         placeholder="{{'app.code' | translate}}" minlength="10" type="text"
                         (keypress)="oFormService.disableKey($event)" (keydown)="oFormService.disableKey($event)"
                         (keyup)="oFormService.disableKey($event)" readonly disabled required/>
                  <mat-error *ngIf="code?.errors">{{ oFormService.getErrorMessage(code) }}</mat-error>
                </mat-form-field>
              </div>

            </nb-card-body>
            <nb-card-footer>
              <button nbButton hero status="success" class="float-right"
                      [disabled]="!f.form.valid">{{'app.save' | translate}}</button>
            </nb-card-footer>
          </nb-card>
        </form>
      </div>
    </div>`,
})
export class OrganizationDataComponent implements OnInit, OnDestroy {

  public form: FormGroup;
  @Input() public model: SystemOrganization = new SystemOrganization();
  @ViewChild(FormGroupDirective, {static: false}) public formDirective: FormGroupDirective;
  private alive = true;

  constructor(
    public oFormService: FormService,
    private oHttpService: HttpService,
  ) {
  }

  ngOnInit() {
  }

  /**
   * realiza la peticion del login
   */
  public onSubmit(): void {
    this.oHttpService.put(this.model, {
      path: 'rest/system_organization',
    });
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
