import {Component, OnDestroy} from '@angular/core';

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./home.component.scss'],
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnDestroy {

  private alive = true;

  ngOnDestroy() {
    this.alive = false;
  }
}
