import {Component} from '@angular/core';
import {MENU_ITEMS} from './principal-menu';

@Component({
  selector: 'ngx-principal',
  styleUrls: ['principal.component.scss'],
  template: `
    <ngx-principal-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-principal-layout>
  `,
})
export class PrincipalComponent {

  menu = MENU_ITEMS;

}
