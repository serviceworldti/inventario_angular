import {NgModule} from '@angular/core';
import {NbMenuModule} from '@nebular/theme';
import {TranslateModule} from '@ngx-translate/core';

import {ThemeModule} from '../../@theme/theme.module';
import {PrincipalComponent} from './principal.component';
import {HomeModule} from './home/home.module';
import {PrincipalRoutingModule} from './principal-routing.module';

@NgModule({
  imports: [
    PrincipalRoutingModule,
    ThemeModule,
    NbMenuModule,
    HomeModule,
    TranslateModule,
  ],
  declarations: [
    PrincipalComponent,
  ],
  entryComponents: [],
  providers: [],
})
export class PrincipalModule {
}
