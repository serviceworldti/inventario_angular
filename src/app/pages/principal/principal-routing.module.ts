import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

import {
  InMaintenanceComponent,
  NotFoundComponent,
  NotKeepComponent,
  NotSessionComponent,
} from '../../@theme/components';
import {AccessAuthService, AccessPanelRootService} from '../../@core/mock';
import {PrincipalComponent} from './principal.component';
import {HomeComponent} from './home/home.component';

const routes: Routes = [{
  path: '',
  component: PrincipalComponent,
  children: [
    {
      path: 'home',
      component: HomeComponent,
    },
    {
      path: 'auth',
      canActivate: [AccessAuthService],
      loadChildren: () => import('../../@theme/modules/auth/auth.module').then(m => m.AuthModule),
    },
    {
      path: 'panel',
      canActivate: [AccessPanelRootService],
      canActivateChild: [AccessPanelRootService],
      loadChildren: () => import('../panel/panel.module').then(m => m.PanelModule),
    },
    {
      path: 'not-keep',
      component: NotKeepComponent,
    },
    {
      path: 'not-session',
      component: NotSessionComponent,
    },
    {
      path: 'in-maintenance',
      component: InMaintenanceComponent,
    },
    {
      path: '',
      redirectTo: 'home',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrincipalRoutingModule {
}
