import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

import {NotFoundComponent} from '../../@theme/components';
import {PanelComponent} from './panel.component';
import {DashboardComponent} from './dashboard/dashboard.component';

const routes: Routes = [{
  path: '',
  component: PanelComponent,
  children: [
    {
      path: 'dashboard',
      component: DashboardComponent,
    },
    {
      path: 'account',
      loadChildren: () => import('../../@theme/modules/account/account.module').then(m => m.AccountModule),
    },
    {
      path: 'security',
      loadChildren: () => import('../../@theme/modules/security/security.module').then(m => m.SecurityModule),
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PanelRoutingModule {
}
