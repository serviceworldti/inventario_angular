import {NgModule} from '@angular/core';
import {NbMenuModule} from '@nebular/theme';

import {ThemeModule} from '../../@theme/theme.module';
import {PanelComponent} from './panel.component';
import {DashboardModule} from './dashboard/dashboard.module';
import {PanelRoutingModule} from './panel-routing.module';

@NgModule({
  imports: [
    PanelRoutingModule,
    ThemeModule,
    NbMenuModule,
    DashboardModule,
  ],
  declarations: [
    PanelComponent,
  ],
})
export class PanelModule {
}
