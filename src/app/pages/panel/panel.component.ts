import {Component} from '@angular/core';

@Component({
  selector: 'ngx-panel',
  styleUrls: ['panel.component.scss'],
  template: `
    <router-outlet></router-outlet>
  `,
})
export class PanelComponent {
}
