import {EntityUtil} from '../@core/entities/util/entity.util';
import {PersonInterface} from '../interfaces/person.interface';

export class Person extends EntityUtil implements PersonInterface {

  constructor() {
    super();
  }

  nothing() {
  }

}
