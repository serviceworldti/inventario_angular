/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import {Component, OnInit} from '@angular/core';
import {SwUpdate, UpdateAvailableEvent} from '@angular/service-worker';
import {AnalyticsService} from './@core/utils/analytics.service';
import {SeoService} from './@core/utils/seo.service';
import {AlertService} from './@core/utils';

@Component({
  selector: 'ngx-app',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {

  constructor(
    private swUpdate: SwUpdate,
    private oAlertService: AlertService,
    private analytics: AnalyticsService,
    private seoService: SeoService,
  ) {

    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe((event: UpdateAvailableEvent) => {
        this.oAlertService.openDialog({
          title: 'error.title.426',
          message: 'app.confirm_update',
          acceptTitle: 'app.accept',
          accept: () => {
            window.location.reload();
          },
          decline: () => {
          },
        });
      });
    }
  }

  ngOnInit(): void {
    this.analytics.trackPageViews();
    this.seoService.trackCanonicalChanges();
  }
}
