import {EntityUtilInterface} from '../util/entity.util.interface';

export interface PersonPhoneTypeInterface extends EntityUtilInterface {
  status: boolean;
  name: string;
  icon: string;
}
