import {EntityUtilInterface} from '../util/entity.util.interface';

export interface PersonTypeInterface extends EntityUtilInterface {
  status: boolean;
  name: string;
}
