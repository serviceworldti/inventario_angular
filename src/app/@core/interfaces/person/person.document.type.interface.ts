import {EntityUtilInterface} from '../util/entity.util.interface';

export interface PersonDocumentTypeInterface extends EntityUtilInterface {
  personDocumentType: PersonDocumentTypeInterface;
  status: boolean;
  name: string;
}
