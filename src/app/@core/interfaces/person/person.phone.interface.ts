import {EntityUtilInterface} from '../util/entity.util.interface';
import {WorldCountryInterface} from '../world/world.country.interface';
import {PersonSystemInterface} from './person.system.interface';
import {PersonPhoneTypeInterface} from './person.phone.type.interface';

export interface PersonPhoneInterface extends EntityUtilInterface {
  person: PersonSystemInterface;
  worldCountry: WorldCountryInterface;
  personPhoneType: PersonPhoneTypeInterface;
  status: boolean;
  number: string;
}
