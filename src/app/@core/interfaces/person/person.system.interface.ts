import {PersonInterface} from '../../../interfaces/person.interface';
import {PersonTypeInterface} from './person.type.interface';

export interface PersonSystemInterface extends PersonInterface {

  personType: PersonTypeInterface;
  status: boolean;
  firstName: string;
  lastName: string;
  birthdate: Date;

}
