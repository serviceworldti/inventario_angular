import {EntityUtilInterface} from '../util/entity.util.interface';
import {WorldCountryInterface} from '../world/world.country.interface';
import {PersonSystemInterface} from './person.system.interface';

export interface PersonAddressInterface extends EntityUtilInterface {
  person: PersonSystemInterface;
  worldCountry: WorldCountryInterface;
  status: boolean;
  principal: boolean;
  state: string;
  street: string;
  zipcode: string;
  address1: string;
  address2: string;
  settlement: string;
}
