import {EntityUtilInterface} from '../util/entity.util.interface';
import {PersonSystemInterface} from './person.system.interface';
import {PersonDocumentTypeInterface} from './person.document.type.interface';

export interface PersonDocumentInterface extends EntityUtilInterface {
  person: PersonSystemInterface;
  personDocumentType: PersonDocumentTypeInterface;
  number: string;
}
