export interface EntityUtilInterface {
  id: number;
  dateCreated: Date;
  dateUpdate: Date;

  deepCopy(json: any);
}
