import {EntityUtilInterface} from '../util/entity.util.interface';
import {WorldCountryInterface} from './world.country.interface';

export interface WorldCityInterface extends EntityUtilInterface {
  worldCountry: WorldCountryInterface;
  status: boolean;
  name: string;
  code: string;
  postalCode: string;
}
