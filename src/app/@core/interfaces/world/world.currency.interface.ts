import {EntityUtilInterface} from '../util/entity.util.interface';
import {WorldCountryInterface} from './world.country.interface';

export interface WorldCurrencyInterface extends EntityUtilInterface {
  worldCountry: WorldCountryInterface;
  worldCurrency: WorldCurrencyInterface;
  status: boolean;
  name: string;
  code: string;
}
