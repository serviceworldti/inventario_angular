import {EntityUtilInterface} from '../util/entity.util.interface';

export interface WorldCountryInterface extends EntityUtilInterface {
  status: boolean;
  nameEs: string;
  nameUs: string;
  name: string;
  code2: string;
  code3: string;
  phoneCode: string;
}
