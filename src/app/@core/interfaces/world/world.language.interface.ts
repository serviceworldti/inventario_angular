import {EntityUtilInterface} from '../util/entity.util.interface';

export interface WorldLanguageInterface extends EntityUtilInterface {
  status: boolean;
  name: string;
  iso: string;
}
