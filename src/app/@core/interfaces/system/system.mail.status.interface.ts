import {EntityUtilInterface} from '../util/entity.util.interface';

export interface SystemMailStatusInterface extends EntityUtilInterface {
  status: boolean;
  name: string;
}
