import {EntityUtilInterface} from '../util/entity.util.interface';

export interface SystemMailTypeInterface extends EntityUtilInterface {
  status: boolean;
  name: string;
}
