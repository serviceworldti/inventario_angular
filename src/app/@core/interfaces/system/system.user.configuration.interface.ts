import {EntityUtilInterface} from '../util/entity.util.interface';
import {SystemNotificationTypeInterface} from './system.notification.type.interface';
import {SystemUserInterface} from './system.user.interface';
import {SystemThemeInterface} from './system.theme.interface';
import {WorldLanguageInterface} from '../world/world.language.interface';

export interface SystemUserConfigurationInterface extends EntityUtilInterface {
  systemUser: SystemUserInterface;
  systemTheme: SystemThemeInterface;
  worldLanguage: WorldLanguageInterface;
  systemNotificationTypes: SystemNotificationTypeInterface[];
}
