import {EntityUtilInterface} from '../util/entity.util.interface';

export interface SystemMenuInterface extends EntityUtilInterface {
  status: boolean;
  logged: boolean;
  showInNavbar: boolean;
  showInSidebar: boolean;
  name: string;
  label: string;
  uri: string;
  icon: string;
  keepLogin: boolean;
  submenus: SystemMenuInterface[];
}
