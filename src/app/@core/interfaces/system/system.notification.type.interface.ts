import {EntityUtilInterface} from '../util/entity.util.interface';

export interface SystemNotificationTypeInterface extends EntityUtilInterface {
  status: boolean;
  name: string;
}
