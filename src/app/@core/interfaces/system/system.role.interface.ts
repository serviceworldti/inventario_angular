import {EntityUtilInterface} from '../util/entity.util.interface';
import {SystemPermissionInterface} from './system.permission.interface';

export interface SystemRoleInterface extends EntityUtilInterface {
  status: boolean;
  name: string;
  systemPermissions: SystemPermissionInterface[];
}
