import {EntityUtilInterface} from '../util/entity.util.interface';
import {SystemNotificationTypeInterface} from './system.notification.type.interface';
import {SystemOrganizationInterface} from './system.organization.interface';
import {SystemThemeInterface} from './system.theme.interface';
import {WorldLanguageInterface} from '../world/world.language.interface';
import {SystemReportConfigurationInterface} from './system.report.configuration.interface';

export interface SystemConfigurationInterface extends EntityUtilInterface {
  systemOrganization: SystemOrganizationInterface;
  systemTheme: SystemThemeInterface;
  worldLanguage: WorldLanguageInterface;
  logo: string;
  systemReportConfigurations: SystemReportConfigurationInterface[];
  systemNotificationTypes: SystemNotificationTypeInterface[];
}
