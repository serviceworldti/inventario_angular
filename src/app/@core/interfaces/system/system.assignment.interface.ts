import {EntityUtilInterface} from '../util/entity.util.interface';
import {SystemPermissionInterface} from './system.permission.interface';

export interface SystemAssignmentInterface extends EntityUtilInterface {
  systemPermission: SystemPermissionInterface;
  status: boolean;
  showInMenu: boolean;
  name: string;
  method: string;
}
