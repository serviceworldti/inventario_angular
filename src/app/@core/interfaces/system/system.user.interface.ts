import {EntityUtilInterface} from '../util/entity.util.interface';
import {SystemOrganizationInterface} from './system.organization.interface';
import {SystemUserStatusInterface} from './system.user.status.interface';
import {SystemRoleInterface} from './system.role.interface';
import {PersonSystemInterface} from '../person/person.system.interface';

export interface SystemUserInterface extends EntityUtilInterface {
  systemUserStatus: SystemUserStatusInterface;
  systemOrganization: SystemOrganizationInterface;
  person: PersonSystemInterface;
  email: string;
  username: string;
  password: string;
  photo: string;
  lastConection: Date;
  lastActivity: Date;
  tokenSecurity: string;
  dateExpiredTokenSecurity: Date;
  tokenRecoveryPassword: string;
  dateTokenRecoveryPassword: Date;
  systemRoles: SystemRoleInterface[];
}
