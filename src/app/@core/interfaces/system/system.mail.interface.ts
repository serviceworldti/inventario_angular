import {EntityUtilInterface} from '../util/entity.util.interface';
import {SystemUserInterface} from './system.user.interface';
import {SystemMailTypeInterface} from './system.mail.type.interface';
import {SystemMailStatusInterface} from './system.mail.status.interface';

export interface SystemMailInterface extends EntityUtilInterface {
  systemUserSender: SystemUserInterface;
  systemUsers: SystemUserInterface[];
  mailType: SystemMailTypeInterface;
  mailStatus: SystemMailStatusInterface;
  subject: string;
  description: string;
}
