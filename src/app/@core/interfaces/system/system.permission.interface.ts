import {EntityUtilInterface} from '../util/entity.util.interface';

export interface SystemPermissionInterface extends EntityUtilInterface {
  systemPermission: SystemPermissionInterface;
  status: boolean;
  logged: boolean;
  showInNavbar: boolean;
  showInSidebar: boolean;
  name: string;
  label: string;
  uri: string;
  icon: string;
  keepLogin: boolean;
}
