import {EntityUtilInterface} from '../util/entity.util.interface';

export interface SystemThemeInterface extends EntityUtilInterface {
  status: boolean;
  name: string;
}
