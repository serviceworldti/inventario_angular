import {EntityUtilInterface} from '../util/entity.util.interface';

export interface SystemReadDataFileInterface extends EntityUtilInterface {
  status: boolean;
  name: string;
  checksum: string;
}
