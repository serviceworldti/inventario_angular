import {EntityUtilInterface} from '../util/entity.util.interface';
import {SystemUserInterface} from './system.user.interface';

export interface SystemAuditInterface extends EntityUtilInterface {
  systemUser: SystemUserInterface;
  tableOperation: string;
  typeOperation: string;
  oldChange: string;
}
