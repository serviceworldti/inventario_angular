import {EntityUtilInterface} from '../util/entity.util.interface';

export interface SystemUserStatusInterface extends EntityUtilInterface {
  status: boolean;
  name: string;
}
