import {EntityUtilInterface} from '../util/entity.util.interface';
import {SystemGroupInterface} from './system.group.interface';

export interface SystemOrganizationInterface extends EntityUtilInterface {
  systemGroup: SystemGroupInterface;
  status: boolean;
  name: string;
  email: string;
  code: string;
}
