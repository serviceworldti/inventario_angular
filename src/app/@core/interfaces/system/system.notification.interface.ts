import {EntityUtilInterface} from '../util/entity.util.interface';
import {SystemNotificationTypeInterface} from './system.notification.type.interface';
import {SystemUserInterface} from './system.user.interface';

export interface SystemNotificationInterface extends EntityUtilInterface {
  systemNotificationType: SystemNotificationTypeInterface;
  systemUser: SystemUserInterface;
  status: boolean;
}
