import {EntityUtilInterface} from '../util/entity.util.interface';
import {SystemOrganizationInterface} from './system.organization.interface';

export interface SystemReportConfigurationInterface extends EntityUtilInterface {
  systemOrganization: SystemOrganizationInterface;
  status: boolean;
  name: string;
  template: string;
}
