import {EntityUtilInterface} from '../util/entity.util.interface';
import {SystemRoleInterface} from './system.role.interface';

export interface SystemGroupInterface extends EntityUtilInterface {
  status: boolean;
  name: string;
  systemRoles: SystemRoleInterface[];
}
