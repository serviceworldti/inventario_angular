import {EntityUtilInterface} from '../util/entity.util.interface';
import {SystemUserInterface} from './system.user.interface';

export interface SystemDeviceInterface extends EntityUtilInterface {
  systemUser: SystemUserInterface;
  status: boolean;
  name: string;
  tokenSecurity: string;
  lastConection: Date;
}
