import {ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {NbButtonModule, NbCardModule, NbDatepickerModule} from '@nebular/theme';
import {NbAuthModule, NbDummyAuthStrategy} from '@nebular/auth';
import {NbRoleProvider, NbSecurityModule} from '@nebular/security';
import {TranslateModule} from '@ngx-translate/core';
import {FilePickerModule} from 'ngx-awesome-uploader';
import {of as observableOf} from 'rxjs';

import {throwIfAlreadyLoaded} from './module-import-guard';

import {
  AlertMessageComponent,
  FilesDownloadComponent,
  FilesViewComponent,
  GridFilterCheckboxComponent,
  GridFilterDateComponent,
  GridFilterDaterangeComponent,
  GridFilterInputComponent,
  GridFilterInputnumberComponent,
  GridFilterNoneComponent,
  GridFilterSelectComponent,
  GridFilterSelectmultipleComponent,
} from './components';
import {
  AlertService,
  AnalyticsService,
  DatetimeService,
  FileService,
  FormService,
  GridService,
  HttpService,
  LayoutService,
  LoadingService,
  LoginService,
  MenuService,
  ObjectService,
  PlayerService,
  SeoService,
  StateService,
  StorageService,
} from './utils';
import {UserData} from './data/users';

import {UserService} from './mock/users.service';
import {MockDataModule} from './mock/mock-data.module';

const socialLinks = [
  {
    url: 'https://github.com/akveo/nebular',
    target: '_blank',
    icon: 'github',
  },
  {
    url: 'https://www.facebook.com/akveo/',
    target: '_blank',
    icon: 'facebook',
  },
  {
    url: 'https://twitter.com/akveo_inc',
    target: '_blank',
    icon: 'twitter',
  },
];

const DATA_SERVICES = [
  AlertService,
  DatetimeService,
  FileService,
  FormService,
  GridService,
  HttpService,
  LoadingService,
  LoginService,
  MenuService,
  ObjectService,
  StorageService,
  {provide: UserData, useClass: UserService},
];

export class NbSimpleRoleProvider extends NbRoleProvider {
  getRole() {
    // here you could provide any role based on any auth flow
    return observableOf('guest');
  }
}

export const NB_CORE_PROVIDERS = [
  ...MockDataModule.forRoot().providers,
  ...DATA_SERVICES,
  ...NbAuthModule.forRoot({
    strategies: [
      NbDummyAuthStrategy.setup({
        name: 'email',
        delay: 3000,
      }),
    ],
    forms: {
      login: {
        socialLinks: socialLinks,
      },
      register: {
        socialLinks: socialLinks,
      },
    },
  }).providers,

  NbSecurityModule.forRoot({
    accessControl: {
      guest: {
        view: '*',
      },
      user: {
        parent: 'guest',
        create: '*',
        edit: '*',
        remove: '*',
      },
    },
  }).providers,
  {
    provide: NbRoleProvider, useClass: NbSimpleRoleProvider,
  },
  AnalyticsService,
  LayoutService,
  PlayerService,
  SeoService,
  StateService,
];

export const NB_CORE_IMPORTS = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  MatSelectModule,
  MatInputModule,
  MatSlideToggleModule,
  MatDatepickerModule,
  NbCardModule,
  NbButtonModule,
  NbDatepickerModule,
  TranslateModule,
  FilePickerModule,
];

export const NB_CORE_EXPORTS = [
  NbAuthModule,
];

export const NB_CORE_COMPONENTS = [
  AlertMessageComponent,
  FilesDownloadComponent,
  FilesViewComponent,
  GridFilterCheckboxComponent,
  GridFilterDateComponent,
  GridFilterDaterangeComponent,
  GridFilterInputComponent,
  GridFilterInputnumberComponent,
  GridFilterNoneComponent,
  GridFilterSelectComponent,
  GridFilterSelectmultipleComponent,
];

@NgModule({
  imports: [...NB_CORE_IMPORTS],
  exports: [...NB_CORE_EXPORTS],
  declarations: [...NB_CORE_COMPONENTS],
  entryComponents: [...NB_CORE_COMPONENTS],
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }

  static forRoot(): ModuleWithProviders<CoreModule> {
    return {
      ngModule: CoreModule,
      providers: [
        ...NB_CORE_PROVIDERS,
      ],
    };
  }
}
