import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {LoginService} from '../utils';

@Injectable({
  providedIn: 'root',
})
export class AccessAuthService implements CanActivate {

  constructor(
    private oLoginService: LoginService,
  ) {
  }

  /**
   * activa el enrutador
   *
   * @param route
   * @param state
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (!this.oLoginService.getLogin() || state.url.trim().toLowerCase().startsWith('/auth/logout')) {
      return true;
    }

    this.oLoginService.http().storage().object().router().navigate(['not-keep']);
    return false;
  }

}
