import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanActivateChild, RouterStateSnapshot} from '@angular/router';
import {LoginService, MenuService} from '../utils';
import {SystemAssignment, SystemMenu} from '../entities';

@Injectable({
  providedIn: 'root',
})
export class AccessPanelRootService implements CanActivate, CanActivateChild {

  private lastMenu: SystemMenu = null;
  private lastUri: string = '';
  private lastValidate: boolean = false;

  constructor(
    private oLoginService: LoginService,
    private oMenuService: MenuService,
  ) {
  }

  /**
   * activa el enrutador
   *
   * @param route
   * @param state
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.oLoginService.getLogin()) {
      return true;
    }

    this.oLoginService.http().storage().object().router().navigate(['not-session']);
    return false;
  }

  /**
   * activa el enrutador
   *
   * @param route
   * @param state
   */
  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let validMenu: boolean = false;
    const uri = state.url.trim().toLowerCase();
    this.lastMenu = new SystemMenu();
    this.oLoginService.http().assignments = [];

    if (this.lastUri !== uri) {
      this.lastUri = uri;
      validMenu = this.validateAccessMenu(this.oMenuService.dataMenuBar, validMenu, uri);
      validMenu = this.validateAccessMenu(this.oMenuService.dataMenuSidebar, validMenu, uri);

      if (validMenu) {
        this.lastValidate = true;

        if (this.lastMenu.id > 0) {
          this.oLoginService.http().get(null, {
            path: 'rest/system_assignment/menu/' + this.lastMenu.id,
            entity: SystemAssignment,
          }, {}).then((result: SystemAssignment[]) => {
            this.oLoginService.http().assignments = result;
            this.oLoginService.http().isAssignments.emit(this.oLoginService.http().assignments);
          });
        } else {
          this.oLoginService.http().setAssignmentsDefault();
        }
      }
    }

    if (this.lastValidate) {
      return true;
    }

    this.oLoginService.http().storage().object().router().navigate(['not-keep']);
    return false;
  }

  /**
   * valida el acceso en el submenu
   *
   * @param menus
   * @param valid
   * @param uri
   */
  private validateAccessMenu(menus: SystemMenu[], valid: boolean, uri: string): boolean {
    if (!this.oLoginService.http().storage().object().isNull(menus)) {
      menus.forEach((menu: SystemMenu) => {
        let menuUri: string = menu.uri.trim().toLowerCase();

        if (
          this.oLoginService.http().storage().object().getSize(menu.submenus) > 0 &&
          uri.indexOf(menuUri) > -1
        ) {
          valid = this.validateAccessMenu(menu.submenus as SystemMenu[], valid, uri);
        } else {
          uri += (uri.endsWith('/')) ? '' : '/';
          menuUri += (menuUri.endsWith('/')) ? '' : '/';

          if (uri === menuUri) {
            this.lastMenu = menu;
            valid = true;
          }
        }
      });
    }

    return valid;
  }

}
