import {EntityUtil} from '../util/entity.util';
import {WorldLanguageInterface} from '../../interfaces/world/world.language.interface';

export class WorldLanguage extends EntityUtil implements WorldLanguageInterface {
  public status: boolean;
  public name: string;
  public iso: string;

  constructor() {
    super();
    this.status = true;
    this.name = '';
    this.iso = '';
  }
}
