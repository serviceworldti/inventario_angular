import {EntityUtil} from '../util/entity.util';
import {WorldCountryInterface} from '../../interfaces/world/world.country.interface';

export class WorldCountry extends EntityUtil implements WorldCountryInterface {
  public status: boolean;
  public nameEs: string;
  public nameUs: string;
  public name: string;
  public code2: string;
  public code3: string;
  public phoneCode: string;

  constructor() {
    super();
    this.status = true;
    this.nameEs = '';
    this.nameUs = '';
    this.name = '';
    this.code2 = '';
    this.code3 = '';
    this.phoneCode = '';
  }
}
