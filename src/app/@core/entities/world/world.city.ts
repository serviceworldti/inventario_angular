import {EntityUtil} from '../util/entity.util';
import {WorldCityInterface} from '../../interfaces';
import {WorldCountry} from './world.country';

export class WorldCity extends EntityUtil implements WorldCityInterface {
  public worldCountry: WorldCountry;
  public status: boolean;
  public name: string;
  public code: string;
  public postalCode: string;

  constructor() {
    super();
    this.status = true;
    this.name = '';
    this.code = '';
    this.postalCode = '';
  }
}
