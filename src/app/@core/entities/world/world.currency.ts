import {EntityUtil} from '../util/entity.util';
import {WorldCurrencyInterface} from '../../interfaces';
import {WorldCountry} from './world.country';

export class WorldCurrency extends EntityUtil implements WorldCurrencyInterface {
  public worldCountry: WorldCountry;
  public worldCurrency: WorldCurrency;
  public status: boolean;
  public name: string;
  public code: string;

  constructor() {
    super();
    this.status = true;
    this.name = '';
    this.code = '';
  }
}
