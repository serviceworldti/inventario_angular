import {EntityUtil} from '../util/entity.util';
import {PersonDocumentTypeInterface} from '../../interfaces';

export class PersonDocumentType extends EntityUtil implements PersonDocumentTypeInterface {
  public personDocumentType: PersonDocumentType;
  public status: boolean;
  public name: string;

  constructor() {
    super();
    this.status = true;
    this.name = '';
  }

}
