import {EntityUtil} from '../util/entity.util';
import {PersonPhoneInterface} from '../../interfaces';
import {WorldCountry} from '../world/world.country';
import {PersonPhoneType} from './person.phone.type';
import {PersonSystem} from './person.system';

export class PersonPhone extends EntityUtil implements PersonPhoneInterface {
  public person: PersonSystem;
  public worldCountry: WorldCountry;
  public personPhoneType: PersonPhoneType;
  public status: boolean;
  public number: string;

  constructor() {
    super();
    this.status = true;
    this.number = '';
  }

}
