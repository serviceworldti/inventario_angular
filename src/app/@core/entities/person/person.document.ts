import {EntityUtil} from '../util/entity.util';
import {PersonDocumentInterface} from '../../interfaces';
import {PersonSystem} from './person.system';
import {PersonDocumentType} from './person.document.type';

export class PersonDocument extends EntityUtil implements PersonDocumentInterface {
  public person: PersonSystem;
  public personDocumentType: PersonDocumentType;
  public number: string;

  constructor() {
    super();
    this.number = '';
  }

}
