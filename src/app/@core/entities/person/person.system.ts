import {PersonSystemInterface} from '../../interfaces';
import {Person} from '../../../entities/person';
import {PersonType} from './person.type';

export class PersonSystem extends Person implements PersonSystemInterface {

  public personType: PersonType;
  public status: boolean;
  public firstName: string;
  public lastName: string;
  public birthdate: Date;

  constructor() {
    super();
    this.status = true;
    this.firstName = '';
    this.lastName = '';
    this.birthdate = new Date();
  }

}
