import {EntityUtil} from '../util/entity.util';
import {PersonTypeInterface} from '../../interfaces';

export class PersonType extends EntityUtil implements PersonTypeInterface {
  public status: boolean;
  public name: string;

  constructor() {
    super();
    this.status = true;
    this.name = '';
  }

}
