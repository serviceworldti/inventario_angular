import {EntityUtil} from '../util/entity.util';
import {PersonPhoneTypeInterface} from '../../interfaces';

export class PersonPhoneType extends EntityUtil implements PersonPhoneTypeInterface {
  public status: boolean;
  public name: string;
  public icon: string;

  constructor() {
    super();
    this.status = true;
    this.name = '';
    this.icon = '';
  }

}
