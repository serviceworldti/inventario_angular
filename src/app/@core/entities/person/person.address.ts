import {EntityUtil} from '../util/entity.util';
import {PersonAddressInterface} from '../../interfaces';
import {WorldCountry} from '../world/world.country';
import {PersonSystem} from './person.system';

export class PersonAddress extends EntityUtil implements PersonAddressInterface {
  public person: PersonSystem;
  public worldCountry: WorldCountry;
  public status: boolean;
  public principal: boolean;
  public state: string;
  public street: string;
  public zipcode: string;
  public address1: string;
  public address2: string;
  public settlement: string;

  constructor() {
    super();
    this.status = true;
    this.principal = false;
    this.state = '';
    this.street = '';
    this.zipcode = '';
    this.address1 = '';
    this.address2 = '';
    this.settlement = '';
  }

}
