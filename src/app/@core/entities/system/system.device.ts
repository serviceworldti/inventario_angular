import {EntityUtil} from '../util/entity.util';
import {SystemDeviceInterface} from '../../interfaces';
import {SystemUser} from './system.user';

export class SystemDevice extends EntityUtil implements SystemDeviceInterface {
  public systemUser: SystemUser;
  public status: boolean;
  public name: string;
  public tokenSecurity: string;
  public lastConection: Date;

  constructor() {
    super();
    this.status = true;
    this.name = '';
    this.tokenSecurity = '';
    this.lastConection = new Date();
  }
}
