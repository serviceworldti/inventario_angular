import {EntityUtil} from '../util/entity.util';
import {SystemUserInterface} from '../../interfaces';
import {SystemOrganization} from './system.organization';
import {SystemRole} from './system.role';
import {SystemUserStatus} from './system.user.status';
import {PersonSystem} from '../person/person.system';

export class SystemUser extends EntityUtil implements SystemUserInterface {
  public systemUserStatus: SystemUserStatus;
  public systemOrganization: SystemOrganization;
  public person: PersonSystem;
  public email: string;
  public username: string;
  public password: string;
  public repassword: string;
  public photo: string;
  public lastConection: Date;
  public lastActivity: Date;
  public tokenSecurity: string;
  public dateExpiredTokenSecurity: Date;
  public tokenRecoveryPassword: string;
  public dateTokenRecoveryPassword: Date;
  public systemRoles: SystemRole[];

  constructor() {
    super();
    this.email = '';
    this.username = '';
    this.password = '';
    this.repassword = '';
    this.photo = '';
    this.tokenSecurity = '';
    this.tokenRecoveryPassword = '';
    this.lastConection = new Date();
    this.lastActivity = new Date();
    this.dateExpiredTokenSecurity = new Date();
    this.dateTokenRecoveryPassword = new Date();
  }
}
