import {EntityUtil} from '../util/entity.util';
import {SystemPermissionInterface} from '../../interfaces';

export class SystemPermission extends EntityUtil implements SystemPermissionInterface {
  public systemPermission: SystemPermission;
  public status: boolean;
  public logged: boolean;
  public showInNavbar: boolean;
  public showInSidebar: boolean;
  public name: string;
  public label: string;
  public uri: string;
  public icon: string;
  public keepLogin: boolean;

  constructor() {
    super();
    this.status = true;
    this.logged = true;
    this.showInNavbar = true;
    this.showInSidebar = true;
    this.name = '';
    this.icon = '';
    this.keepLogin = false;
  }

  compareFn(o1: SystemPermission, o2: SystemPermission) {
    return o1 && o2 ? o1.id === o2.id : o1 === o2;
  }

}
