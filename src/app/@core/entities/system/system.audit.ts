import {EntityUtil} from '../util/entity.util';
import {SystemAuditInterface} from '../../interfaces';
import {SystemUser} from './system.user';

export class SystemAudit extends EntityUtil implements SystemAuditInterface {
  public systemUser: SystemUser;
  public tableOperation: string;
  public typeOperation: string;
  public oldChange: string;

  constructor() {
    super();
    this.tableOperation = '';
    this.typeOperation = '';
    this.oldChange = '';
  }
}
