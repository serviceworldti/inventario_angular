import {EntityUtil} from '../util/entity.util';
import {SystemThemeInterface} from '../../interfaces';

export class SystemTheme extends EntityUtil implements SystemThemeInterface {
  public status: boolean;
  public name: string;

  constructor() {
    super();
    this.status = true;
    this.name = '';
  }
}
