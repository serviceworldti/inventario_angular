import {EntityUtil} from '../util/entity.util';
import {SystemReadDataFileInterface} from '../../interfaces';

export class SystemReadDataFile extends EntityUtil implements SystemReadDataFileInterface {
  public status: boolean;
  public name: string;
  public checksum: string;

  constructor() {
    super();
    this.status = true;
    this.name = '';
    this.checksum = '';
  }

}
