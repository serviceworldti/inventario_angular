import {EntityUtil} from '../util/entity.util';
import {SystemMailTypeInterface} from '../../interfaces/system/system.mail.type.interface';

export class SystemMailType extends EntityUtil implements SystemMailTypeInterface {
  public status: boolean;
  public name: string;

  constructor() {
    super();
    this.status = true;
    this.name = '';
  }
}
