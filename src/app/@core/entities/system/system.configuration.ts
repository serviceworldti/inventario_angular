import {EntityUtil} from '../util/entity.util';
import {SystemConfigurationInterface} from '../../interfaces';
import {SystemNotificationType} from './system.notification.type';
import {SystemOrganization} from './system.organization';
import {SystemTheme} from './system.theme';
import {WorldLanguage} from '../world/world.language';
import {SystemReportConfiguration} from './system.report.configuration';

export class SystemConfiguration extends EntityUtil implements SystemConfigurationInterface {
  public systemOrganization: SystemOrganization;
  public systemTheme: SystemTheme;
  public worldLanguage: WorldLanguage;
  public logo: string;
  public systemReportConfigurations: SystemReportConfiguration[];
  public systemNotificationTypes: SystemNotificationType[];

  constructor() {
    super();
    this.logo = '';
  }
}
