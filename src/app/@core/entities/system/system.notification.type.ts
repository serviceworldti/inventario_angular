import {EntityUtil} from '../util/entity.util';
import {SystemNotificationTypeInterface} from '../../interfaces';

export class SystemNotificationType extends EntityUtil implements SystemNotificationTypeInterface {
  public status: boolean;
  public name: string;

  constructor() {
    super();
    this.status = true;
    this.name = '';
  }

}
