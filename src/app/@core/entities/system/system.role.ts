import {EntityUtil} from '../util/entity.util';
import {SystemRoleInterface} from '../../interfaces';
import {SystemPermission} from './system.permission';
import {SystemAssignment} from './system.assignment';

export class SystemRole extends EntityUtil implements SystemRoleInterface {
  public status: boolean;
  public name: string;
  public systemPermissions: SystemPermission[];
  public systemAssignments: SystemAssignment[];

  constructor() {
    super();
    this.status = true;
    this.name = '';
  }
}
