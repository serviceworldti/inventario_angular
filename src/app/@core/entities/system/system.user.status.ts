import {EntityUtil} from '../util/entity.util';
import {SystemUserStatusInterface} from '../../interfaces/system/system.user.status.interface';

export class SystemUserStatus extends EntityUtil implements SystemUserStatusInterface {
  public status: boolean;
  public name: string;

  constructor() {
    super();
    this.status = true;
    this.name = '';
  }
}
