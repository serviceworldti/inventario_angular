import {EntityUtil} from '../util/entity.util';
import {SystemAssignmentInterface} from '../../interfaces';
import {SystemPermission} from './system.permission';

export class SystemAssignment extends EntityUtil implements SystemAssignmentInterface {
  public systemPermission: SystemPermission;
  public status: boolean;
  public showInMenu: boolean;
  public name: string;
  public method: string;

  constructor() {
    super();
    this.status = true;
    this.showInMenu = true;
    this.name = '';
    this.method = '';
  }
}
