import {EntityUtil} from '../util/entity.util';
import {SystemMailStatusInterface} from '../../interfaces/system/system.mail.status.interface';

export class SystemMailStatus extends EntityUtil implements SystemMailStatusInterface {
  public status: boolean;
  public name: string;

  constructor() {
    super();
    this.status = true;
    this.name = '';
  }
}
