import {EntityUtil} from '../util/entity.util';
import {SystemReportConfigurationInterface} from '../../interfaces';
import {SystemOrganization} from './system.organization';

export class SystemReportConfiguration extends EntityUtil implements SystemReportConfigurationInterface {
  public systemOrganization: SystemOrganization;
  public status: boolean;
  public name: string;
  public template: string;

  constructor() {
    super();
    this.name = '';
    this.template = '';
  }

}
