import {EntityUtil} from '../util/entity.util';
import {SystemOrganizationInterface} from '../../interfaces';
import {SystemGroup} from './system.group';

export class SystemOrganization extends EntityUtil implements SystemOrganizationInterface {
  public systemGroup: SystemGroup;
  public status: boolean;
  public name: string;
  public email: string;
  public code: string;

  constructor() {
    super();
    this.status = true;
    this.name = '';
    this.email = '';
    this.code = '';
  }
}
