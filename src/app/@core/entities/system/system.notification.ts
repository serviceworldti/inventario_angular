import {EntityUtil} from '../util/entity.util';
import {SystemNotificationInterface} from '../../interfaces';
import {SystemNotificationType} from './system.notification.type';
import {SystemUser} from './system.user';

export class SystemNotification extends EntityUtil implements SystemNotificationInterface {
  public systemNotificationType: SystemNotificationType;
  public systemUser: SystemUser;
  public status: boolean;

  constructor() {
    super();
    this.status = true;
  }
}
