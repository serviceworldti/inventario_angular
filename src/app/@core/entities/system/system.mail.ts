import {EntityUtil} from '../util/entity.util';
import {SystemMailInterface} from '../../interfaces/system/system.mail.interface';
import {SystemUser} from './system.user';
import {SystemMailType} from './system.mail.type';
import {SystemMailStatus} from './system.mail.status';

export class SystemMail extends EntityUtil implements SystemMailInterface {
  public systemUserSender: SystemUser;
  public systemUsers: SystemUser[];
  public mailType: SystemMailType;
  public mailStatus: SystemMailStatus;
  public subject: string;
  public description: string;

  constructor() {
    super();
    this.subject = '';
    this.description = '';
  }
}
