import {EntityUtil} from '../util/entity.util';
import {SystemGroupInterface} from '../../interfaces';
import {SystemRole} from './system.role';

export class SystemGroup extends EntityUtil implements SystemGroupInterface {
  public status: boolean;
  public name: string;
  public systemRoles: SystemRole[];

  constructor() {
    super();
    this.status = true;
    this.name = '';
  }
}
