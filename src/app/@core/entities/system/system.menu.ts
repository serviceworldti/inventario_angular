import {EntityUtil} from '../util/entity.util';
import {SystemMenuInterface} from '../../interfaces';

export class SystemMenu extends EntityUtil implements SystemMenuInterface {
  public status: boolean;
  public logged: boolean;
  public showInNavbar: boolean;
  public showInSidebar: boolean;
  public name: string;
  public label: string;
  public uri: string;
  public icon: string;
  public keepLogin: boolean;
  public expanded: boolean;
  public submenus: SystemMenu[];

  constructor() {
    super();
    this.status = true;
    this.logged = true;
    this.showInNavbar = true;
    this.showInSidebar = false;
    this.name = '';
    this.label = '';
    this.icon = '';
    this.keepLogin = false;
    this.expanded = false;
  }
}
