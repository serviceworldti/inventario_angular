import {EntityUtil} from '../util/entity.util';
import {SystemUserConfigurationInterface} from '../../interfaces';
import {SystemNotificationType} from './system.notification.type';
import {SystemTheme} from './system.theme';
import {SystemUser} from './system.user';
import {WorldLanguage} from '../world/world.language';

export class SystemUserConfiguration extends EntityUtil implements SystemUserConfigurationInterface {
  public systemUser: SystemUser;
  public systemTheme: SystemTheme;
  public worldLanguage: WorldLanguage;
  public systemNotificationTypes: SystemNotificationType[];

  constructor() {
    super();
  }
}
