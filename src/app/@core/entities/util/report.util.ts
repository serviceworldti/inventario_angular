import {ReportColumnUtil} from './report/report.column.util';

export class ReportUtil {

  // REQUEST
  public template: string;
  public name: string;
  public type: string;
  public columns: ReportColumnUtil[];
  public data: any[];

  // RESPONSE
  public base64: string;

  constructor() {
    this.template = '';
    this.name = 'report';
    this.type = 'pdf';
    this.base64 = '';
    this.columns = [];
    this.data = [];
  }

  /**
   * convierte la data en la entidad deseada
   *
   * @param entity
   */
  deepCopyData(entity?: any) {
    if (this.data !== null && this.data !== undefined && this.data.length > 0 && entity !== null) {
      return (new entity).deepCopy(this.data, entity);
    }

    return this.data;
  }

  /**
   * hace una copia fiel del objeto en la entidad
   *
   * @param json
   * @param entity
   */
  deepCopy(json: any, entity?: any) {
    if (json instanceof Array) {
      const entities: any[] = [];

      json.forEach((o: this) => {
        entities.push(Object.assign(entity !== undefined ? new entity : this, o));
      });

      return entities;
    } else {
      return Object.assign(this, json);
    }
  }

}
