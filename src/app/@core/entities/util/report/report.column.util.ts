export class ReportColumnUtil {

  public title: string;
  public type: string;
  public attribute: string;

  constructor() {
    this.title = '';
    this.type = '';
    this.attribute = '';
  }

}
