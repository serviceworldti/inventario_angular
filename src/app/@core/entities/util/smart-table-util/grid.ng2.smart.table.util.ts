import {GridNg2SmartTableAttrUtil} from './grid.ng2.smart.table.attr.util';
import {GridNg2SmartTableActionsUtil} from './grid.ng2.smart.table.actions.util';
import {GridNg2SmartTableFilterUtil} from './grid.ng2.smart.table.filter.util';
import {GridNg2SmartTableEditUtil} from './grid.ng2.smart.table.edit.util';
import {GridNg2SmartTableAddUtil} from './grid.ng2.smart.table.add.util';
import {GridNg2SmartTableDeleteUtil} from './grid.ng2.smart.table.delete.util';
import {GridNg2SmartTablePagerUtil} from './grid.ng2.smart.table.pager.util';
import {GridNg2SmartTableColumnsUtil} from './grid.ng2.smart.table.columns.util';

export class GridNg2SmartTableUtil {

  columns: any = [new GridNg2SmartTableColumnsUtil()];
  mode: string;
  hideHeader: boolean;
  hideSubHeader: boolean;
  noDataMessage: string;
  attr: GridNg2SmartTableAttrUtil;
  actions: GridNg2SmartTableActionsUtil;
  filter: GridNg2SmartTableFilterUtil;
  edit: GridNg2SmartTableEditUtil;
  add: GridNg2SmartTableAddUtil;
  delete: GridNg2SmartTableDeleteUtil;
  pager: GridNg2SmartTablePagerUtil;
  rowClassFunction: any;

  constructor() {
    this.columns = {};
    this.mode = 'external';
    this.hideHeader = false;
    this.hideSubHeader = false;
    this.noDataMessage = 'No Data Found';
    this.attr = new GridNg2SmartTableAttrUtil();
    this.actions = new GridNg2SmartTableActionsUtil();
    this.filter = new GridNg2SmartTableFilterUtil();
    this.edit = new GridNg2SmartTableEditUtil();
    this.add = new GridNg2SmartTableAddUtil();
    this.delete = new GridNg2SmartTableDeleteUtil();
    this.pager = new GridNg2SmartTablePagerUtil();
  }

}
