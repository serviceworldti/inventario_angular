export class GridNg2SmartTableAddUtil {

  inputClass: string;
  addButtonContent: string;
  createButtonContent: string;
  cancelButtonContent: string;
  confirmCreate: boolean;

  constructor() {
    this.inputClass = '';
    this.addButtonContent = '<i class="nb-plus"></i>';
    this.createButtonContent = 'Create';
    this.cancelButtonContent = 'Cancel';
    this.confirmCreate = false;
  }

}
