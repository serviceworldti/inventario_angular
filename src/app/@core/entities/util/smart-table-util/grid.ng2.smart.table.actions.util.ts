export class GridNg2SmartTableActionsUtil {

  columnTitle: string;
  add: boolean;
  edit: boolean;
  delete: boolean;
  position: string;

  constructor() {
    this.columnTitle = 'Actions';
    this.add = true;
    this.edit = false;
    this.delete = true;
    this.position = 'right';
  }

}
