export class GridNg2SmartTableDeleteUtil {

  deleteButtonContent: string;
  confirmDelete: boolean;

  constructor() {
    this.deleteButtonContent = '<i class="nb-trash"></i>';
    this.confirmDelete = false;
  }

}
