export class GridNg2SmartTableEditUtil {

  inputClass: string;
  editButtonContent: string;
  saveButtonContent: string;
  cancelButtonContent: string;
  confirmSave: boolean;

  constructor() {
    this.inputClass = '';
    this.editButtonContent = '<i class="nb-edit"></i>';
    this.saveButtonContent = 'Update';
    this.cancelButtonContent = 'Cancel';
    this.confirmSave = false;
  }

}
