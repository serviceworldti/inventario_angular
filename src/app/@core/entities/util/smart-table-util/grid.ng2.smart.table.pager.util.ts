export class GridNg2SmartTablePagerUtil {

  display: boolean;
  perPage: number;

  constructor() {
    this.display = true;
    this.perPage = 10;
  }

}
