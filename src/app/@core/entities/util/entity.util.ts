import {EntityUtilInterface} from '../../interfaces/util/entity.util.interface';

export class EntityUtil implements EntityUtilInterface {
  public id: number;
  public dateCreated: Date;
  public dateUpdate: Date;

  constructor() {
    this.dateCreated = new Date();
    this.dateUpdate = new Date();
  }

  /**
   * hace una copia fiel del objeto en la entidad
   *
   * @param json
   * @param entity
   */
  deepCopy(json: any, entity?: any) {
    if (json instanceof Array) {
      const entities: any[] = [];

      json.forEach((o: this) => {
        entities.push(Object.assign(entity !== undefined ? new entity : this, o));
      });

      return entities;
    } else {
      return Object.assign(this, json);
    }
  }

}
