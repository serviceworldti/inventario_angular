import {Injectable} from '@angular/core';
import {MomentDateAdapter} from '@angular/material-moment-adapter';

import {DatetimeService} from '../utils/datetime.service';
import * as moment from 'moment';

@Injectable()
export class DatetimeAdapter extends MomentDateAdapter {

  constructor(private _dateTimeService: DatetimeService) {
    super(_dateTimeService.locale);
  }

  public format(date: moment.Moment, displayFormat: string): string {
    return date.locale(this._dateTimeService.locale)
      .format(this._dateTimeService.format.toLocaleUpperCase());
  }
}
