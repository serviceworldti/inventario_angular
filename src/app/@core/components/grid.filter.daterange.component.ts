import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormControl} from '@angular/forms';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {DefaultFilter} from 'ng2-smart-table';

@Component({
  selector: 'ngx-date-range-custom-filter',
  template: `
      <mat-form-field class="col-12">
          <input matInput [formControl]="inputControl" [nbDatepicker]="formpicker" placeholder="{{column.title}}" readonly>
          <nb-rangepicker #formpicker></nb-rangepicker>
      </mat-form-field>
  `,
})
export class GridFilterDaterangeComponent extends DefaultFilter implements OnInit, OnChanges {

  inputControl = new FormControl();

  constructor() {
    super();
  }

  ngOnInit() {
    if (this.query) {
      this.inputControl.setValue(this.query);
    }
    this.inputControl.valueChanges
      .pipe(
        distinctUntilChanged(),
        debounceTime(this.delay),
      )
      .subscribe((value: string) => {
        const start = Date.parse(this.inputControl.value.start);
        const end = this.inputControl.value.end;

        this.query = start + '-' + (end !== undefined && end !== null ? Date.parse(end) + 86399999 : start + 86399999);
        this.setFilter();
      });
  }

  onChange(changes: SimpleChanges) {
    if (changes.query) {
      // this.inputControl.setValue(this.query);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.query) {
      // this.inputControl.setValue(this.query);
    }
  }
}
