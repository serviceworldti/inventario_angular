import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {NbDialogService} from '@nebular/theme';
import {ViewCell} from 'ng2-smart-table';
import {HttpService} from '../utils/http.service';
import {FileService} from '../utils/file.service';
import {FilesDownloadComponent} from './files.download.component';
import {FilePreviewModel} from 'ngx-awesome-uploader';

@Component({
  selector: 'ngx-files-view-component-gird',
  template: `
    <span #spanRef *ngIf="loading" (click)="nothing()">Cargando archivos...</span>
    <button nbButton class="w-100" *ngIf="myFiles.length > 0" (click)="viewFiles()"><i class="fas fa-paperclip"></i> &nbsp; {{'app.view' | translate}} </button>
  `,
})
export class FilesViewComponent implements ViewCell, OnInit, AfterViewInit {

  @ViewChild('spanRef', {static: false}) spanRef: ElementRef;
  @Input() value: any;
  @Input() rowData: any;
  public myFiles: FilePreviewModel[] = [];
  loading: boolean = true;

  constructor(
    private oHttpService: HttpService,
    private oFileService: FileService,
    private dialogService: NbDialogService,
  ) {
  }

  ngOnInit() {
    const t = this;

    this.oHttpService.get(null, {
      path: this.value.path,
    }).then((files: any[]) => {
      files.forEach((file) => {
        const fl: File = t.oFileService.base64ToFile(file[this.value.imagen]);

        t.myFiles.push(t.oFileService.fileToPreviewModel(fl));
      });

      if (this.loading === true) {
        this.click();
      }
    });
  }

  ngAfterViewInit() {
    if (this.loading === false) {
      this.click();
    }
  }

  nothing() {
  }

  viewFiles() {
    this.dialogService.open(FilesDownloadComponent, {
      backdropClass: 'modalNebular',
      context: {
        myFiles: this.myFiles,
      },
    });
  }

  private click(): void {
    if (this.spanRef !== undefined) {
      const el: HTMLElement = this.spanRef.nativeElement;
      this.loading = false;
      el.click();
    }
  }

}
