import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormControl} from '@angular/forms';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {DefaultFilter} from 'ng2-smart-table';
import {DateAdapter, MAT_DATE_FORMATS} from '@angular/material/core';
import {DatetimeAdapter} from './datetime.adapter';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'ngx-date-custom-filter',
  template: `
    <mat-form-field class="col-12">
      <input matInput [formControl]="inputControl" [matDatepicker]="dateFilter" placeholder="{{column.title}}" readonly>
      <mat-datepicker-toggle matSuffix [for]="dateFilter"></mat-datepicker-toggle>
      <mat-datepicker #dateFilter disabled="false"></mat-datepicker>
    </mat-form-field>
  `,
  providers: [
    DatetimeAdapter,
    {provide: DatetimeAdapter, useClass: DatetimeAdapter},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class GridFilterDateComponent extends DefaultFilter implements OnInit, OnChanges {

  inputControl = new FormControl();

  constructor() {
    super();
  }

  ngOnInit() {
    if (this.query) {
      this.inputControl.setValue(this.query);
    }
    this.inputControl.valueChanges
      .pipe(
        distinctUntilChanged(),
        debounceTime(this.delay),
      )
      .subscribe((value: string) => {
        this.query = this.inputControl.value.format('x').toString();
        this.setFilter();
      });
  }

  onChange(changes: SimpleChanges) {
    if (changes.query) {
      // this.inputControl.setValue(this.query);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.query) {
      // this.inputControl.setValue(this.query);
    }
  }
}
