import {AfterViewInit, Component, Input, ViewChild} from '@angular/core';
import {FileService} from '../utils/file.service';
import {FilePickerComponent, FilePreviewModel} from 'ngx-awesome-uploader';

@Component({
  selector: 'ngx-files-download-component-gird',
  template: `
    <nb-card>
      <nb-card-header>{{'app.download_files' | translate}}</nb-card-header>
      <nb-card-body>
        <div class="uploader-wrapper w-100">
          <ngx-file-picker #uploader class="w-100 simpleButton" [adapter]="adapter" [showeDragDropZone]="false">
            <div class="dropzoneTemplate"></div>
          </ngx-file-picker>
        </div>
      </nb-card-body>
      <nb-card-footer>
        <button nbButton class="w-50" *ngFor="let o of myFiles" (click)="downloadFile(o)">
          <i class="fas fa-paperclip"></i> &nbsp; {{o.fileName}}
        </button>
      </nb-card-footer>
    </nb-card>
  `,
})
export class FilesDownloadComponent implements AfterViewInit {

  @ViewChild('uploader', {static: false}) uploader: FilePickerComponent;
  @Input() myFiles: FilePreviewModel[] = [];
  adapter;

  constructor(
    private oFileService: FileService,
  ) {
  }

  ngAfterViewInit() {
    const t = this;

    this.myFiles.forEach((f) => {
      t.uploader.files.push(f);
    });
  }

  downloadFile(o: FilePreviewModel) {
    this.oFileService.downloadFilePreviewModel(o);
  }

}
