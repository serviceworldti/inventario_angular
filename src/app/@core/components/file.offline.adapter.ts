import {FilePickerAdapter, FilePreviewModel} from 'ngx-awesome-uploader';
import {Observable} from 'rxjs';

export class FileOfflineAdapter extends FilePickerAdapter {

  constructor() {
    super();
  }

  public uploadFile(o: FilePreviewModel): Observable<number | string> {
    return new Observable((observer) => {
      observer.next('');

      return {
        unsubscribe: () => {
          return '';
        },
      };
    });
  }

  public removeFile(o: FilePreviewModel): Observable<any> {
    return new Observable((observer) => {
      observer.next('');

      return {
        unsubscribe: () => {
          return '';
        },
      };
    });
  }

}
