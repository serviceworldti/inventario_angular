import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormControl} from '@angular/forms';
import {debounceTime} from 'rxjs/operators';
import {DefaultFilter} from 'ng2-smart-table';

@Component({
  selector: 'ngx-checkbox-custom-filter',
  template: `
      <div class="w-100" style="margin: auto; text-align: center;">
          <mat-slide-toggle [formControl]="inputControl" ng-true-value="'t'" ng-false-value="'f'"></mat-slide-toggle>
          <p><a href="#" *ngIf="filterActive" (click)="resetFilter($event)">{{column.getFilterConfig()?.resetText || 'reset'}}</a></p>
      </div>
  `,
})
export class GridFilterCheckboxComponent extends DefaultFilter implements OnInit, OnChanges {

  filterActive: boolean = false;
  inputControl = new FormControl();

  constructor() {
    super();
  }

  ngOnInit() {
    this.changesSubscription = this.inputControl.valueChanges
      .pipe(debounceTime(this.delay))
      .subscribe((checked: boolean) => {
        this.filterActive = true;
        this.query = checked ? 'true' : 'false';
        this.setFilter();
      });
  }

  onChange(changes: SimpleChanges) {
    if (changes.query) {
      // this.inputControl.setValue(this.query);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.query) {
      // this.inputControl.setValue(this.query);
    }
  }

  resetFilter(event: any) {
    event.preventDefault();
    this.query = '';
    this.inputControl.setValue(false, {emitEvent: false});
    this.filterActive = false;
    this.setFilter();
  }
}
