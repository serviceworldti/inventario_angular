import {Component, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {NgControl} from '@angular/forms';
import {debounceTime, distinctUntilChanged, skip} from 'rxjs/operators';
import {DefaultFilter} from 'ng2-smart-table';

@Component({
  selector: 'ngx-select-custom-filter',
  template: `
      <mat-form-field class="col-12">
          <mat-label>Seleccione</mat-label>
          <mat-select [ngClass]="inputClass" #inputControl [(ngModel)]="query">
              <mat-option [value]="0"> {{ column.getFilterConfig().selectText }} </mat-option>
              <mat-option *ngFor="let option of column.getFilterConfig().list" [value]="option.value"> {{option.title}} </mat-option>
          </mat-select>
      </mat-form-field>
  `,
})
export class GridFilterSelectComponent extends DefaultFilter implements OnInit, OnChanges {

  @ViewChild('inputControl', {read: NgControl, static: true}) inputControl: NgControl;

  constructor() {
    super();
  }

  ngOnInit() {
    this.inputControl.valueChanges
      .pipe(
        skip(1),
        distinctUntilChanged(),
        debounceTime(this.delay),
      )
      .subscribe((value: string) => this.setFilter());
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.query && changes.column === undefined) {
      this.inputControl = changes.query.currentValue;
    }
  }
}
