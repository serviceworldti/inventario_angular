import {Component, Input} from '@angular/core';
import {NbDialogRef} from '@nebular/theme';

@Component({
  selector: 'ngx-message',
  template: `
    <nb-card>
      <nb-card-header>{{ title | translate }}</nb-card-header>
      <nb-card-body>
        {{ message | translate}}
        <div *ngIf="content" [innerHTML]="content"></div>
      </nb-card-body>
      <nb-card-footer>
        <button nbButton hero status="primary" (click)="acceptVoid()">{{'app.accept' | translate}}</button> &nbsp;
        <button nbButton hero status="danger" (click)="declineVoid()" *ngIf="decline">{{'app.decline' | translate}}</button>
      </nb-card-footer>
    </nb-card>
  `,
})
export class AlertMessageComponent {

  @Input() title: string;
  @Input() message: string;
  @Input() content: string;
  @Input() accept: any;
  @Input() decline: any;
  @Input() acceptTitle: string = 'app.dismiss';
  @Input() declineTitle: string = 'app.decline';

  constructor(
    protected ref: NbDialogRef<AlertMessageComponent>,
  ) {
  }

  dismiss() {
    this.ref.close();
  }

  acceptVoid() {
    if (this.accept !== undefined && this.accept !== null) {
      this.accept();
    }

    this.dismiss();
  }

  declineVoid() {
    this.decline();
    this.dismiss();
  }

}
