import {Injectable} from '@angular/core';
import {FilePreviewModel, UploaderCaptions, ValidationError} from 'ngx-awesome-uploader';
import {FileOfflineAdapter} from '../components/file.offline.adapter';
import {ObjectService} from './object.service';
import {AlertService} from './alert.service';

@Injectable({
  providedIn: 'root',
})
export class FileService {

  public adapter = new FileOfflineAdapter();
  public captions: UploaderCaptions = {
    dropzone: {
      title: this.object().translate().instant('app.dropzone.title'),
      or: this.object().translate().instant('app.dropzone.or'),
      browse: this.object().translate().instant('app.dropzone.browse'),
    },
    previewCard: {
      remove: this.object().translate().instant('app.previewCard.remove'),
      uploadError: this.object().translate().instant('app.previewCard.uploadError'),
    },
    cropper: {
      crop: this.object().translate().instant('app.cropper.crop'),
      cancel: this.object().translate().instant('app.cropper.cancel'),
    },
  };
  private stringSave: string = '=-=';

  constructor(
    private oObjectService: ObjectService,
    private oAlertService: AlertService,
  ) {
  }

  /**
   * object
   */
  public object(): ObjectService {
    return this.oObjectService;
  }

  /**
   * Elimina un archivo de la lista
   *
   * @param e
   * @param myFiles
   * @param myFilesRemove
   */
  onRemoveFile(e: FilePreviewModel, myFiles: FilePreviewModel[], myFilesRemove?: FilePreviewModel[]) {
    myFiles.forEach((v: FilePreviewModel, i: number) => {
      if (v.fileName === e.fileName && v.fileId === e.fileId) {
        myFiles.splice(i, 1);

        if (myFilesRemove !== undefined) {
          myFilesRemove.push(e);
        }
      }
    });
  }

  /**
   * Agrega un archivo a la lista
   *
   * @param e
   * @param myFiles
   */
  onAddFile(e: FilePreviewModel, myFiles: FilePreviewModel[]) {
    myFiles.push(e);
  }

  /**
   * valida el error en el archivo
   *
   * @param e
   */
  onValidationError(e: ValidationError) {
    this.oAlertService.openDialog({
      title: 'error.title.426',
      message: 'app.message_delete_file',
      acceptTitle: 'app.accept',
    });
  }

  getFileString(v: FilePreviewModel, isProgress?: boolean): Promise<any> {
    const t = this;

    return new Promise<any>((resolve, reject) => {
      const r: FileReader = new FileReader();
      isProgress = t.object().getDefault(isProgress, true);

      if (v === undefined || v === null) {
        reject(v);
        return;
      }

      if (!t.isSave(v)) {
        v.fileName = (new Date()).getTime().toString() + v.fileName;
      }

      if (isProgress) {
        t.object().loading().setProgress({status: 'load_file', message: 10});
      }

      r.onload = (function (tf) {
        return function (e) {
          t.object().loading().resetProgress();
          resolve('name:' + v.fileName.replace(/[^a-zA-Z0-9\.]/ig, '') +
            ';data:' + v.file.type + ';base64,' + window.btoa(e.target.result));
        };
      })(v);

      r.readAsBinaryString(v.file);
    });
  }

  getFilesString(fs: FilePreviewModel[], isProgress?: boolean): Promise<any> {
    const t = this;

    return new Promise<any>((resolve, reject) => {
      let r: FileReader;
      const filesString: string[] = [];
      let counter: number = 0;
      isProgress = t.object().getDefault(isProgress, true);

      if (isProgress) {
        t.object().loading().setProgress({status: 'load_file', message: 10});
      }

      fs.forEach((v: FilePreviewModel, i: number) => {
        r = new FileReader();
        counter++;
        t.object().loading().setProgress({
          status: 'load_file',
          message: Math.round(((100 * counter) / fs.length)),
        });

        r.onload = (function (tf) {
          return function (e) {
            filesString.push('name:' + v.fileName.replace(/[^a-zA-Z0-9\.]/ig, '') +
              'data:' + v.file.type + ';base64,' + window.btoa(e.target.result));

            if (counter >= fs.length) {
              t.object().loading().resetProgress();
              resolve(filesString.join('|'));
            }
          };
        })(v);

        r.readAsBinaryString(v.file);
      });
    });
  }

  getDataBase64FromString(base64: string): { filename: string, data: string, base64: string } {
    let filename = (new Date()).toString();
    if (base64.trim() === null || base64.trim() === '') {
      return null;
    }

    const datas = base64.split(',');
    const data = datas[0].split(';');
    const b64 = datas[1];
    const mime = (data.length > 2 ? data[1] : data[0]).replace('data:', '');
    filename = (data.length > 2 ? data[0] : filename + '.' + mime.split('/')[1]).replace('name:', '');

    return {
      filename: filename,
      data: mime,
      base64: b64,
    };
  }

  base64ToFile(base64: string): any {
    const data = this.getDataBase64FromString(base64);

    if (data === null) {
      return '';
    }

    const bstr = atob(data.base64);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);

    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }

    return new File([u8arr], this.stringSave + data.filename, {type: data.data});
  }

  public fileToPreviewModel(o: File): FilePreviewModel {
    const f: FilePreviewModel = {
      fileId: o.name,
      file: o,
      fileName: o.name,
    };

    return f;
  }

  /**
   * verifica si el archivo ha sido guardado anteriormente
   * @param v
   */
  isSave(v: FilePreviewModel): boolean {
    return (v.fileName.indexOf(this.stringSave) > -1);
  }

  /**
   * descarga un archivo
   *
   * @param file
   */
  public downloadFilePreviewModel(file: FilePreviewModel): void {
    this.getFileString(file).then((o) => {
      this.downloadFileFromBase64(o);
    });
  }

  /**
   * descarga un archivo
   *
   * @param name
   * @param base64
   * @param type
   */
  public downloadFileFromBase64(base64: string, name?: string, type?: string): void {
    const data = this.getDataBase64FromString(base64);
    type = this.object().getDefault(type, 'file');
    name = this.object().getDefault(name, `archivo.${type}`);
    data.filename = data.filename.trim().length < 1 ? name : data.filename;
    data.base64 = `data:${data.data};base64,${data.base64}`;

    this.downloadFile(data.base64, data.filename);
  }

  /**
   * descarga un archivo
   *
   * @param name
   * @param base64
   * @param type
   */
  public downloadFile(linkSource: string, name: string): void {
    const downloadLink = document.createElement('a');

    downloadLink.href = linkSource;
    downloadLink.download = name;
    downloadLink.click();
  }

}
