import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {FormGroup} from '@angular/forms';
import {DomSanitizer} from '@angular/platform-browser';
import {TranslateService} from '@ngx-translate/core';
import {
  NbDialogService,
  NbIconLibraries,
  NbMediaBreakpointsService,
  NbMenuService,
  NbSidebarService,
  NbThemeService,
} from '@nebular/theme';
import {LoadingService} from './loading.service';
import {DatetimeService} from './datetime.service';
import {LayoutService} from './layout.service';

@Injectable({
  providedIn: 'root',
})
export class ObjectService {

  public idCompare = 'id';
  private days: string[] = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
  ];
  private months: string[] = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  private itemsError: any = {
    'required': 'Debe ingresar información en el campo',
    'min': 'Debe ingresar un valor mayor de: ${min.min}',
    'max': 'Debe ingresar un valor menor de: ${max.max}',
    'minlength': 'Debe ingresar más de ${minlength.requiredLength} carácteres',
    'maxlength': 'Debe ingresar más de ${maxlength.requiredLength} carácteres',
    'email': 'Debe ingresar una dirección de correo válida',
    'pattern': 'El valor introducido no coincide con el patrón indicado',
  };

  constructor(
    private oNbSidebarService: NbSidebarService,
    private oNbMenuService: NbMenuService,
    private oNbThemeService: NbThemeService,
    private oNbIconLibraries: NbIconLibraries,
    private oLayoutService: LayoutService,
    private oNbMediaBreakpointsService: NbMediaBreakpointsService,
    private oTranslateService: TranslateService,
    private oLoadingService: LoadingService,
    private oDateTimeService: DatetimeService,
    private dialogService: NbDialogService,
    private oDomSanitizer: DomSanitizer,
    private oRouter: Router,
  ) {
    this.translateErrors();

    this.oTranslateService.onTranslationChange.subscribe(() => {
      this.translateErrors();
    });
  }

  /**
   * router
   */
  public router(): Router {
    return this.oRouter;
  }

  /**
   * NbSidebar
   */
  public NbSidebar(): NbSidebarService {
    return this.oNbSidebarService;
  }

  /**
   * NbMenu
   */
  public NbMenu(): NbMenuService {
    return this.oNbMenuService;
  }

  /**
   * NbTheme
   */
  public NbTheme(): NbThemeService {
    return this.oNbThemeService;
  }

  /**
   * NbIconLibraries
   */
  public NbIconLibraries(): NbIconLibraries {
    return this.oNbIconLibraries;
  }

  /**
   * layout
   */
  public layout(): LayoutService {
    return this.oLayoutService;
  }

  /**
   * NbMediaBreakpoints
   */
  public NbMediaBreakpoints(): NbMediaBreakpointsService {
    return this.oNbMediaBreakpointsService;
  }

  /**
   * translate
   */
  public translate(): TranslateService {
    return this.oTranslateService;
  }

  /**
   * loading
   */
  public loading(): LoadingService {
    return this.oLoadingService;
  }

  /**
   * time
   */
  public time(): DatetimeService {
    return this.oDateTimeService;
  }

  /**
   * dialog
   */
  public dialog(): NbDialogService {
    return this.dialogService;
  }

  /**
   * sanitizer
   */
  public sanitizer(): DomSanitizer {
    return this.oDomSanitizer;
  }

  /**
   * determina si el objeto es null
   *
   * @param o
   */
  public isNull(o: any) {
    return o === undefined || o === null;
  }

  /**
   * determina si el objeto no es null
   *
   * @param o
   */
  public isNotNull(o: any) {
    return !this.isNull(o);
  }

  /**
   * determina si es de un tipo
   *
   * @param o
   * @param type
   */
  public isType(o: any, type: string) {
    return this.isNull(o) && typeof o !== type;
  }

  /**
   * determina si no esta en una lista de tipos
   *
   * @param o
   * @param types
   */
  public isNotType(o: any, types: string[]) {
    let pass: boolean = true;

    if (this.isNull(o)) {
      return false;
    }

    types.every(t => {
      if (typeof o === t) {
        pass = false;
      }
    });

    return pass;
  }

  /**
   * obtiene un valor predefinido de un valor
   *
   * @param o
   * @param d
   */
  public getDefault(o: any, d: any) {
    return this.isNull(o) ? d : o;
  }

  /**
   * obtiene un tamaño de un objeto
   *
   * @param o
   */
  public getSize(o: any[]): number {
    if (!this.isNull(o)) {
      return (!this.isNull(o.length)) ? o.length : 0;
    }

    return 0;
  }

  /**
   * realiza una espera segun lo que se le indique
   *
   * @param time
   */
  public timeout(time: number): Promise<any> {
    return new Promise<any>((resolve) => {
      setTimeout(() => {
        resolve();
      }, time);
    });
  }

  /**
   * traduce los errores en el formulario
   */
  translateErrors() {
    const t = this;
    const m: string[] = [];

    for (const key in this.itemsError) {
      if (this.itemsError.hasOwnProperty(key)) {
        m.push('error.' + key);
      }
    }

    this.oTranslateService.stream(m).subscribe((data) => {
      for (const key in t.itemsError) {
        if (t.itemsError.hasOwnProperty(key)) {
          t.itemsError[key] = t.getItem(data, key);
        }
      }
    });
  }

  /**
   * compara strings
   *
   * @param o1
   * @param o2
   */
  public compare(o1: string, o2: string) {
    return o1 === o2;
  }

  /**
   * ordenamiento
   *
   * @param o1
   * @param o2
   */
  public compareFn(o1: any, o2: any) {
    return o1 && o2 ? o1[this.idCompare] === o2[this.idCompare] : o1 === o2;
  }

  /**
   * compara contraseñas
   *
   * @param group
   */
  public compareFormPassword(group: FormGroup) {
    const password = group.controls.password.value;
    const repassword = group.controls.repassword.value;

    return password === repassword ? null : {notSame: true};
  }

  /**
   * Inicializa el array
   *
   * @param list
   */
  public flushArray(list: any[]): any[] {
    return (list) ? list : [];
  }

  /**
   * retorna los dias de la semana
   */
  public getDays(): string[] {
    return this.days;
  }

  /**
   * retorna los dias de la semana
   *
   * @param index
   */
  public getDay(index: number): string {
    return this.days[index] !== undefined ? this.days[index] : this.days[index--];
  }

  /**
   * retorna los meses del año
   */
  public getMonths(): string[] {
    return this.months;
  }

  /**
   * retorna los meses del año
   *
   * @param index
   */
  public getMonth(index: number): string {
    return this.months[index] !== undefined ? this.months[index] : this.months[index--];
  }

  /**
   * clona un objeto
   *
   * @param o
   */
  public clone(o: any): any {
    let copy;

    // Handle the 3 simple types, and null or undefined
    if (null == o || 'object' !== typeof o) return o;

    // Handle Date
    if (o instanceof Date) {
      copy = new Date();
      copy.setTime(o.getTime());

      return copy;
    }

    // Handle Array
    if (o instanceof Array) {
      copy = [];

      for (let i = 0, len = o.length; i < len; i++) {
        copy[i] = this.clone(o[i]);
      }

      return copy;
    }

    // Handle Object
    if (o instanceof Object) {
      copy = {};

      for (const attr in o) {
        if (o.hasOwnProperty(attr)) copy[attr] = this.clone(o[attr]);
      }

      return copy;
    }

    throw new Error('Unable to copy obj! Its type isnt supported.');
  }

  /**
   * Obtiene un mensaje de error para mostrarlo en el formulario
   *
   * @param o
   * @param m
   */
  getErrorMessage(o: any, m?: string): string {
    let errorMessage: string = '';
    const expresion = /\$\{([a-zA-Z0-9\.]+)\}/gm;

    for (const key in this.itemsError) {
      if (this.itemsError.hasOwnProperty(key) && o.hasError(key) && o.errors[key] !== undefined) {
        let value = this.itemsError[key];

        if (value.indexOf('${') > -1) {
          let v: string = '';
          const strings = value.match(expresion)[0];
          const count = strings.replace(/\$|\{|\}/gm, '').split('.', 4);

          if (count.length > 0) {
            if (count.length === 1) {
              v = o.errors[count[0]];
            } else if (count.length === 2) {
              v = o.errors[count[0]][count[1]];
            } else if (count.length === 3) {
              v = o.errors[count[0]][count[1]][count[2]];
            } else {
              v = o.errors[count[0]][count[1]][count[2]][count[3]];
            }
          }

          value = value.replace(/\$\{.+\}/, v);
        }

        errorMessage = value;
      }
    }

    return errorMessage;
  }

  /**
   * obtiene un valor de un array o retorna el valor por defecto
   *
   * @param data
   * @param search
   * @param def
   */
  private getItem(data: any, search: string): string {
    return data !== undefined ? (data['error.' + search] !== undefined ? data['error.' + search] : search) : search;
  }

}
