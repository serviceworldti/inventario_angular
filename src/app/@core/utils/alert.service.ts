import {EventEmitter, Injectable, Output} from '@angular/core';
import {AlertMessageComponent} from '../components/alert.message.component';
import {ObjectService} from './object.service';

@Injectable({
  providedIn: 'root',
})
export class AlertService {

  @Output() alert: EventEmitter<any> = new EventEmitter();
  public dataAlert: any = {
    type: 'success',
    message: 'Exito!',
    open: false,
  };

  constructor(
    private oObjectService: ObjectService,
  ) {
  }

  /**
   * object
   */
  public object(): ObjectService {
    return this.oObjectService;
  }

  /**
   * Setea una alerta
   *
   * @param message
   * @param type
   * @param keep
   */
  public setAlert(message?: string, type?: string, keep?: boolean) {
    keep = keep === undefined ? false : keep;
    this.dataAlert.message = this.object().getDefault(message, 'Exito!');
    this.dataAlert.type = this.object().getDefault(type, 'success');
    this.dataAlert.open = true;

    this.object().timeout(10).then(() => this.alert.emit(this.dataAlert));

    if (!keep) {
      this.object().timeout(8 * 1000).then(() => this.endAlert());
    }
  }

  /**
   * culmina la alerta
   */
  public endAlert() {
    this.dataAlert.open = false;
    this.object().timeout(2 * 1000).then(() => this.alert.emit(this.dataAlert));
  }

  /**
   * Abre una ventana de dialogo
   *
   * @param context
   */
  public openDialog(context: {
    title: string,
    message?: string,
    content?: any,
    accept?: any,
    acceptTitle?: string,
    decline?: any,
    declineTitle?: string,
  }) {
    context.content = context.content !== undefined ?
      this.object().sanitizer().bypassSecurityTrustHtml(context.content) : context.content;

    this.object().dialog().open(AlertMessageComponent, {
      context: context,
    });
  }

}
