import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {FormGroup} from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class FormService {

  private itemsError: any = {
    'required': 'Debe ingresar información en el campo',
    'min': 'Debe ingresar un valor mayor de: ${min.min}',
    'max': 'Debe ingresar un valor menor de: ${max.max}',
    'minlength': 'Debe ingresar más de ${minlength.requiredLength} carácteres',
    'maxlength': 'Debe ingresar más de ${maxlength.requiredLength} carácteres',
    'email': 'Debe ingresar una dirección de correo válida',
    'pattern': 'El valor introducido no coincide con el patrón indicado',
  };

  constructor(
    private oTranslateService: TranslateService,
  ) {
    const z = this;
    this.translateErrors();

    this.translate().onTranslationChange.subscribe(() => {
      z.translateErrors();
    });
  }

  translate(): TranslateService {
    return this.oTranslateService;
  }

  /**
   * traduce los errores en el formulario
   */
  translateErrors() {
    const z = this;
    const m: string[] = [];

    for (const key in this.itemsError) {
      if (this.itemsError.hasOwnProperty(key)) {
        m.push('error.' + key);
      }
    }

    this.translate().stream(m).subscribe((data) => {
      for (const key in z.itemsError) {
        if (z.itemsError.hasOwnProperty(key)) {
          z.itemsError[key] = z.getItem(data, key);
        }
      }
    });
  }

  /**
   * previene los eventos
   *
   * @param event
   */
  disableKey(event: KeyboardEvent): void {
    const target: any = event.target;
    target.disabled = true;
    event.preventDefault();
  }

  /**
   * obtiene un valor de un array o retorna el valor por defecto
   *
   * @param data
   * @param search
   */
  private getItem(data: any, search: string): string {
    return data !== undefined ? (data['error.' + search] !== undefined ? data['error.' + search] : search) : search;
  }

  /**
   * ordenamiento
   *
   * @param o1
   * @param o2
   */
  public compareFn(o1: any, o2: any) {
    return o1 && o2 ? o1.id === o2.id : o1 === o2;
  }

  /**
   * Inicializa el array
   *
   * @param list
   */
  public flushArrayIfNotEmpty(list: any[]): any[] {
    return (list) ? list : [];
  }

  /**
   *
   * @param arr
   * @param obj
   */
  public upsert(arr: any[], obj: any): any[] {
    if (obj) {
      const index = arr.findIndex((e) => e.id === obj.id);

      if (index === -1) {
        arr.push(obj);
      }
    }

    return arr;
  }

  /**
   * compara contraseñas
   *
   * @param group
   */
  public compareFormPassword(group: FormGroup) {
    const password = group.controls.password.value;
    const repassword = group.controls.repassword.value;

    return password === repassword ? null : {notSame: true};
  }

  /**
   * compara contraseñas
   *
   * @param password
   * @param repassword
   */
  public comparePassword(password: string, repassword: string) {
    return password === repassword;
  }

  /**
   * Obtiene un mensaje de error para mostrarlo en el formulario
   *
   * @param o
   * @param m
   */
  getErrorMessage(o: any, m?: string): string {
    let errorMessage: string = '';
    const expresion = /\$\{([a-zA-Z0-9\.]+)\}/gm;

    for (const key in this.itemsError) {
      if (this.itemsError.hasOwnProperty(key) && o.hasError(key) && o.errors[key] !== undefined) {
        let value = this.itemsError[key];

        if (value.indexOf('${') > -1) {
          let v: string = '';
          const strings = value.match(expresion)[0];
          const count = strings.replace(/\$|\{|\}/gm, '').split('.', 4);

          if (count.length > 0) {
            if (count.length === 1) {
              v = o.errors[count[0]];
            } else if (count.length === 2) {
              v = o.errors[count[0]][count[1]];
            } else if (count.length === 3) {
              v = o.errors[count[0]][count[1]][count[2]];
            } else {
              v = o.errors[count[0]][count[1]][count[2]][count[3]];
            }
          }

          value = value.replace(/\$\{.+\}/, v);
        }

        errorMessage = value;
      }
    }

    return errorMessage;
  }

}
