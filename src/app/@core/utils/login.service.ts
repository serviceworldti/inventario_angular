import {EventEmitter, Injectable, Output} from '@angular/core';
import {HttpService} from './http.service';
import {FileService} from './file.service';
import {SystemUser} from '../entities';

@Injectable({
  providedIn: 'root',
})
export class LoginService {

  dataUser: SystemUser = new SystemUser();
  @Output() login: EventEmitter<SystemUser> = new EventEmitter();

  constructor(
    private oHttpService: HttpService,
    private oFileService: FileService,
  ) {
    this.dataUser = this.getUser();

    this.http().storage().object().timeout(100).then(() => {
      this.login.emit(this.dataUser);
    });
  }

  /**
   * http
   */
  public http(): HttpService {
    return this.oHttpService;
  }

  /**
   * file
   */
  public file(): FileService {
    return this.oFileService;
  }

  /**
   * obtiene el usuario almacenado en memoria
   */
  getUser(): SystemUser {
    return this.http().storage().getStorage(this.http().stgAuthSession, {
      def: new SystemUser(),
      jsonParse: true,
    });
  }

  /**
   * obtiene si el usuario se ha logueado
   */
  getLogin(): boolean {
    return this.dataUser.username.trim().length > 0;
  }

  /**
   * obtiene los datos basicos del usuario
   */
  getUserPhoto(): any {
    const user = this.getUser();
    const name = this.http().storage().object().getDefault(user.username, '');
    let picture = 'assets/img/avatar.png';

    if (!this.http().storage().object().isNull(user)) {
      if (!this.http().storage().object().isNull(user.photo) && user.photo.trim() !== '') {
        const dataPhoto = this.file().getDataBase64FromString(user.photo);
        picture = 'data:' + dataPhoto.data + ';base64,' + dataPhoto.base64;
      }
    }

    return {
      name: name,
      picture: picture,
    };
  }

  /**
   * realiza el inicio de sesion
   *
   * @param o
   */
  setLogin(o: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.http().post<SystemUser>(o, {
        remote: true,
        path: this.http().storage().getEnviroment().securityUri + '/login',
        showSuccess: false,
      }, {}).then((data) => {
        this.http().storage().setStorage(this.http().stgAuthSession, data, {jsonParse: true});
        this.dataUser = data;
        resolve(data);
        this.http().storage().object().timeout(100).then(() => {
          this.login.emit(data);
        });
      }, reject);
    });
  }

  /**
   * realiza el inicio de sesion
   *
   * @param o
   */
  getRecovery(o: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.http().post<SystemUser>(o, {
        remote: true,
        path: this.http().storage().getEnviroment().securityUri + '/recovery',
        showSuccess: false,
      }, {}).then((data) => {
        resolve(data);
      }, (data) => {
        reject(data);
      });
    });
  }

  /**
   * realiza el inicio de sesion
   *
   * @param o
   */
  setRecovery(o: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.http().post<SystemUser>(o, {
        remote: true,
        path: this.http().storage().getEnviroment().securityUri + '/recovery',
      }, {}).then((data) => {
        resolve(data);
      }, (data) => {
        reject(data);
      });
    });
  }

  /**
   * realiza el inicio de sesion
   *
   * @param o
   */
  setChangePassword(o: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.http().post<SystemUser>(o, {
        remote: true,
        path: this.http().storage().getEnviroment().securityUri + '/change_password',
      }, {}).then((data) => {
        resolve(data);
      }, (data) => {
        reject(data);
      });
    });
  }

  /**
   * registra un usuario
   *
   * @param o
   */
  setRegister(o: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.http().post<SystemUser>(o, {
        remote: true,
        path: this.http().storage().getEnviroment().securityUri + '/register',
      }, {}).then((data) => {
        resolve(data);
      }, (data) => {
        reject(data);
      });
    });
  }

  /**
   * realiza el cierre de sesion
   */
  setLogout() {
    this.http().storage().setClear();
    this.dataUser = new SystemUser();

    this.http().storage().object().timeout(100).then(() => {
      this.login.emit(this.dataUser);
    });
  }

}
