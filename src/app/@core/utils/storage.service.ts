import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {ObjectService} from './object.service';

@Injectable({
  providedIn: 'root',
})
export class StorageService {

  constructor(
    private oObjectService: ObjectService,
  ) {
  }

  /**
   * object
   */
  public object(): ObjectService {
    return this.oObjectService;
  }

  /**
   * obtiene las variables del entorno
   */
  public getEnviroment(): any {
    return environment;
  }

  /**
   * obtiene un valor almacenado
   *
   * @param key
   * @param options
   */
  public getStorage(key: string, options?: { def?: any, jsonParse?: any }): any {
    return (environment.session) ? this.getStorageSession(key, options) : this.getStorageLocal(key, options);
  }

  /**
   * setea una variable para almacenar en el navegador
   *
   * @param key
   * @param value
   * @param options
   */
  public setStorage(key: string, value: any, options?: { def?: any, jsonParse?: boolean }): void {
    (environment.session) ? this.setStorageSession(key, value, options) : this.setStorageLocal(key, value, options);
  }

  /**
   * obtiene un valor almacenado en el navegador
   *
   * @param key
   * @param options
   */
  public getStorageLocal(key: string, options?: { def?: any, jsonParse?: any }): any {
    const oValue = localStorage.getItem(key);

    return this.get(oValue, options);
  }

  /**
   * setea una variable para almacenar en el navegador
   *
   * @param key
   * @param value
   * @param options
   */
  public setStorageLocal(key: string, value: any, options?: { def?: any, jsonParse?: boolean }): void {
    localStorage.setItem(key, this.set(value, options));
  }

  /**
   * obtiene un valor almacenado en el navegador
   *
   * @param key
   * @param options
   */
  public getStorageSession(key: string, options?: { def?: any, jsonParse?: any }): any {
    const oValue = sessionStorage.getItem(key);

    return this.get(oValue, options);
  }

  /**
   * setea una variable para almacenar en el navegador
   *
   * @param key
   * @param value
   * @param options
   */
  public setStorageSession(key: string, value: any, options?: { def?: any, jsonParse?: boolean }): void {
    sessionStorage.setItem(key, this.set(value, options));
  }

  /**
   * Limpia la session
   */
  public setClear(): void {
    localStorage.clear();
    sessionStorage.clear();
  }

  /**
   * operacion get Item
   *
   * @param value
   * @param options
   */
  private get(value: any, options?: { def?: any, jsonParse?: any }): any {
    options = this.object().getDefault(options, {});
    options.def = this.object().getDefault(options.def, '');
    options.jsonParse = this.object().getDefault(options.jsonParse, false);

    if (!this.object().isNull(value)) {
      if (options.jsonParse === false) {
        return value;
      } else if (options.jsonParse === true) {
        return JSON.parse(value);
      }

      return (new options.jsonParse).deepCopy(JSON.parse(value), options.jsonParse);
    }

    return options.def;
  }

  /**
   * operacion get Item
   *
   * @param value
   * @param options
   */
  private set(value: any, options?: { def?: any, jsonParse?: boolean }): any {
    options = this.object().getDefault(options, {});
    options.def = this.object().getDefault(options.def, '');
    options.jsonParse = this.object().getDefault(options.jsonParse, false);

    return (!this.object().isNull(value)) ? ((options.jsonParse) ? JSON.stringify(value) : value) : options.def;
  }
}
