import {EventEmitter, Injectable, Output} from '@angular/core';
import {Router} from '@angular/router';
import {NbMenuItem, NbMenuService} from '@nebular/theme';
import {HttpService} from './http.service';
import {LoginService} from './login.service';
import {SystemMenu} from '../entities/system/system.menu';

@Injectable({
  providedIn: 'root',
})
export class MenuService {

  dataMenuBar: SystemMenu[] = [];
  dataNbMenuBarH: NbMenuItem[] = [];
  dataNbMenuBarV: NbMenuItem[] = [];
  dataMenuSidebar: SystemMenu[] = [];
  dataNbMenuSidebar: NbMenuItem[] = [];
  isNavbarOpen: boolean = false;
  isSidebarOpen: boolean = false;
  @Output() change: EventEmitter<boolean> = new EventEmitter();
  @Output() navbarOpen: EventEmitter<boolean> = new EventEmitter();
  @Output() sidebarOpen: EventEmitter<boolean> = new EventEmitter();

  constructor(
    private oHttpService: HttpService,
    private oLoginService: LoginService,
    private oNbMenuService: NbMenuService,
    private oRouter: Router,
  ) {
    this.constructMenuBar();
    this.constructMenuSidebar();

    this.http().storage().object().timeout(100).then(() => {
      this.change.emit(true);
    });
  }

  /**
   * http
   */
  public http(): HttpService {
    return this.oHttpService;
  }

  /**
   * router
   */
  public router(): Router {
    return this.oRouter;
  }

  /**
   * nbMenu
   */
  public nbMenu(): NbMenuService {
    return this.oNbMenuService;
  }

  /**
   * oculta/muestra navbar
   */
  public toogleNavbar() {
    this.isNavbarOpen = !this.isNavbarOpen;
    this.navbarOpen.emit(this.isNavbarOpen);
  }

  /**
   * oculta navbar
   */
  public closeNavbar() {
    this.isNavbarOpen = false;
    this.navbarOpen.emit(this.isNavbarOpen);
  }

  /**
   * oculta/muestra sidebar
   */
  public toogleSidebar() {
    this.isSidebarOpen = !this.isSidebarOpen;
    this.sidebarOpen.emit(this.isSidebarOpen);
  }

  /**
   * oculta sidebar
   */
  public closeSidebar() {
    this.isSidebarOpen = false;
    this.sidebarOpen.emit(this.isSidebarOpen);
  }

  /**
   * setea el menu en memoria
   */
  setMenu() {
    this.http().post(null, {
      remote: true,
      path: this.http().storage().getEnviroment().securityUri + '/generate_menu',
      showSuccess: false,
    }, {}).then((data) => {
      this.http().storage().setStorage(this.http().stgMenuBar, data.navbar, {jsonParse: true, def: []});
      this.http().storage().setStorage(this.http().stgMenuSidebar, data.sidebar, {jsonParse: true, def: []});
      this.constructMenu();

      this.http().storage().object().timeout(100).then(() => {
        this.change.emit(true);
      });
    });
  }

  /**
   * contruye todo el menu
   */
  constructMenu() {
    this.constructMenuBar();
    this.constructMenuSidebar();
  }

  /**
   * construye el menu superior
   */
  constructMenuBar() {
    const navItems: SystemMenu[] = this.getMenuStorage(this.http().stgMenuBar);
    this.dataMenuBar = navItems;
    this.dataNbMenuBarH = this.getNbMenu(navItems, true);
    this.dataNbMenuBarV = this.getNbMenu(navItems, false);
  }

  /**
   * contruye el menu lateral
   */
  constructMenuSidebar() {
    const navItems: SystemMenu[] = this.getMenuStorage(this.http().stgMenuSidebar);
    this.dataMenuSidebar = navItems;
    this.dataNbMenuSidebar = this.getNbMenu(navItems);
  }

  /**
   * obtiene un array generado compatible con menu nebular
   *
   * @param menus
   * @param horizontal
   */
  getNbMenu(menus: SystemMenu[], horizontal?: boolean): NbMenuItem[] {
    const items: NbMenuItem[] = [];
    const itemsTraduction: string[] = this.getTraductions(menus, ['app.home', 'app.logout']);
    horizontal = this.http().storage().object().getDefault(horizontal, false);

    this.http().storage().object().translate().stream(itemsTraduction).subscribe((data) => {
      items.push({
        title: this.getItem(data, 'app.home', 'Home'),
        expanded: horizontal,
        link: '/home',
        icon: {icon: 'home', pack: 'solid'},
      });

      for (const menu of this.setSubmenu(menus, horizontal, data)) {
        items.push(menu);
      }

      if (!horizontal) {
        if (this.oLoginService.getLogin()) {
          items.push({
            title: this.getItem(data, 'app.logout', 'Logout'),
            expanded: horizontal,
            link: '/auth/logout',
            icon: {icon: 'sign-out-alt', pack: 'solid'},
          });
        }
      }
    });

    return items;
  }

  /**
   * retorna los menues en sus entidad correspondiente
   *
   * @param key
   */
  private getMenuStorage(key: string): SystemMenu[] {
    const navItems: SystemMenu[] = this.http().storage().getStorage(key, {
      def: [],
      jsonParse: SystemMenu,
    });

    if (!this.http().storage().object().isNull(navItems)) {
      navItems.forEach((menus: SystemMenu) => {
        this.sort(menus);
      });
    }

    return navItems;
  }

  /**
   * obtiene todos los nombres para la traduccion
   *
   * @param menus
   * @param itemsTraduction
   */
  private getTraductions(menus: SystemMenu[], itemsTraduction: string[]): string[] {
    if (!this.http().storage().object().isNull(menus)) {
      for (const menu of menus) {
        itemsTraduction.push(menu.name);

        if (this.http().storage().object().getSize(menu.submenus) > 0) {
          itemsTraduction = this.getTraductions(menu.submenus, itemsTraduction);
        }
      }
    }

    return itemsTraduction;
  }

  /**
   * obtiene un valor de un array o retorna el valor por defecto
   *
   * @param data
   * @param search
   * @param def
   */
  private getItem(data: any, search: string, def?: string): string {
    def = def !== undefined ? def : search;
    data = data !== undefined ? (data[search] !== undefined ? data[search] : def) : def;

    return data !== search ? data : def;
  }

  /**
   * obtiene todo el submenu
   *
   * @param menus
   * @param horizontal
   * @param traduction
   */
  private setSubmenu(menus: SystemMenu[], horizontal: boolean, traduction: any): NbMenuItem[] {
    const t = this;
    const items: NbMenuItem[] = [];

    if (!this.http().storage().object().isNull(menus)) {
      menus.sort(((a: SystemMenu, b: SystemMenu) => (a.id > b.id) ? 1 : -1));

      for (const menu of menus) {
        const item: NbMenuItem = {
          title: t.getItem(traduction, menu.label, menu.name),
          expanded: horizontal,
          link: menu.uri,
        };
        if (!this.http().storage().object().isNull(menu.icon) && menu.icon !== '') {
          item.icon = {icon: menu.icon, pack: 'solid'};
        }
        if (this.http().storage().object().getSize(menu.submenus) > 0) {
          item.children = this.setSubmenu(menu.submenus, horizontal, traduction);
        }

        items.push(item);
      }
    }

    return items;
  }

  /**
   * ordena el listado del menu
   *
   * @param menus
   */
  private sort(menus: SystemMenu): void {
    if (this.http().storage().object().getSize(menus.submenus) > 0) {
      menus.submenus.sort((a, b) => a.id - b.id);

      menus.submenus.forEach(submenus => {
        this.sort(submenus);
      });
    }
  }

}
