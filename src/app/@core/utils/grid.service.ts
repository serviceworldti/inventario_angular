import {ComponentRef, EventEmitter, Injectable, Output} from '@angular/core';
import {DatePipe} from '@angular/common';
import {LocalDataSource} from 'ng2-smart-table';
import {
  GridFilterCheckboxComponent,
  GridFilterDateComponent,
  GridFilterDaterangeComponent,
  GridFilterInputComponent,
  GridFilterInputnumberComponent,
  GridFilterNoneComponent,
  GridFilterSelectComponent,
  GridFilterSelectmultipleComponent,
} from '../components';
import {HttpService} from './http.service';
import {GridNg2SmartTableUtil} from '../entities/util/smart-table-util/grid.ng2.smart.table.util';
import {GridUtil} from '../entities/util/grid.util';
import {ReportUtil} from '../entities/util/report.util';
import {EntityUtil} from '../entities/util/entity.util';
import {SystemAssignment} from '../entities';
import {ReportColumnUtil} from '../entities/util/report/report.column.util';
import {FileService} from './file.service';

@Injectable({
  providedIn: 'root',
})
export class GridService {

  public canReport = false;
  public canFiles = false;
  public data: GridUtil = new GridUtil();
  public dataFilter: any = [];
  public source: LocalDataSource = new LocalDataSource();
  @Output() change: EventEmitter<any> = new EventEmitter();
  public settings: GridNg2SmartTableUtil = new GridNg2SmartTableUtil();
  public reportData: ReportUtil = new ReportUtil();
  private ec: { entity: any, update: any, view: any } = {
    entity: EntityUtil,
    update: ComponentRef,
    view: ComponentRef,
  };
  private uri: { get: string, post: string, put: string, delete: string, report: string } = {
    get: '',
    post: '',
    put: '',
    delete: '',
    report: '',
  };

  constructor(
    private oHttpService: HttpService,
    public oFileService: FileService,
  ) {
    this.cleanGridComponent();

    this.http().isAssignments.subscribe((systemAssignment: SystemAssignment[]) => {
      this.settings.actions.add = false;
      this.settings.actions.edit = false;
      this.settings.actions.delete = false;

      systemAssignment.forEach((o: SystemAssignment) => {
        if (o.method === 'POST') {
          this.settings.actions.add = true;
        }

        if (o.method === 'PUT') {
          this.settings.actions.edit = true;
        }

        if (o.method === 'DELETE') {
          this.settings.actions.delete = true;
        }

        if (o.method === 'REPORT') {
          this.canReport = true;
        }

        if (o.method === 'FILES') {
          this.canFiles = true;
        }
      });
    });
  }

  /**
   * http
   */
  public http(): HttpService {
    return this.oHttpService;
  }

  /**
   * file
   */
  public setInit(get: string, ec: {
    entity?: any,
    update?: any,
    view?: any,
  }, options?: {
    onGet?: string,
    onUpdate?: string,
    onSave?: string,
    onDelete?: string,
    onReport?: string,
  }): void {
    options = this.http().storage().object().getDefault(options, {});
    ec = this.http().storage().object().getDefault(ec, {});
    this.uri.get = get + this.http().storage().object().getDefault(options.onGet, '');
    this.uri.post = this.http().storage().object().getDefault(options.onUpdate, get);
    this.uri.put = this.http().storage().object().getDefault(options.onSave, get);
    this.uri.delete = this.http().storage().object().getDefault(options.onDelete, get);
    this.uri.report = this.http().storage().object().getDefault(options.onReport, this.uri.post + '/report');
    this.ec.entity = this.http().storage().object().getDefault(ec.entity, null);
    this.ec.update = this.http().storage().object().getDefault(ec.update, null);
    this.ec.view = this.http().storage().object().getDefault(ec.view, null);

    if (this.ec.view !== null) {
      this.settings.attr.class = 'smart-table-has-view';
    }

    this.cleanGridComponent();
  }

  /**
   * Limpia el componente
   */
  cleanGridComponent() {
    this.data = new GridUtil();
    this.dataFilter = [];
    this.source.empty().then(() => {
    });
    this.settings.actions.columnTitle = this.http().storage().object().translate().instant('app.actions');
    this.settings.noDataMessage = this.http().storage().object().translate().instant('app.nodata');
  }

  /**
   * crea una modal para agregar datos
   *
   * @param event
   */
  onSelect(event): void {
    if (this.http().storage().object().isNotNull(this.ec.view)) {
      this.http().storage().object().dialog().open(this.ec.view, {
        backdropClass: 'modalNebular',
        closeOnBackdropClick: true,
        hasScroll: true,
        context: {
          title: 'app.view',
          model: event.data,
        },
      });
    }
  }

  /**
   * crea una modal para agregar datos
   *
   * @param event
   */
  onCreate(event): void {
    if (this.http().storage().object().isNotNull(this.ec.update)) {
      this.http().storage().object().dialog().open(this.ec.update, {
        backdropClass: 'modalNebular',
        context: {
          title: 'app.new',
          model: new this.ec.entity,
        },
      });
    }
  }

  /**
   * crea una modal para editar o modificar datos
   *
   * @param event
   */
  onEdit(event): void {
    if (this.http().storage().object().isNotNull(this.ec.update)) {
      this.http().storage().object().dialog().open(this.ec.update, {
        backdropClass: 'modalNebular',
        context: {
          title: 'app.update',
          model: event.data,
        },
      });
    }
  }

  /**
   * crea una modal de confirmacion para la eliminacion de registros
   *
   * @param event
   */
  onDelete(event): void {
    this.oHttpService.alert().openDialog({
      title: 'error.title.428',
      message: 'app.confirm_delete',
      acceptTitle: 'app.accept',
      accept: () => {
        this.setDelete(event.data);
      },
      decline: () => {
      },
    });
  }

  /**
   * genera la informacion necesaria para construir el reporte
   *
   * @param event
   * @param type
   * @param name
   */
  onReport(event, type?: string, name?: string) {
    this.setColumnReport();
    this.reportData.name = this.http().storage().object().translate()
      .instant(this.http().storage().object().getDefault(name, this.reportData.name));

    this.source.getFilteredAndSorted().then((data) => {
      if (this.http().storage().object().isNotNull(data) || data.length > 1) {
        this.reportData.data = data;
        this.reportData.type = type;

        this.http().post(this.reportData, {
          path: this.uri.report,
        }, {}).then((result) => {
          this.http().storage().object().timeout(100).then(() => {
            this.oFileService.downloadFileFromBase64(result.base64);
          });
        });
      } else {
        this.oHttpService.alert().openDialog({
          title: 'error.title.204',
          message: 'No hay ninguna información',
        });
      }
    });
  }

  /**
   * refresca la tabla y sus atributos
   */
  refreshSettings(): void {
    const t = this;
    const settings: any = this.settings;

    if (this.http().storage().object().isNotNull(settings.columns)) {
      Object.keys(settings.columns).every((name) => {
        let column = settings.columns[name];
        const attribute = t.http().storage().object().getDefault(column.attribute, name);
        column.title = t.http().storage().object().translate().instant(column.title);

        switch (column.type) {
          case 'list':
            column = t.getList(column, attribute, t.data.data);
            break;
          case 'list-multiple':
            column = t.getListMultiple(column, attribute, t.data.data);
            break;
          case 'date':
            column = t.getDate(column, attribute, t.data.data);
            break;
          case 'date-range':
            column = t.getDateRange(column, attribute, t.data.data);
            break;
          case 'boolean':
          case 'checkbox':
            column = t.getCheckbox(column, attribute, t.data.data);
            break;
          case 'none':
            column = t.getNone(column, attribute, t.data.data);
            break;
          case 'number':
            column = t.getInputNumber(column, attribute, t.data.data);
            break;
          case 'custom':
            break;
          default:
            column = t.getInput(column, attribute, t.data.data);
        }

        settings.columns[name] = column;
        return t.settings.columns[name];
      });
    }

    this.source.load(this.data.data).then(() => {
    });
    this.http().storage().object().timeout(100).then(() => this.settings = Object.assign({}, settings));
  }

  /**
   * realiza la obtencion de la data
   *
   * @param options
   */
  public get<T>(options?: { path?: string}): Promise<any> {
    options = this.http().storage().object().getDefault(options, {});
    options.path = this.http().storage().object().getDefault(options.path, this.uri.get);

    return new Promise<any>((resolve, reject) => {
      this.http().get<T>(null, {
        path: options.path,
      }, {}).then(result => {
        this.data = result;

        this.http().storage().object().timeout(100).then(() => {
          resolve(result);
          this.refreshSettings();
        });
      }, result => {
        this.http().storage().object().timeout(100).then(() => {
          reject(result);
        });
      });
    });
  }

  /**
   * realiza la obtencion de la data
   *
   * @param options
   */
  public list<T>(options?: { path?: string }): Promise<any> {
    options = this.http().storage().object().getDefault(options, {});
    options.path = this.http().storage().object().getDefault(options.path, this.uri.get);

    return new Promise<any>((resolve, reject) => {
      this.http().list<T>(null, {
        path: options.path,
        entity: GridUtil,
      }, {}).then((result: GridUtil) => {
        this.data = result;

        this.http().storage().object().timeout(100).then(() => {
          resolve(result);
          this.refreshSettings();
        });
      }, result => {
        this.http().storage().object().timeout(100).then(() => {
          reject(result);
        });
      });
    });
  }

  /**
   * realiza la actualizacion de la data
   *
   * @param o
   * @param options
   */
  public saveOrUpdate(o: any, options?: { path?: string }): Promise<any> {
    options = this.http().storage().object().getDefault(options, {});
    options.path = this.http().storage().object().getDefault(options.path, this.uri.post);

    return new Promise<any>((resolve, reject) => {
      this.http().put(o, {
        path: options.path,
      }).then(result => {
        this.list().then(resolve, reject);
      });
    });
  }

  /**
   * realiza la eliminacion de la data
   *
   * @param o
   * @param options
   */
  public setDelete(o: any, options?: { path?: string, success?: any }): Promise<any> {
    options = this.http().storage().object().getDefault(options, {});
    options.path = this.http().storage().object().getDefault(options.path, this.uri.delete);
    options.success = this.http().storage().object().getDefault(options.success, () => {
    });

    return new Promise<any>((resolve, reject) => {
      this.http().delete(o, {
        path: options.path,
      }).then(result => {
        this.list().then(resolve, reject);
      });
    });
  }

  /**
   * setea las opciones por defecto del reporte
   *
   * @param options
   */
  private getOptions(options?: {
    nameReport?: string,
    type?: string,
    success?: any,
    otherColumns?: any,
  }): any {
    options = this.http().storage().object().getDefault(options, {});
    options.nameReport = this.http().storage().object().getDefault(options.nameReport, 'Report');
    options.type = this.http().storage().object().getDefault(options.type, 'pdf');
    options.otherColumns = this.http().storage().object().getDefault(options.otherColumns, {});
    options.success = this.http().storage().object().getDefault(options.success, undefined);

    return options;
  }

  /**
   * setea las columnas por defecto del reporte
   */
  private setColumnReport() {
    const t = this;
    const settings: any = this.settings;

    if (this.reportData.columns.length < 1) {
      Object.keys(settings.columns).every(function (name) {
        const col = settings.columns[name];
        let add: boolean = true;

        if (t.http().storage().object().isNotNull(col['exclude'])) {
          add = col['exclude'];
        }

        if (add) {
          const column: ReportColumnUtil = new ReportColumnUtil();
          column.title = col['title'];
          column.type = col['type'];
          column.attribute = t.http().storage().object().isNotNull(col['attribute']) ? col['attribute'] : name;

          t.reportData.columns.push(column);
        }

        return settings.columns[name];
      });
    }
  }

  /**
   * deja el filtro en blanco
   *
   * @param column
   * @param attribute
   * @param data
   */
  private getNone(column: string, attribute: string, data: any): any {
    column['renderComponent'] = GridFilterNoneComponent;
    column['filterFunction'] = (): boolean => {
      return true;
    };
    column['filter'] = {
      component: GridFilterNoneComponent,
      type: 'custom',
    };

    return column;
  }

  /**
   * coloca un filtro de campo tipo numerico
   *
   * @param column
   * @param attribute
   * @param data
   */
  private getInputNumber(column: string, attribute: string, data: any): any {
    column['renderComponent'] = GridFilterInputnumberComponent;
    column['filterFunction'] = (cell?: any, search?: any): boolean => {
      const value = (attribute.indexOf('.') > -1) ? cell[attribute.replace(name + '.', '')] : cell;

      return (search === value.toString() || search === '');
    };
    column['filter'] = {
      component: GridFilterInputnumberComponent,
      type: 'custom',
    };

    return column;
  }

  /**
   * coloca un filtro de campo tipo texto
   *
   * @param column
   * @param attribute
   * @param data
   */
  private getInput(column: string, attribute: string, data: any): any {
    column['renderComponent'] = GridFilterInputComponent;
    column['filterFunction'] = (cell?: any, search?: any): boolean => {
      const value = (attribute.indexOf('.') > -1) ? cell[attribute.replace(name + '.', '')] : cell;

      return (value.toLowerCase().indexOf(search.toLowerCase()) > -1 || search === '');
    };
    column['filter'] = {
      component: GridFilterInputComponent,
      type: 'custom',
    };

    return column;
  }

  /**
   * coloca un filtro de tipo fecha
   *
   * @param column
   * @param attribute
   * @param data
   */
  private getDate(column: string, attribute: string, data: any): any {
    column['renderComponent'] = GridFilterDateComponent;
    column['filterFunction'] = (cell?: any, search?: any): boolean => {
      let value: any = (attribute.indexOf('.') > -1) ? cell[attribute.replace(name + '.', '')] : cell;
      value = this.http().storage().object().isNotType(value, ['object', 'number']) ? new Date(value).getTime() : value;
      const init: number = Number(search);
      const end: number = init + 86399999;

      return ((value >= init && value <= end) || search === '');
    };
    column['filter'] = {
      component: GridFilterDateComponent,
      type: 'custom',
    };

    return column;
  }

  /**
   * coloca un filtro de rango de fechas
   *
   * @param column
   * @param attribute
   * @param data
   */
  private getDateRange(column: string, attribute: string, data: any): any {
    column['renderComponent'] = GridFilterDaterangeComponent;
    column['filterFunction'] = (cell?: any, search?: any): boolean => {
      let value: any = (attribute.indexOf('.') > -1) ? cell[attribute.replace(name + '.', '')] : cell;
      value = this.http().storage().object().isNotType(value, ['object', 'number']) ? new Date(value).getTime() : value;
      const searchSplit = search.split('-');
      const init: number = Number(searchSplit[0]);
      const end: number = Number(searchSplit[1]);

      return ((value >= init && value <= end) || search === '');
    };
    column['filter'] = {
      component: GridFilterDaterangeComponent,
      type: 'custom',
    };
    column['valuePrepareFunction'] = (cell, row) =>
      new DatePipe('en-US').transform(cell, 'MM/dd/yy hh:mm a', 'UTC');

    return column;
  }

  /**
   * coloca un filtro de seleccion booleana
   *
   * @param column
   * @param attribute
   * @param data
   */
  private getCheckbox(column: any, attribute: string, data: any): any {
    column['renderComponent'] = GridFilterCheckboxComponent;
    column['filterFunction'] = (cell?: any, search?: any): boolean => {
      const value = (attribute.indexOf('.') > -1) ? cell[attribute.replace(name + '.', '')] : cell;

      return (value.toString().toLowerCase().indexOf(search.toLowerCase()) > -1 || search === '');
    };
    column['filter'] = {
      component: GridFilterCheckboxComponent,
      type: 'custom',
    };
    if (this.http().storage().object().isNotNull(column['translate'])) {
      column['valuePrepareFunction'] = (cell: any, row: any) => {
        return this.http().storage().object().translate().instant('app.' + cell);
      };
    }

    return column;
  }

  /**
   * coloca un filtro de tipo lista
   *
   * @param column
   * @param attribute
   * @param data
   */
  private getList(column: string, attribute: string, data: any): any {
    const l: string[] = [];

    column['renderComponent'] = GridFilterSelectComponent;
    column['filterFunction'] = (cell?: any, search?: any): boolean => {
      const value = (attribute.indexOf('.') > -1) ? cell[attribute.replace(name + '.', '')] : cell;

      return (value === search || search === '');
    };
    column['filter'] = {
      component: GridFilterSelectComponent,
      type: 'custom',
      config: {
        selectText: 'Seleccione',
        list: [],
      },
    };

    if (this.http().storage().object().isNotType(data, ['string', 'number'])) {
      for (const d of data) {
        let value = d;

        if (!this.http().storage().object().isNull(attribute) && !this.http().storage().object().isNull(value)) {
          if (attribute.indexOf('.') > -1) {
            for (const x of attribute.split('.')) {
              value = !this.http().storage().object().isNull(value) ? value[x] : null;
            }
          } else {
            value = value[attribute];
          }

          if (l.indexOf(value) === -1 && value !== null) {
            column['filter']['config'].list.push(
              {value: value, title: value},
            );
            l.push(value);
          }
        }
      }
    }

    return column;
  }

  /**
   * coloca un filtro de tipo lista multiple
   *
   * @param column
   * @param attribute
   * @param data
   */
  private getListMultiple(column: string, attribute: string, data: any): any {
    const l: string[] = [];

    column['renderComponent'] = GridFilterSelectmultipleComponent;
    column['filterFunction'] = (cell?: any, search?: any): boolean => {
      const value = (attribute.indexOf('.') > -1) ? cell[attribute.replace(name + '.', '')] : cell;

      return (value === search || search === '' ||
        (typeof (search) !== 'string' && typeof (search) !== 'number' && search.indexOf(value) > -1));
    };
    column['filter'] = {
      component: GridFilterSelectmultipleComponent,
      type: 'custom',
      config: {
        selectText: 'Seleccione',
        list: [],
      },
    };

    if (this.http().storage().object().isNotType(data, ['string', 'number'])) {
      for (const d of data) {
        let value = d;

        if (!this.http().storage().object().isNull(attribute) && !this.http().storage().object().isNull(value)) {
          if (attribute.indexOf('.') > -1) {
            for (const x of attribute.split('.')) {
              value = !this.http().storage().object().isNull(value) ? value[x] : null;
            }
          } else {
            value = value[attribute];
          }

          if (l.indexOf(value) === -1 && value !== null) {
            column['filter']['config'].list.push(
              {value: value, title: value},
            );
            l.push(value);
          }
        }
      }
    }

    return column;
  }

}
