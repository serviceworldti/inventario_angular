import {EventEmitter, Injectable, Output} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpEvent, HttpEventType, HttpHeaders, HttpResponse} from '@angular/common/http';
import {NgxSoapService} from 'ngx-soap';
import {StorageService} from './storage.service';
import {webSocket, WebSocketSubject} from 'rxjs/webSocket';
import {map} from 'rxjs/operators';
import {AlertService} from './alert.service';
import {SystemAssignment} from '../entities';
import {GridUtil} from '../entities/util/grid.util';

@Injectable({
  providedIn: 'root',
})
export class HttpService {

  public assignments: SystemAssignment[] = [];
  @Output() isAssignments: EventEmitter<SystemAssignment[]> = new EventEmitter();
  public stgAuthJwt: string = 'X-Auth-Jwt';
  public stgAuthSession: string = 'X-Auth-Session';
  public stgAuthOrganization: string = 'X-Auth-Organization';
  public stgAuthToken: string = 'X-Auth-Token';
  public stgMenuBar: string = 'X-Menu-Bar';
  public stgMenuSidebar: string = 'X-Menu-Sidebar';
  private getAssignments: string[] = ['GET', 'POST', 'PUT', 'DELETE', 'REPORT', 'FILES'];

  constructor(
    private oHttpClient: HttpClient,
    private oNgxSoapService: NgxSoapService,
    private olocalStorageService: StorageService,
    private oAlertService: AlertService,
  ) {
  }

  /**
   * storage
   */
  public storage(): StorageService {
    return this.olocalStorageService;
  }

  /**
   * alert
   */
  public alert(): AlertService {
    return this.oAlertService;
  }

  public setAssignmentsDefault() {
    const t = this;

    this.getAssignments.forEach((o: string, index: number) => {
      const a = new SystemAssignment();
      a.method = o;
      a.name = o;
      this.assignments.push(a);

      if (index === (t.getAssignments.length - 1)) {
        t.isAssignments.emit(t.assignments);
      }
    });
  }

  /**
   * obtiene un token de seguridad (JWT)
   *
   * @param options
   */
  public getJwt<T>(options?: {
    path?: string,
    remote?: boolean,
    cors?: boolean,
    showError?: boolean,
    showSuccess?: boolean,
    onLoad?: any,
  }): Promise<any> {
    const nowTime: number = (new Date()).getTime();
    const timeJwt: any = this.storage().getStorage(this.stgAuthJwt + 'Time');
    const call = (this.storage().object().isNotNull(timeJwt) && timeJwt !== '') ?
      (nowTime - timeJwt > (5 * 60 * 100)) : true;

    if (call) {
      options = this.storage().object().getDefault(options, {});
      options.path = this.storage().object().getDefault(options.path, this.storage().getEnviroment().securityUri);
      options.showSuccess = false;

      return new Promise<any>((resolve, reject) => {
        this.oPost<T>(null, options).then(resolve, reject);
      });
    } else {
      return new Promise<any>((resolve, reject) => {
        resolve();
      });
    }
  }

  /**
   * activa las notificaciones
   *
   * @param success
   * @param error
   */
  public getNotifications(success: any, error?: any): WebSocketSubject<any> {
    return this.getWs(this.storage().getEnviroment().sockets + 'notification', '', success, error);
  }

  /**
   * abre un websocket para la transmision de dato en tiempo real
   *
   * @param uri
   * @param firstMessage
   * @param success
   * @param error
   * @param remote
   * @param cors
   */
  public getWs(uri: string, firstMessage: any, success: any, error?: any, remote?: boolean, cors?: boolean):
    WebSocketSubject<any> {
    error = error !== undefined ? error : () => {
    };
    remote = remote !== undefined ? remote : true;
    cors = cors !== undefined ? cors : true;
    const subject = webSocket(this.getUri(uri, remote, cors).replace('http:', 'ws:').replace('https:', 'wss:'));

    subject.subscribe(
      (message) => success(message),
      (err) => error(err),
    );
    subject.next({message: firstMessage, jwt: this.getHeader(this.stgAuthJwt)});

    return subject;
  }

  /**
   * realiza la peticion GET
   *
   * @param o
   * @param options
   * @param optionsJwt
   */
  public get<T>(o: any, options?: {
    path?: string,
    remote?: boolean,
    cors?: boolean,
    showError?: boolean,
    showSuccess?: boolean,
    onLoad?: any,
    entity?: any,
  }, optionsJwt?: {
    path?: string,
    remote?: boolean,
    error?: any,
    cors?: boolean,
    onLoad?: any,
  }): Promise<any> {
    if (this.storage().object().isNotNull(optionsJwt)) {
      return this.getJwt<any>(optionsJwt).then(() => this.oGet<T>(o, options));
    } else {
      return this.oGet<T>(o, options);
    }
  }

  /**
   * realiza la peticion GET
   *
   * @param o
   * @param options
   * @param optionsJwt
   */
  public list<T>(o: any, options?: {
    path?: string,
    remote?: boolean,
    cors?: boolean,
    showError?: boolean,
    showSuccess?: boolean,
    onLoad?: any,
    entity?: any,
  }, optionsJwt?: {
    path?: string,
    remote?: boolean,
    cors?: boolean,
    onLoad?: any,
  }): Promise<any> {
    if (this.storage().object().isNotNull(optionsJwt)) {
      return this.getJwt<any>(optionsJwt).then(() => this.oPost<T>(new GridUtil().setMaxLength(), options));
    } else {
      return this.oPost<T>(new GridUtil().setMaxLength(), options);
    }
  }

  /**
   * realiza la peticion POST
   *
   * @param o
   * @param options
   * @param optionsJwt
   */
  public post<T>(o: any, options?: {
    path?: string,
    remote?: boolean,
    cors?: boolean,
    showError?: boolean,
    showSuccess?: boolean,
    onLoad?: any,
    entity?: any,
  }, optionsJwt?: {
    path?: string,
    remote?: boolean,
    cors?: boolean,
    onLoad?: any,
  }): Promise<any> {
    if (optionsJwt !== undefined && optionsJwt !== null) {
      return this.getJwt<any>(optionsJwt).then(() => this.oPost<T>(o, options));
    } else {
      return this.oPost<T>(o, options);
    }
  }

  /**
   * realiza la peticion POST
   *
   * @param o
   * @param options
   * @param optionsJwt
   */
  public put<T>(o: any, options?: {
    path?: string,
    remote?: boolean,
    cors?: boolean,
    showError?: boolean,
    showSuccess?: boolean,
    onLoad?: any,
    entity?: any,
  }, optionsJwt?: {
    path?: string,
    remote?: boolean,
    cors?: boolean,
    onLoad?: any,
  }): Promise<any> {
    if (optionsJwt !== undefined && optionsJwt !== null) {
      return this.getJwt<any>(optionsJwt).then(() => this.oPut<T>(o, options));
    } else {
      return this.oPut<T>(o, options);
    }
  }

  /**
   * realiza la peticion DELETE
   *
   * @param o
   * @param options
   * @param optionsJwt
   */
  public delete<T>(o: any, options?: {
    path?: string,
    remote?: boolean,
    cors?: boolean,
    showError?: boolean,
    showSuccess?: boolean,
    onLoad?: any,
    entity?: any,
  }, optionsJwt?: {
    path?: string,
    remote?: boolean,
    cors?: boolean,
    onLoad?: any,
  }): Promise<any> {
    if (optionsJwt !== undefined && optionsJwt !== null) {
      return this.getJwt<any>(optionsJwt).then(() => this.oDelete<T>(o, options));
    } else {
      return this.oDelete<T>(o, options);
    }
  }

  /**
   * realiza la consulta con el metodo GET
   *
   * @param path
   * @param options
   */
  public soap = (path: string, options?: {
    cors?: string,
    endpoint?: string,
    success?: any,
    optionWsdl?: any,
  }): void => {
    this.storage().object().loading().fullProgress();

    options = this.storage().object().getDefault(options, {});
    options.cors = this.storage().object().getDefault(options.cors, '');
    options.endpoint = this.storage().object().getDefault(options.endpoint, '');
    options.success = this.storage().object().getDefault(options.success, () => {
    });
    options.optionWsdl = this.storage().object().getDefault(options.optionWsdl, {});

    this.oNgxSoapService.createClient(options.cors + path, options.optionWsdl)
      .then((response) => {
        if (options.cors && options.endpoint !== '') {
          response.setEndpoint(options.cors + options.endpoint);
        }

        this.storage().object().loading().resetProgress();

        options.success(response);
      }).catch(error => {
      this.callError(error, null);
    });
  }

  /**
   * Realiza el llamado del metodo
   *
   * @param soapResult
   * @param method
   * @param body
   * @param success
   */
  public call(soapResult: any, method: string, body: any, success: any): void {
    this.storage().object().loading().fullProgress();

    soapResult.call(method, body).subscribe(result => {
      success(result);
      this.storage().object().loading().resetProgress();
    }, error => {
      this.storage().object().loading().resetProgress();
      this.callError(error, null);
    });
  }

  /**
   * obtiene las cabeceras
   */
  private getHeaders(): any {
    return new HttpHeaders()
      .set('Content-type', 'application/json')
      .set(this.stgAuthJwt, this.getHeader(this.stgAuthJwt))
      .set(this.stgAuthOrganization, this.storage().getEnviroment().organization);
  }

  /**
   * setea las cabeceras
   *
   * @param header
   */
  private setHeaders(header: HttpHeaders): void {
    const headers = [this.stgAuthJwt];
    const time: Date = new Date();

    for (const h of headers) {
      if (header.has(h)) {
        this.storage().setStorage(h, header.get(h));
        this.storage().setStorage(h + 'Time', time.getTime());
      }
      if (header.has(h.toLowerCase())) {
        this.storage().setStorage(h, header.get(h.toLowerCase()));
        this.storage().setStorage(h + 'Time', time.getTime());
      }
    }
  }

  /**
   * obtiene la cabecera
   *
   * @param key
   */
  private getHeader(key: string): string {
    return this.storage().getStorage(key);
  }

  /**
   * construye la uri según los parametros indicados
   *
   * @param path
   * @param remote
   * @param cors
   * @param entity
   */
  private getUri = (path: string, remote: boolean, cors: boolean, entity?: any): string => {
    const component = this;
    const ols = component.storage();
    let uri = (cors && ols.getEnviroment().enableCors ? ols.getEnviroment().urlCors : '');
    uri += (remote ? ols.getEnviroment().urlRemote : '');
    uri += path + (entity != null ? (entity.id != null ? '/' + entity.id : '') : '');

    return uri;
  }

  /**
   * construye las opciones predeterminadas
   *
   * @param options
   */
  private getOptions = (options?: {
    path?: string,
    remote?: boolean,
    cors?: boolean,
    showError?: boolean,
    showSuccess?: boolean,
    onLoad?: any,
    type?: any,
    entity?: any,
  }): any => {
    this.storage().object().loading().fullProgress();

    options = this.storage().object().getDefault(options, {});
    options.path = this.storage().object().getDefault(options.path, '');
    options.remote = this.storage().object().getDefault(options.remote, true);
    options.cors = this.storage().object().getDefault(options.cors, true);
    options.onLoad = this.storage().object().getDefault(options.onLoad, (data) => {
    });
    options.showError = this.storage().object().getDefault(options.showError, true);
    options.showSuccess = this.storage().object().getDefault(options.showSuccess, true);
    options.type = this.storage().object().getDefault(options.type, null);
    options.entity = this.storage().object().getDefault(options.entity, null);

    return options;
  }

  /**
   *
   * @param event
   */
  private callEvent<T>(event: HttpEvent<T>): HttpEvent<T> | { status: string, message: number } {
    switch (event.type) {
      case HttpEventType.UploadProgress:
        const progress = Math.round(100 * event.loaded / event.total);
        return {status: 'progress', message: progress};

      case HttpEventType.Response:
        return event;
      default:
        return {status: 'progress', message: 100};
    }
  }

  /**
   * exito predeterminado
   *
   * @param response
   * @param options
   */
  private callSuccess<T>(response: HttpResponse<T> | { status: string, message: number }, resolve: any, options?: {
    path?: string,
    remote?: boolean,
    cors?: boolean,
    onLoad?: any,
    entity?: any,
  }): void {
    if ((<HttpResponse<T>>response).headers !== undefined) {
      this.setHeaders((<HttpResponse<T>>response).headers);
      const body: any = (<HttpResponse<T>>response).body;

      this.storage().object().loading().resetProgress();

      if (this.storage().object().isNotNull(resolve)) {
        if (this.storage().object().isNotNull(options.entity)) {
          resolve((new options.entity).deepCopy(body, options.entity));
        } else {
          resolve(body);
        }
      }
    } else {
      this.storage().object().loading().setProgress(<{ status: string, message: number }>response);
      options.onLoad(<{ status: string, message: number }>response);
    }
  }

  /**
   * error predeterminado
   *
   * @param error
   * @param options
   */
  private callError<T>(error: HttpErrorResponse, reject: any, options?: {
    path?: string,
    remote?: boolean,
    cors?: boolean,
    showError?: boolean,
    showSuccess?: boolean,
    onLoad?: any,
  }): void {
    const err = this.storage().object().getDefault(error.error, {});
    const code = this.storage().object().getDefault(err.code, error.status);
    const messageTr = this.storage().object().translate().instant('error.message.' + code);
    let message = this.storage().object().getDefault(err.message, error.statusText);
    this.storage().object().loading().setProgress({status: 'error', message: 101});

    if (messageTr !== 'error.message.' + code) {
      const a = message.indexOf('Detail:');
      message = a > -1 ? message.substring(a, message.length - 1).replace('Detail:', '. Detalle:') : '. ' + message;
      message = messageTr; // + message;
    }

    if (this.storage().object().isNotNull(reject)) {
      reject(error.error);
    }

    if (options.showError) {
      this.oAlertService.openDialog({
        title: 'error.title.' + code,
        message: message,
      });
    }

    this.storage().object().timeout(1000).then(() => {
      this.storage().object().loading().resetProgress();
    });
  }

  /**
   * realiza la consulta con el metodo GET
   *
   * @param o
   * @param options
   */
  private oGet<T>(o: any, options?: {
    path?: string,
    remote?: boolean,
    cors?: boolean,
    showError?: boolean,
    showSuccess?: boolean,
    onLoad?: any,
    type?: any,
    entity?: any,
  }): Promise<any> {
    options = this.getOptions(options);
    const uri = this.getUri(options.path, options.remote, options.cors, o);

    return new Promise<any>((resolve, reject) => {
      this.oHttpClient.get<T>(uri, {
        headers: this.getHeaders(),
        reportProgress: true,
        observe: 'events',
        responseType: 'json',
      }).pipe(map((event: HttpEvent<T>) => {
          return this.callEvent<T>(event);
        }),
      ).subscribe((response: HttpResponse<T>) => {
        this.callSuccess<T>(response, resolve, options);
      }, (error: HttpErrorResponse) => {
        this.callError<T>(error, reject, options);
      });
    });
  }

  /**
   * realiza la consulta con el metodo POST
   *
   * @param o
   * @param options
   */
  private oPost<T>(o: any, options?: {
    path?: string,
    remote?: boolean,
    cors?: boolean,
    showError?: boolean,
    showSuccess?: boolean,
    onLoad?: any,
    entity?: any,
  }): Promise<any> {
    options = this.getOptions(options);
    const uri = this.getUri(options.path, options.remote, options.cors);

    return new Promise<any>((resolve, reject) => {
      this.oHttpClient.post<T>(uri, o, {
        headers: this.getHeaders(),
        reportProgress: true,
        observe: 'events',
        responseType: 'json',
      }).pipe(map((event: HttpEvent<T>) => {
          return this.callEvent<T>(event);
        }),
      ).subscribe((response: HttpResponse<T>) => {
          this.callSuccess<T>(response, resolve, options);
        }, (error: HttpErrorResponse) => {
          this.callError<T>(error, reject, options);
        },
      );
    });
  }

  /**
   * realiza la consulta con el metodo PUT
   *
   * @param o
   * @param options
   */
  private oPut<T>(o: any, options?: {
    path?: string,
    remote?: boolean,
    cors?: boolean,
    showError?: boolean,
    showSuccess?: boolean,
    onLoad?: any,
    entity?: any,
  }): Promise<any> {
    options = this.getOptions(options);
    const uri = this.getUri(options.path, options.remote, options.cors);

    return new Promise<any>((resolve, reject) => {
      this.oHttpClient.put<T>(uri, o, {
        headers: this.getHeaders(),
        observe: 'events',
        responseType: 'json',
      }).pipe(map((event: HttpEvent<T>) => {
          return this.callEvent<T>(event);
        }),
      ).subscribe((response: HttpResponse<T>) => {
          this.callSuccess<T>(response, resolve, options);

          if (options.showSuccess && response.headers !== undefined) {
            this.oAlertService.setAlert(this.storage().object().translate().instant('app.updatesuccess'));
          }
        }, (error: HttpErrorResponse) => {
          this.callError<T>(error, reject, options);
        },
      );
    });
  }

  /**
   * realiza la consulta con el metodo DELETE
   *
   * @param o
   * @param options
   */
  private oDelete<T>(o: any, options?: {
    path?: string,
    remote?: boolean,
    cors?: boolean,
    showError?: boolean,
    showSuccess?: boolean,
    onLoad?: any,
    entity?: any,
  }): Promise<any> {
    options = this.getOptions(options);
    const uri = this.getUri(options.path, options.remote, options.cors, o);

    return new Promise<any>((resolve, reject) => {
      this.oHttpClient.delete<T>(uri, {
        headers: this.getHeaders(),
        reportProgress: true,
        observe: 'events',
        responseType: 'json',
      }).pipe(map((event: HttpEvent<T>) => {
          return this.callEvent(event);
        }),
      ).subscribe((response: HttpResponse<T>) => {
          this.callSuccess<T>(response, resolve, options);

          if (options.showSuccess && response.headers !== undefined) {
            this.oAlertService.setAlert(this.storage().object().translate().instant('app.deletesuccess'));
          }
        }, (error: HttpErrorResponse) => {
          this.callError<T>(error, reject, options);
        },
      );
    });
  }

}
