/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  session: true,
  enableCors: false,
  organization: '$s0$41010$9q6JeDolBwCzhNdPIlf7EA==$hspt9DzZ2h4xKlQe+6VLNc1BdD99alj9DKctMldOGVs=',
  urlCors: 'https://cors-anywhere.herokuapp.com/',
  urlRemote: 'http://localhost:8080/skeleton/',
  securityUri: 'rest/system_security',
  sockets: 'websocket/',
  baseUri: '',
  theme: 'dark',
  lang: 'es',
};
