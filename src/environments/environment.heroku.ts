/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  session: true,
  enableCors: false,
  organization: '$s0$41010$9q6JeDolBwCzhNdPIlf7EA==$hspt9DzZ2h4xKlQe+6VLNc1BdD99alj9DKctMldOGVs=',
  urlCors: 'https://cors-anywhere.herokuapp.com/',
  urlRemote: 'https://skeleton--java.herokuapp.com/skeleton/',
  securityUri: 'rest/system_security',
  sockets: 'websocket/',
  baseUri: '',
  theme: 'dark',
  lang: 'es',
};
